var express = require('express');
var jwt = require("jsonwebtoken")

const app = express();

app.post('/api/login', function(req, res){

    // insert code here to actually authenticate, or fake it
    const user = { id: 3 };
  
    // then return a token, secret key should be an env variable
    const token = jwt.sign({ user: user.id }, 'Admin key');
    res.json({
      token: token
    });

    return token;
}
)

app.listen(3000, function () {
  console.log('App listening on port 3000!');
});