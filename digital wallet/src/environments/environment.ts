
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  // apiUrl: EnvServiceFactory().apiUrl,//'http://localhost:8090'
  // enableDebug:EnvServiceFactory().enableDebug    // apiUrl: 'http://localhost:8090',
  apiUrl: 'http://197.51.66.21:8095',
  // firebase: {
  //   apiKey: "AIzaSyBtKrYk_QTimHAD5Z5GWlpB_dutAcjufLo",
  //   authDomain: "digital-wallet-e6c62.firebaseapp.com",
  //   databaseURL: "https://digital-wallet-e6c62.firebaseio.com",
  //   projectId: "digital-wallet-e6c62",
  //   storageBucket: "",
  //   messagingSenderId: "403818303909",
  //   appId: "1:403818303909:web:2949a97fff0871a2"
  // } 

  firebase: {
    apiKey: "AIzaSyBtKrYk_QTimHAD5Z5GWlpB_dutAcjufLo",
    authDomain: "digital-wallet-e6c62.firebaseapp.com",
    databaseURL: "https://digital-wallet-e6c62.firebaseio.com",
    projectId: "digital-wallet-e6c62",
    storageBucket: "",
    messagingSenderId: "403818303909"
  }
};
