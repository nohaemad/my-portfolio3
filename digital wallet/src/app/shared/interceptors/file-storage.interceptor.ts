import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class FileStorageInterceptor implements HttpInterceptor {

    constructor(public router: Router, private toastr: ToastrService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(retry(2),
            catchError((err: HttpErrorResponse) => {
                let ctType = request.headers.get("Content-Type");


                /******************** start search page **************/
                if (ctType && ctType == "application/json,uploadFile") {
                    if (err.status === 404) {
                        this.toastr.error(err.error.MessageAPI, '',
                            {
                                timeOut: 2000
                            });
                    } else if (err.status === 503) {
                        this.toastr.error(err.error.MessageAPI, '',
                            {
                                timeOut: 2000
                            });
                    } else {
                        this.toastr.error("System is under enhancements", '',
                            {
                                timeOut: 5000
                            });
                    }
                }

                /******************** end search page **************/

                /******************** start send tramsaction page **************/

                else if (ctType && ctType == "application/json,getFiles") {
                    if (err.status === 503) {

                        this.toastr.error(err.error.MessageAPI, '',
                            {
                                timeOut: 2000
                            });

                    } else {
                        this.toastr.error("System is under enhancements", '',
                            {
                                timeOut: 5000
                            });
                    }
                }
                /******************** end send tramsaction page **************/

                /************** start transactions page  **********************/

                else if (ctType && ctType == "application/json,deleteFile") {
                    if (err.status === 503) {
                        this.toastr.error(err.error.MessageAPI, '',
                            {
                                timeOut: 2000
                            });
                    } else {
                        this.toastr.error("System is under enhancements", '',
                            {
                                timeOut: 5000
                            });
                    }
                }

                /************** end transactions page  **********************/

                /************** start get balance page  **********************/

                else if (ctType && ctType == "application/json,downloadFile") {
                    if (err.status === 503) {
                        this.toastr.error(err.error.MessageAPI, '',
                            {
                                timeOut: 2000
                            });
                    } else {
                        this.toastr.error("System is under enhancements", '',
                            {
                                timeOut: 5000
                            });
                    }
                }

                /************** end get balance page  **********************/


                const error = err.error.message || err.statusText;
                return throwError(error);
            }
            ))
    }

}
