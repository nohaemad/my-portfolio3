import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class ServicesInterceptor implements HttpInterceptor {

    constructor(public router: Router, private toastr: ToastrService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(retry(2),
            catchError((err: HttpErrorResponse) => {
                let ctType = request.headers.get("Content-Type");

                /*********************** start services page ********************/

                if (ctType && ctType == "application/json,getServices") {

                    if (err.status === 503) {
                        if (err.error.MessageAPI === 'please enter your correct request ') {
                            this.toastr.error("An error has occurred while sending your request  please try later.", '',
                                {
                                    timeOut: 5000
                                });

                        } else if (err.error.MessageAPI === 'Invalid public key or password') {
                            this.toastr.error("please check your public key or password", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'not authorized') {
                            this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'your Account have been blocked') {
                            this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else {
                            this.toastr.error(err.error.MessageAPI, '',
                                {
                                    timeOut: 5000
                                });
                        }
                    }
                    else {
                        this.toastr.error("System is under enhancement", '',
                            {
                                timeOut: 5000
                            });
                    }
                }

                /*********************** end services page ********************/

                /*********************** start add service page ********************/

                else if (ctType && ctType == "application/json,addService") {

                    if (err.status === 503) {
                        if (err.error.MessageAPI === 'please enter your correct request ') {
                            this.toastr.error("An error has occurred while sending your request  please try later.", '',
                                {
                                    timeOut: 5000
                                });

                        } else if (err.error.MessageAPI === 'Invalid public key or password') {
                            this.toastr.error("please check your public key or password", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'Invalid BandWidth') {
                            this.toastr.error("", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'Invalid Duration') {
                            this.toastr.error("", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'you do not have balance for inoToken') {
                            this.toastr.error("Your balance is not enough please check your balance", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'Not Authorized') {
                            this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'your Account have been blocked') {
                            this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else {
                            this.toastr.error(err.error.MessageAPI, '',
                                {
                                    timeOut: 5000
                                });
                        }
                    }
                    else {
                        this.toastr.error("System is under enhancement", '',
                            {
                                timeOut: 5000
                            });
                    }
                }

                /*********************** end add service page ********************/

                /*********************** start get consumbtion page ********************/

                else if (ctType && ctType == "application/json,getConsumbtion") {

                    if (err.status === 503) {
                        if (err.error.MessageAPI === 'please enter your correct request ') {
                            this.toastr.error("An error has occurred while sending your request  please try later.", '',
                                {
                                    timeOut: 5000
                                });

                        } else if (err.error.MessageAPI === 'Invalid public key or password') {
                            this.toastr.error("please check your public key or password", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'Not Authorized') {
                            this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'your Account have been blocked') {
                            this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else {
                            this.toastr.error(err.error.MessageAPI, '',
                                {
                                    timeOut: 5000
                                });
                        }
                    }
                    else {
                        this.toastr.error("System is under enhancement", '',
                            {
                                timeOut: 5000
                            });
                    }
                }

                /*********************** end get consumbtion page ********************/

                /*********************** start purchase services page ********************/


                else if (ctType && ctType == "application/json,puchase_service") {

                    if (err.status === 503) {
                        if (err.error.MessageAPI === 'please enter your correct request ') {
                            this.toastr.error("An error has occurred while sending your request  please try later.", '',
                                {
                                    timeOut: 5000
                                });

                        } else if (err.error.MessageAPI === 'Invalid public key or password') {
                            this.toastr.error("please check your public key or password", '',
                                {
                                    timeOut: 5000
                                });
                        }else if (err.error.MessageAPI === 'service of this transaction is not valid') {
                            this.toastr.error("please check your private key file", '',
                                {
                                    timeOut: 5000
                                });
                        }else if (err.error.MessageAPI === 'you do not have balance for inoToken') {
                            this.toastr.error("Your balance is not enough please check your balance", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'Not Authorized') {
                            this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'your Account have been blocked') {
                            this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else {
                            this.toastr.error(err.error.MessageAPI, '',
                                {
                                    timeOut: 5000
                                });
                        }
                    }
                    else {
                        this.toastr.error("System is under enhancement", '',
                            {
                                timeOut: 5000
                            });
                    }
                }

                const error = err.error.message || err.statusText;
                return throwError(error);
            }
            ))
    }

}