import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class SettingsInterceptor implements HttpInterceptor {

    constructor(public router: Router, private toastr: ToastrService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(retry(2),
            catchError((err: HttpErrorResponse) => {
                let ctType = request.headers.get("Content-Type");

                /****************** start edit profile page  ****************/

                if (ctType && ctType == "application/json,editProfilePage") {
                    if (err.status === 503) {
                        if (err.error.MessageAPI === 'please enter your correct request') {
                            this.toastr.error('An error has occurred while sending your request please try later', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'Invalid Pasword') {
                            this.toastr.error("Check your password", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'Please Check Your data to help me to find your account') {
                            this.toastr.error("Please check your data", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'this user  registered and not confirmed') {
                            this.toastr.error("Please check your email and confirm it", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'this email registered and not confirmed') {
                            this.toastr.error("Please check your email for confirmation", '',
                                {
                                    timeOut: 5000
                                });

                        } else if (err.error.MessageAPI === 'this phone registered and not confirmed') {
                            this.toastr.error("Please check your email for confirmation", '',
                                {
                                    timeOut: 5000
                                });

                        } else if (err.error.MessageAPI === 'this userName  registered and not confirmed') {
                            this.toastr.error("Please check your email and confirm it", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'User Found') {
                            this.toastr.error("User is already exist", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'UserName Found') {
                            this.toastr.error("Username is already exist", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'Email Found') {
                            this.toastr.error("email is already exist", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'Phone Found') {
                            this.toastr.error("phone number is already exist", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'not authorized') {
                            this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'your Account have been blocked') {
                            this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else {
                            this.toastr.error(err.error.MessageAPI, '',
                                {
                                    timeOut: 5000
                                });
                        }
                    } else if (err.status === 404) {
                        if (err.error.MessageAPI === 'Please Check Your data to help me to find your account') {
                            this.toastr.error("Please check your data", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'please check your data') {
                            this.toastr.error("Please check your data", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'this user registered and not confirmed') {
                            this.toastr.error("Please check your email and confirm it", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'user  Found') {
                            this.toastr.error("User is already exist", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else {
                            this.toastr.error(err.error.MessageAPI, '',
                                {
                                    timeOut: 5000
                                });
                        }
                    } else {
                        this.toastr.error("System is under enhancements", '',
                            {
                                timeOut: 5000
                            });
                    }
                }

                /****************** end edit profile page  ****************/

                /****************** start deactivate page  ****************/

                else if (ctType && ctType == "application/json,deactivatePage") {
                    if (err.status === 503) {
                        if (err.error.MessageAPI === 'please enter your correct request') {
                            this.toastr.error('An error has occurred while sending your request please try later', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'Invalid PUblicKey') {
                            this.toastr.error("public key is not connect", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'not authorized') {
                            this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'your Account have been blocked') {
                            this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else {
                            this.toastr.error(err.error.MessageAPI, '',
                                {
                                    timeOut: 5000
                                });
                        }

                    }
                    else if (err.status === 404) {
                        if (err.error.MessageAPI === 'invalid UserNAme OR passsword') {
                            this.toastr.error("Please check username or password", '',
                                {
                                    timeOut: 5000
                                });
                        }

                        else {
                            this.toastr.error(err.error.MessageAPI, '',
                                {
                                    timeOut: 5000
                                });
                        }

                    } else {
                        this.toastr.error("System is under enhancements", '',
                            {
                                timeOut: 5000
                            });
                    }
                }

                /****************** end deactivate page  ****************/

                const error = err.error.message || err.statusText;
                return throwError(error);
            }
            ))
    }

}