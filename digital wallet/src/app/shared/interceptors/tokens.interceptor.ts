import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class TokensInterceptor implements HttpInterceptor {

    constructor(public router: Router, private toastr: ToastrService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(retry(2),
            catchError((err: HttpErrorResponse) => {
                let ctType = request.headers.get("Content-Type");

                /*********************** start add token page ********************/

                if (ctType && ctType == "application/json,addToken") {

                    if (err.status === 503) {
                        if (err.error.MessageAPI === 'please enter your correct request') {
                            this.toastr.error('An error has occurred while sending your request please try later', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'token name must be less than or equal 20 characters') {
                            this.toastr.error("Token name must be less than or equal 20 characters", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'token symbol must be 4 characters') {
                            this.toastr.error("Token symbol must be 4 characters", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'Icon URL is optiaonal if enter it must be less or equal 100 characters') {
                            this.toastr.error("Icon url must be less or equal 100 characters", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'Description is optiaonal if enter it must be less or equal 100 characters') {
                            this.toastr.error("Description must be less or equal 100 characters", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'please enter initiator address') {
                            this.toastr.error("please enter public key", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'please enter valid initiator address') {
                            this.toastr.error("please enter public key", '',
                                {
                                    timeOut: 5000
                                });
                        }  else if (err.error.MessageAPI === 'The given password is incorrect.') {
                            this.toastr.error("Please check your password", '',
                                {
                                    timeOut: 5000
                                });
                        }else if (err.error.MessageAPI === 'please enter Tokens Total Supply more than zero') {
                            this.toastr.error("Token total supply must be more than zero", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'please enter Tokens value more than zero') {
                            this.toastr.error("Token value must be more than zero", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'please enter Precision range from 0 to 5 ') {
                            this.toastr.error("Token precision must be between from 0 to 5 ", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'please enter UsageType security or utility') {
                            this.toastr.error("please choose token usage type security or utility", '',
                                {
                                    timeOut: 5000
                                });
                        } 
                        else if (err.error.MessageAPI === "this public key is not Associated with any account") {
                            this.toastr.error("Please check user public key", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'enter ContractID must be less than or equal 60 characters') {
                            this.toastr.error('Contract id must be less than or equal 60 characters', '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'enter the potential users public keys which can use this token') {
                            this.toastr.error("Please add public key for each potential user", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'please enter TokenType is public  or private') {
                            this.toastr.error("please choose token type", '',
                                {
                                    timeOut: 5000
                                });
                        } 
                        else if (err.error.MessageAPI === "Token ID don't exist before") {
                            this.toastr.error("Token id is not exist already", '',
                                {
                                    timeOut: 5000
                                });
                        }else if (err.error.MessageAPI === 'your balance amount less than total amount of tokens') {
                            this.toastr.error("Your balance is not enough please check your balance", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'The application refused to create token.') {
                            this.toastr.error("The application refused to create token.", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'token name exist before') {
                            this.toastr.error("Token name is exist already", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'token symbol exist before') {
                            this.toastr.error("Token symbol is exist already", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'not authorized') {
                            this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'your Account have been blocked') {
                            this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                                {
                                    timeOut: 5000
                                });
                        }

                        else {
                            this.toastr.error(err.error.MessageAPI, '',
                                {
                                    timeOut: 5000
                                });
                        }
                    }
                    else {
                        this.toastr.error("System is under enhancements", '',
                            {
                                timeOut: 5000
                            });
                    }
                }

                /*********************** end add token page ********************/

                /*********************** start refund token page ********************/

                else if (ctType && ctType == "application/json,refundToken") {

                    if (err.status === 503) {
                        if (err.error.MessageAPI === 'please enter your correct request ') {
                            this.toastr.error("An error has occurred while sending your request please try later.", '',
                                {
                                    timeOut: 5000
                                });
                        }

                        else if (err.error.MessageAPI === 'please make sure amount is more than zero') {
                            this.toastr.error("Amount must be bigger than zero, please check the amount", '',
                                {
                                    timeOut: 5000
                                });
                        }

                        else if (err.error.MessageAPI === 'please enter required data Sender PK, TokenID, Amount, Time and Signature') {
                            this.toastr.error("sender public key , token name , amount time and private key is required", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'please enter the receiver public key in order to refund flat currency') {
                            this.toastr.error("Please enter the receiver public key", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'token ID must be equal to 13 characters') {
                            this.toastr.error("Token id must be equal 13 characters", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'Sender sign must be less than 200 and more than 100 characters') {
                            this.toastr.error("signature must be between 150 and 200", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === "please check your transaction's time") {
                            this.toastr.error("Please check your time", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === "Please check the transaction's amount decimals,maximum number of decimals is 6") {
                            this.toastr.error("Amount must be decimal maximum number of decimals is 6 digits", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'please, check the accounts they should be active') {
                            this.toastr.error("The user is not active please check the user", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'You are not allowed to do this transaction') {
                            this.toastr.error("Please check private key file", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === "the receiver's account is not active.") {
                            this.toastr.error("The user is not active please check the user", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'not authorized') {
                            this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'your Account have been blocked') {
                            this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                                {
                                    timeOut: 5000
                                });
                        }

                        else {
                            this.toastr.error(err.error.MessageAPI, '',
                                {
                                    timeOut: 5000
                                });
                        }
                    }
                    else {
                        this.toastr.error("System is under enhancements", '',
                            {
                                timeOut: 5000
                            });
                    }
                }

                /*********************** end refund token page ********************/

                /*********************** start edit token page ********************/

                else if (ctType && ctType == "application/json,editToken") {

                    if (err.status === 503) {
                        if (err.error.MessageAPI === 'please enter your correct request ') {
                            this.toastr.error("An error has occurred while sending your request  please try later.", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'please enter Tokens Total Supply more than zero') {
                            this.toastr.error("Token total supply must be more than zero", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'please enter valid initiator address (Public key)') {
                            this.toastr.error("Please check your public key", '',
                                {
                                    timeOut: 5000
                                });
                        } 
                        else if (err.error.MessageAPI === 'The given password is incorrect.') {
                            this.toastr.error("Please check your password", '',
                                {
                                    timeOut: 5000
                                });
                        }   else if (err.error.MessageAPI === 'please enter Tokens value more than zero') {
                            this.toastr.error("Token value must be more than zero", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'please enter Precision range from 0 to 5') {
                            this.toastr.error("Token precision must be between from 0 to 5", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'enter ContractID must be less than or equal 60 characters ') {
                            this.toastr.error("Contract id must be less than or equal 60 characters", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'enter the potential users public keys which can use this token') {
                            this.toastr.error("Please add users public key", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'please enter TokenType is public  or private') {
                            this.toastr.error("please choose token type", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === "it rejects the update: there are token holders unlike this user.") {
                            this.toastr.error("your token is already used by other users.", '',
                                {
                                    timeOut: 5000
                                });

                        } else if (err.error.MessageAPI === 'it rejects the update: the user have not balance covers the new increase with the current price to the token.') {
                            this.toastr.error("you don’t have sufficient balance for this operation", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'it rejects the update: the token has association with any contract.') {
                            this.toastr.error("the token is already managed using a contract", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'please enter Contract ID associated with public token') {
                            this.toastr.error("please enter Contract ID can use this token", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'The application refused to update token.') {
                            this.toastr.error("An error has occurred while updating this token. Please try later", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'not authorized') {
                            this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'your Account have been blocked') {
                            this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                                {
                                    timeOut: 5000
                                });
                        }

                        else {
                            this.toastr.error(err.error.MessageAPI, '',
                                {
                                    timeOut: 5000
                                });
                        }
                    }
                    else {
                        this.toastr.error("System is under enhancements", '',
                            {
                                timeOut: 5000
                            });
                    }
                }

                /*********************** start edit token page ********************/

                /*********************** start tokens page ********************/

                else if (ctType && ctType == "application/json,getTokens") {
                    if (err.status === 503) {
                        if (err.error.MessageAPI === 'please enter your correct request ') {
                            this.toastr.error("An error has occurred while sending your request  please try later.", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'The Account with this Public key is not found.') {
                            this.toastr.error("Please check your public key", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'The given password is incorrect.') {
                            this.toastr.error("Please check your public password", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'The user has not create any token or use any public one.') {
                            this.toastr.error("You do not have any tokens", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'your Account have been blocked') {
                            this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                                {
                                    timeOut: 5000
                                });
                        }

                        else {
                            this.toastr.error(err.error.MessageAPI, '',
                                {
                                    timeOut: 5000
                                });
                        }
                    }
                    else {
                        this.toastr.error("System is under enhancements", '',
                            {
                                timeOut: 5000
                            });
                    }
                }

                /*********************** end tokens page ********************/

                const error = err.error.message || err.statusText;
                return throwError(error);
            }
            ))
    }

}