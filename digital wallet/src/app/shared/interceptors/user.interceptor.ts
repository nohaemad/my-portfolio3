import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class UserInterceptor implements HttpInterceptor {

  constructor(public router: Router, private toastr: ToastrService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(retry(2),
      catchError((err: HttpErrorResponse) => {
        let ctType = request.headers.get("Content-Type");

        /****************************** start login page *********************************/

        if (ctType && ctType == "application/json,loginPage") {
          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'please Enter your password  Or Authvalue') {
              this.toastr.error('Please check your account authentication value or password', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Please,Check your account Email or password') {
              this.toastr.error('Account not found please check your email or password', '',
                {
                  timeOut: 5000
                });
            }

            else if (err.error.MessageAPI === 'Account not found pleas check your email or phone') {
              this.toastr.error('Account not found please check your email or phone', '',
                {
                  timeOut: 5000
                });
            }


            else if (err.error.MessageAPI === 'Please,Check your account Email or AuthenticationValue') {
              this.toastr.error('Please check your account email or password', '',
                {
                  timeOut: 5000
                });
            }

            else if (err.error.MessageAPI === 'Please,Check your account  phoneNAmber OR password') {
              this.toastr.error('Please check your account phone number or password', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Please,Check your account  phoneNAmber OR AuthenticationValue') {
              this.toastr.error('Please check your account phone number or password', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'change your sessionId') {
              this.toastr.error('Server uses cookies which are disabled on your browser', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else{
              this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 5000
              });
            }
          }
          else if (err.status === 404) {
            if (err.error.MessageAPI === 'please Enter your Email Or Phone') {
              this.toastr.error('Account is not found please check email or phone', '',
                {
                  timeOut: 5000
                });
            } else if (err.error.MessageAPI === 'Please,Check your account Email') {
              this.toastr.error('Please check your account Email', '',
                {
                  timeOut: 5000
                });
            } else if (err.error.MessageAPI === 'Please Check your account phone') {
              this.toastr.error('Please check your phone number', '',
                {
                  timeOut: 5000
                });
            }
            else{
              this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 5000
              });
            }

          } else {
            this.toastr.error("System is under enhancements", '',
              {
                timeOut: 5000
              });
          }
        }

        /****************************** start login page *********************************/

        /****************************** start save public key function *********************************/

        if (ctType && ctType == "application/json,save_public_key") {

          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'not authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'failed to get this email plz check if account exist an make sure to insert the right pass and email!') {
              this.toastr.error('Please check if account exist and make sure to insert the right password and email!', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'this public key exists before') {
              this.toastr.error('Please check your public key', '',
                {
                  timeOut: 5000
                });
            }
            else{
              this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 5000
              });
            }
          } else {
            this.toastr.error("System is under enhancements", '',
              {
                timeOut: 5000
              });
          }
        }

        /****************************** end save public key function *********************************/

        /****************************** start register page *********************************/
        else if (ctType && ctType == "application/json,registerPage") {
          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'not authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }

            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'this user  registered and not confirmed') {
              this.toastr.error("Please check your email and confirm your previous registration.", '',
                {
                  timeOut: 5000
                });
            } else if (err.error.MessageAPI === 'this email registered and not confirmed') {
              this.toastr.error("Please check your email for confirmation", '',
                {
                  timeOut: 5000
                });

            } else if (err.error.MessageAPI === 'this phon registered and not confirmed') {
              this.toastr.error("Please check your phone for confirmation", '',
                {
                  timeOut: 5000
                });
            } else if (err.error.MessageAPI === 'this userName  registered and not confirmed') {
              this.toastr.error("Please check your email for confirmation", '',
                {
                  timeOut: 5000
                });
            } else if (err.error.MessageAPI === 'user  Found') {
              this.toastr.error("User is already exist", '',
                {
                  timeOut: 5000
                });
            } else if (err.error.MessageAPI === 'UserName Found') {
              this.toastr.error("Username is already exist", '',
                {
                  timeOut: 5000
                });
            } else if (err.error.MessageAPI === 'Email Found') {
              this.toastr.error("email is already exist", '',
                {
                  timeOut: 5000
                });
            } else if (err.error.MessageAPI === 'Phone Found') {
              this.toastr.error("phone number is already exist", '',
                {
                  timeOut: 5000
                });
            } else if (err.error.MessageAPI === 'Please, check your  data') {
              this.toastr.error("Please check your  data", '',
                {
                  timeOut: 5000
                });
            } else if (err.error.MessageAPI === 'Please Check Your data to help me to find your account') {
              this.toastr.error("Please check your data", '',
                {
                  timeOut: 5000
                });
            }
          
            else{
              this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 5000
              });
            }
          }
          else {
            this.toastr.error("System is under enhancements", '',
              {
                timeOut: 5000
              });
          }
        }
        else if (ctType && ctType == "application/json,forgetPasswodPage") {
          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }

            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'invalid Email Or Phone') {
              this.toastr.error("Account not found please check your account email", '',
                {
                  timeOut: 5000
                });
            }
            else{
              this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 5000
              });
            }
          } else {
            this.toastr.error("System is under enhancements", '',
              {
                timeOut: 5000
              });
          }
        }

        else if (ctType && ctType == "application/json,resetPasswodPage") {
          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'invalid Data') {
              this.toastr.error("Please check the code", '',
                {
                  timeOut: 5000
                });
            } else if (err.error.MessageAPI === 'Time out') {
              this.toastr.error("An error has occurred while sending your request  please try later.", '',
                {
                  timeOut: 5000
                });
            }
            else{
              this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 5000
              });
            }
          } else if (err.status === 404) {
            if (err.error.MessageAPI === 'invalid password length') {
              this.toastr.error("Password must contain a minimum of 10 characters (numbers, letters and special characters)", '',
                {
                  timeOut: 5000
                });
            }
            else{
              this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 5000
              });
            }

          } else {
            this.toastr.error("System is under enhancements", '',
              {
                timeOut: 5000
              });
          }
        }
        const error = err.error.message || err.statusText;
        return throwError(error);
      }
      ))
  }

}