import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class AccountInterceptor implements HttpInterceptor {

    constructor(public router: Router, private toastr: ToastrService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(retry(2),
            catchError((err: HttpErrorResponse) => {
                let ctType = request.headers.get("Content-Type");


                /******************** start search page **************/
                if (ctType && ctType == "application/json,searchPage") {
                    if (err.status === 404) {
                        if (err.error.MessageAPI === 'please enter your correct request') {
                            this.toastr.error('An error has occurred while sending your request please try later', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'please check your username and password') {
                            this.toastr.error("please check your username and password", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'I canot find user using this property, enter anthor---*') {
                            this.toastr.error("The user is not exist please check the username", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else {
                            this.toastr.error(err.error.MessageAPI, '',
                                {
                                    timeOut: 5000
                                });
                        }
                    } else if (err.status === 503) {
                        if (err.error.MessageAPI === 'This user is not active') {
                            this.toastr.error("The user is not active please check the user", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'not authorized') {
                            this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'your Account have been blocked') {
                            this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else {
                            this.toastr.error(err.error.MessageAPI, '',
                                {
                                    timeOut: 5000
                                });
                        }
                    } else {
                        this.toastr.error("System is under enhancements", '',
                            {
                                timeOut: 5000
                            });
                    }
                }

                /******************** end search page **************/

                /******************** start send tramsaction page **************/

                else if (ctType && ctType == "application/json,sendPage") {
                    if (err.status === 503) {
                        if (err.error.MessageAPI === 'please enter your correct request') {
                            this.toastr.error('An error has occurred while sending your request please try later', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'You are not allowed to do this transaction') {
                            this.toastr.error("Please check the time or check private key", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'please, check the account balance') {
                            this.toastr.error("You are not active", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'please, check the accounts they should be active') {
                            this.toastr.error("The user is not active", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'there was a double Spend transaction') {
                            this.toastr.error("", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === "please check your transaction's time") {
                            this.toastr.error("Please check your time", '',
                                {
                                    timeOut: 5000
                                });
                        }

                        else if (err.error.MessageAPI === 'please make sure amount is more than zero') {
                            this.toastr.error("Amount must be more than zero please check it", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'please enter required data Sender PK, Receiver PK,TokenID,Amount , Sender, Signature') {
                            this.toastr.error("Sender public key , token name , amount time and private key is required", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'Please check the sender address') {
                            this.toastr.error("Please check your public key", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === "Sender public key can't be the same as Receiver public key") {
                            this.toastr.error("", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === "Sender sign must be more than 100 and less than 200 characters") {
                            this.toastr.error("Signature must be more than 100 characters", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === "the receiver is not  allowed to use this token") {
                            this.toastr.error("", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === "Please check the transaction's amount decimals, maximum number of decimals is 6") {
                            this.toastr.error("Amount must be decimal maximum number of decimals is 6 digits", '',
                                {
                                    timeOut: 5000
                                });
                        }


                        else if (err.error.MessageAPI === 'not authorized') {
                            this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'your Account have been blocked') {
                            this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else {
                            this.toastr.error(err.error.MessageAPI, '',
                                {
                                    timeOut: 5000
                                });
                        }
                    } else {
                        this.toastr.error("System is under enhancements", '',
                            {
                                timeOut: 5000
                            });
                    }
                }
                /******************** end send tramsaction page **************/

                /************** start transactions page  **********************/

                else if (ctType && ctType == "application/json,getTrasactionPage") {
                    if (err.status === 503) {
                        if (err.error.MessageAPI === 'The Account with this Public key is not found.') {
                            this.toastr.error("Please check your public key", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'The Account with this Public key is not active.') {
                            this.toastr.error("Your account is not active", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'The given password is incorrect.') {
                            this.toastr.error("Please check your public password", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'not authorized') {
                            this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'your Account have been blocked') {
                            this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else {
                            this.toastr.error(err.error.MessageAPI, '',
                                {
                                    timeOut: 5000
                                });
                        }
                    } else {
                        this.toastr.error("System is under enhancements", '',
                            {
                                timeOut: 5000
                            });
                    }
                }

                /************** end transactions page  **********************/

                /************** start get balance page  **********************/

                else if (ctType && ctType == "application/json,getBalancePage") {
                    if (err.status === 503) {
                        if (err.error.MessageAPI === 'please enter your correct request') {
                            this.toastr.error('An error has occurred while sending your request please try later', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        if (err.error.MessageAPI === 'The Account with this Public key is not found.') {
                            this.toastr.error("Please check your public key", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'The Account with this Public key is not active.') {
                            this.toastr.error("Your account is not active", '',
                                {
                                    timeOut: 5000
                                });
                        } else if (err.error.MessageAPI === 'The given password is incorrect.') {
                            this.toastr.error("Please check your password", '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'not authorized') {
                            this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else if (err.error.MessageAPI === 'your Account have been blocked') {
                            this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                                {
                                    timeOut: 5000
                                });
                        }
                        else {
                            this.toastr.error(err.error.MessageAPI, '',
                                {
                                    timeOut: 5000
                                });
                        }
                    } else {
                        this.toastr.error("System is under enhancements", '',
                            {
                                timeOut: 5000
                            });
                    }
                }

                /************** end get balance page  **********************/


                const error = err.error.message || err.statusText;
                return throwError(error);
            }
            ))
    }

}
