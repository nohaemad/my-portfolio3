import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment'
import { EnvService } from '../../../env.service';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  // define url in environment.ts
  api_url: string = environment.apiUrl;
  // define old password hash
  hash_old_password;
  // define hash password
  hash_password;
  // define Http Options
  httpOptions = {};

  // define constructor for http client and link of env
  constructor(private http: HttpClient, private env: EnvService) {
    // define url in env.js 
    this.api_url = this.env.apiUrl;
  }


  /****************** start edit profile **************/

  editUserInfo(user): Observable<HttpResponse<object>> {

    // define user
    console.log('%(_) ' + JSON.stringify(user));

    // define request of edit profile api
    var sendObject = {
      Account: {
        //define name
        AccountName: user.name,
        //define password
        AccountPassword: user.password,
        //define email 
        AccountEmail: user.emailAdd,
        // define phone
        AccountPhoneNumber: user.phone,
        //define address
        AccountAddress: user.address,
        //define public key
        AccountPublicKey: user.public_key,
        //define authentication type
        AccountAuthenticationType: '',
        //define authentication value
        AccountAuthenticationValue: ''
      },

      //define old password
      Oldpassword: user.oldPassword
    };

    // print request of edit profile api
    console.log(JSON.stringify(sendObject));

    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,editProfilePage',
        // define jwt token
        'jwt-token': user.jwt_token
      })
    }

    //define the function of edit profile api
    return this.http.put<HttpResponse<object>>(this.api_url + '/28e773082adf7aa0e4f76e8', JSON.stringify(sendObject), this.httpOptions)
      .pipe(
        retry(1),
        catchError(
          this.errorHandler,
        )
      );
  }

  /****************** end edit profile **************/

  /****************** start deactivate account **************/

  deactivateAccount(user): Observable<HttpResponse<object>> {

    // define request of deactivate account api
    var sendObject = {
      //define name
      UserName: user.name,
      //define public key
      PublicKey: user.public_key,
      //define reason
      DeactivationReason: user.reason,
      //define password
      Password: user.password,
    };

    // print request of deactivate account api
    console.log("deactivate account : " + JSON.stringify(sendObject));

    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,deactivatePage',
        // define jwt token
        'jwt-token': user.jwt_token
      })
    }

    //define the function of deactivate account api
    return this.http.post<HttpResponse<object>>(this.api_url + "/06623be7673e8a781bffed9", JSON.stringify(sendObject), this.httpOptions)
      .pipe(
        retry(1),
        catchError(
          this.errorHandler,
        ));
  }

  /****************** end deactivate account **************/

  /****************** start activate account **************/

  activateAccount(user): Observable<HttpResponse<object>> {

    // define request of activate account api
    var sendObject = {
      //define username
      UserName: user.name,
      //define public key
      PublicKey: user.public_key,
      //define deactivate reason
      DeactivationReason: '',
      //define password
      Password: user.password,
    };

    // print request of activate account api
    console.log("activate account : " + JSON.stringify(sendObject));

    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,deactivatePage',
        // define jwt token
        'jwt-token': user.jwt_token
      })
    }

    //define the function of activate account api
    return this.http.post<HttpResponse<object>>(this.api_url + "/06623be7673e8a781bffed9", JSON.stringify(sendObject), this.httpOptions)
      .pipe(
        retry(1),
        catchError(
          this.errorHandler,
        ));
  }

  /****************** end activate account **************/

  /****************** start error  **********/

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || "server Error");
  }

  /****************** end error  **********/
}
