import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { EnvService } from '../../../env.service';
import { retry, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  // define url in environment.ts
  api_url: string = environment.apiUrl;
  // define Http Options
  httpOptions = {};

  jwt_token = JSON.parse(localStorage.getItem('jwt_token'));

  // define constructor for http client and link of env
  constructor(private http: HttpClient, private env: EnvService) {
    // define url in env.js 
    this.api_url = this.env.apiUrl;
  }

  /****************** start get services **************/

  getServices(service): Observable<HttpResponse<Object>> {

    // define request of get services api
    var sendObject = {
      //define user password
      Password: service.password,
      // define user public key
      PublicKey: service.public_key
    };

    // print request of get services api
    console.log(JSON.stringify(sendObject));
    // define http header
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,getServices',
        // define jwt token
        'jwt-token': service.jwt_token
      })
    }

    //define the function of get services api

    return this.http.post<HttpResponse<object>>(this.api_url + '/1acbf975e4f59bffae27ce43',
      JSON.stringify(sendObject),
      this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandler));
  }

  /****************** end get services **************/

  /****************** start add service **************/

  addService(service) {

    // define request of add service api
    var sendObject = {
      // define service type
      SeviceType: service.type,
      // define mega bytes
      Mbytes: service.Mbytes,
      // define amount
      Amount: service.amount,
      // define duration
      Duration: parseInt(service.Duration),
      // define day
      Day: service.day,
      // define bandwidth
      bandwidth: service.bandwidth,
      // define password
      Password: service.password,
      // define public key
      PublicKey: service.public_key
    };

    // print request of add service api
    console.log(JSON.stringify(sendObject));


    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,addService',
        // define jwt token
        'jwt-token': service.jwt_token
      })
    }

    //define the function of add service api

    return this.http.post<HttpResponse<object>>(this.api_url + '/b512e6d3fb3e7015880d275b',
      JSON.stringify(sendObject),
      this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandler));
  }

  /****************** end add service **************/

  /****************** start purchase service **************/

  puchase_service(details) {

    // define request of purchase service api
    var sendObject = {
      // define service id
      ID: details.service_id,
      // define password
      Password: details.password,
      Transactionobj: {
        // define sender public key
        Sender: details.Sender,
        // define receiver public key
        Receiver: details.reciever,
        // define token id
        TokenID: "0000000000000",
        // define amount
        Amount: parseFloat(details.Amount),
        // define time
        Time: details.timeZone,
        // define signature
        Signature: details.Hash
      }
    };

    // print request of purchase service api
    console.log(JSON.stringify(sendObject));

    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,puchase_service',
        // define jwt token
        'jwt-token': details.jwt_token
      })
    }

    //define the function of purchase service api

    return this.http.post<HttpResponse<object>>(this.api_url + '/29545d292781d573475ad68c',
      JSON.stringify(sendObject),
      this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandler));
  }

  /****************** end purchase service **************/

  /****************** start get consumbtion **************/

  getConsumbtion(details) {

    // define request of get consumbtion api
    var sendObject = {
      // define voucher id
      VoucherID: details.id,
      // define user password
      Password: details.password,
      // define user public key
      PublicKey: details.public_key
    };

    // print request of get consumbtion api
    console.log(JSON.stringify(sendObject));

    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,getConsumbtion',
        // define jwt token
        'jwt-token': details.jwt_token
      })
    }

    //define the function of get consumbtion api

    return this.http.post<HttpResponse<object>>(this.api_url + '/b64c260d1b9e1e044f397c50',
      JSON.stringify(sendObject),
      this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandler));
  }

  /****************** end get consumbtion  **********/

  /****************** start error handle  **********/

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || "server Error");
  }

  /****************** end error handle  **********/

}
