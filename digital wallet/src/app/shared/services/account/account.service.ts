import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { Observable } from 'rxjs/Rx'

import { environment } from '../../../../environments/environment';
import { retry, catchError } from 'rxjs/operators';
import { EnvService } from '../../../env.service';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  // define url in environment.ts
  api_url: string = environment.apiUrl;
  // define Http Options
  httpOptions = {};
  // define jwt token 
  token: string;

  jwt_token = JSON.parse(localStorage.getItem('jwt_token'));


  // define constructor for http client and link of env
  constructor(private http: HttpClient, private env: EnvService) {
    // define url in env.js 
    this.api_url = this.env.apiUrl;
  }

  /****************** start seaech **************/

  search(search): Observable<HttpResponse<Object>> {

    // define request of search api
    var sendObject = {
      // define text of search
      TextSearch: search.searchText,
      Account: {
        // define username
        AccountName: search.name,
        // define password
        AccountPassword: search.password
      }
    };

    // print request of search api
    console.log(JSON.stringify(sendObject));

    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,searchPage',
        // define jwt token
        'jwt-token': search.jwt_token

      })
    }

    console.log(this.jwt_token);
    //define the function of search api
    return this.http.post<HttpResponse<object>>(this.api_url + '/6c954f53e30ceed92348ed9', JSON.stringify(sendObject), this.httpOptions)
      .catch(this.errorHandler);
  }

  /****************** end seaech **************/

  /****************** start seaech2 **************/

  search2(search): Observable<HttpResponse<Object>> {

    // define request of search api
    var sendObject = {
      // define text of search
      TextSearch: search.searchText2,
      Account: {
        // define username
        AccountName: search.name,
        // define password
        AccountPassword: search.password,

      }
    };

    // print request of search api
    console.log(JSON.stringify(sendObject));

    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,searchPage',
        // define jwt token
        'jwt-token': search.jwt_token
      })
    }

    //define the function of search api
    return this.http.post<HttpResponse<object>>(this.api_url + '/6c954f53e30ceed92348ed9', JSON.stringify(sendObject), this.httpOptions)
      .catch(this.errorHandler);
  }

  /****************** end seaech2 **************/

  /****************** start send transaction  **********/

  sendBalance(balanceDetails): Observable<HttpResponse<Object>> {

    // // define request of send transaction api
    // var sendObject = {
    //   // define receiver public key
    //   Receiver: balanceDetails.reciever,
    //   // define time
    //   Time: balanceDetails.timeZone,
    //   // define sender public key
    //   Sender: balanceDetails.Sender,
    //   // define amount
    //   Amount: parseFloat(balanceDetails.Amount),
    //   // define signature
    //   Signature: balanceDetails.Hash,
    //   //define token id
    //   TokenID: balanceDetails.token
    // };

    // define user token 
    this.token = JSON.parse(localStorage.getItem("userToken"));

    var sendObject = {
      EncryptedData: balanceDetails.encrypt_data,
      SessionID: this.token
    }

    // print request of send transaction api
    console.log(JSON.stringify(sendObject));

    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,sendPage',
        // define jwt token
        'jwt-token': balanceDetails.jwt_token
      })
    }

    //define the function of send transaction api
    return this.http.post<HttpResponse<object>>(this.api_url + '/2e4a9d667ad5e3cef02eae9', JSON.stringify(sendObject), this.httpOptions)

      .catch(this.errorHandler)
      .pipe(
        retry(1),
        catchError(this.errorHandler));
  }

  /****************** end send transaction  **********/

  /****************** start  get balance***********************/

  getBalance(balance) {

    // define request of get balance api
    var sendObject = {
      // define user public key
      PublicKey: balance.public_key,
      //define user password
      Password: balance.password
    };
    // print request of get balance api
    console.log(JSON.stringify(sendObject));
    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json',
        // define jwt token
        'jwt-token': balance.jwt_token
      })
    }

    //define the function of get balance api
    return this.http.post<HttpResponse<object>>(this.api_url + '/d1c22221807844b6515d806', JSON.stringify(sendObject), this.httpOptions)
      .pipe(
        retry(10),
        catchError(this.errorHandler)
      );

  }

  /****************** end  get balance***********************/

  /****************** start  get transactions***********************/

  getTransactions(transaction) {

    // define request of get transactions api
    var sendObject = {
      // define user public key
      PublicKey: transaction.public_key,
      //define user password
      Password: transaction.password,
    };

    // print request of get transcations api
    console.log(JSON.stringify(sendObject));
    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,getTrasactionPage',
        // define jwt token
        'jwt-token': transaction.jwt_token
      })
    }

    //define the function of get transcations api
    return this.http.post<HttpResponse<object>>(this.api_url + '/46cda9a3c810848b8141sfk', JSON.stringify(sendObject), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandler)
      );
  }

  /****************** end  get balance***********************/

  /****************** start  get validator public key ***********************/
  getValidator_publickey(transaction) {

    // define request of get transactions api
    var sendObject = {
      // define user public key
      AccountName: transaction.name,
      //define user password
      AccountPassword: transaction.password,
      AccountPublicKey : transaction.public_key
    };

    // print request of get transcations api
    console.log(JSON.stringify(sendObject));
    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,getValidator_publickey',
        // define jwt token
        'jwt-token': transaction.jwt_token
      })
    }

    //define the function of get transcations api
    return this.http.post<HttpResponse<object>>(this.api_url + '/ef3d81592439a480ddea0647', JSON.stringify(sendObject), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandler)
      );
  }
  /****************** end  get validator public key ***********************/

  /****************** start error  **********/


  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || "server Error");
  }

  /****************** end error  **********/

}
