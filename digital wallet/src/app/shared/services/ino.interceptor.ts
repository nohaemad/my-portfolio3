import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class InoInterceptor implements HttpInterceptor {

  constructor(public router: Router, private toastr: ToastrService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(retry(2),
      catchError((err: HttpErrorResponse) => {
        let ctType = request.headers.get("Content-Type");
        if (ctType && ctType == "application/json,loginPage") {
          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else {
              this.toastr.error(err.error.MessageAPI, '',
                {
                  timeOut: 2000
                });
            }
          }
          else if (err.status === 404) {
            this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 2000
              });
          } else {
            this.toastr.error("Request timeout", '',
              {
                timeOut: 2000
              });
          }
        }
        if (ctType && ctType == "application/json,save_public_key") {
          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else {
              this.toastr.error(err.error.MessageAPI, '',
                {
                  timeOut: 2000
                });
            }
          }
          else if (err.status === 404) {
            this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 2000
              });
          } else {
            this.toastr.error("Request timeout", '',
              {
                timeOut: 2000
              });
          }
        }
        else if (ctType && ctType == "application/json,registerPage") {

          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else {
              this.toastr.error(err.error.MessageAPI, '',
                {
                  timeOut: 2000
                });
            }
          }
          else {
            this.toastr.error("Request timeout", '',
              {
                timeOut: 2000
              });
          }
        }
        else if (ctType && ctType == "application/json,forgetPasswodPage") {
          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else {
              this.toastr.error(err.error.MessageAPI, '',
                {
                  timeOut: 2000
                });
            }
          } else {
            this.toastr.error("Request timeout", '',
              {
                timeOut: 2000
              });
          }
        }

        else if (ctType && ctType == "application/json,resetPasswodPage") {
          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else {
              this.toastr.error(err.error.MessageAPI, '',
                {
                  timeOut: 2000
                });
            }
          } else if (err.status === 404) {
            this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 2000
              });

          } else {
            this.toastr.error("Request timeout", '',
              {
                timeOut: 2000
              });
          }
        }

        else if (ctType && ctType == "application/json,searchPage") {
          if (err.status === 404) {
            this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 2000
              });
          } else if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else {
              this.toastr.error(err.error.MessageAPI, '',
                {
                  timeOut: 2000
                });
            }
          } else {
            this.toastr.error("Request timeout", '',
              {
                timeOut: 2000
              });
          }
        }

        else if (ctType && ctType == "application/json,sendPage") {
          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else {
              this.toastr.error(err.error.MessageAPI, '',
                {
                  timeOut: 2000
                });
            }
          } else {
            this.toastr.error("Request timeout", '',
              {
                timeOut: 2000
              });
          }
        }

        else if (ctType && ctType == "application/json,editProfilePage") {
          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else {
              this.toastr.error(err.error.MessageAPI, '',
                {
                  timeOut: 2000
                });
            }
          } else if (err.status === 404) {
            this.toastr.error(err.error.MessageAPI);

          } else {
            this.toastr.error("Request timeout", '',
              {
                timeOut: 2000
              });
          }
        }

        else if (ctType && ctType == "application/json,deactivatePage") {
          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else{
            this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 2000
              });
            }
          } else if (err.status === 404) {
            this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 2000
              });

          } else {
            this.toastr.error("Request timeout", '',
              {
                timeOut: 2000
              });
          }
        }

        else if (ctType && ctType == "application/json,getTrasactionPage") {
          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else{
            this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 2000
              });
            }
          } else if (err.status === 404) {
            this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 2000
              });

          } else {
            this.toastr.error("Request timeout", '',
              {
                timeOut: 2000
              });
          }
        }

        else if (ctType && ctType == "application/json,getBalancePage") {
          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else{
            this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 2000
              });
            }
          } else if (err.status === 404) {
            this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 2000
              });

          } else {
            this.toastr.error("Request timeout", '',
              {
                timeOut: 2000
              });
          }
        }


        else if (ctType && ctType == "application/json,addToken") {

          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else{
            this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 2000
              });
            }
          }
          else {
            this.toastr.error("Request timeout", '',
              {
                timeOut: 2000
              });
          }
        }
        else if (ctType && ctType == "application/json,refundToken") {

          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else{
            this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 2000
              });
            }
          }
          else {
            this.toastr.error("Request timeout", '',
              {
                timeOut: 2000
              });
          }
        }
        else if (ctType && ctType == "application/json,editToken") {

          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else{
            this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 2000
              });
            }
          }
          else {
            this.toastr.error("Request timeout", '',
              {
                timeOut: 2000
              });
          }
        }
        else if (ctType && ctType == "application/json,getServices") {

          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else{
            this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 2000
              });
            }
          }
          else {
            this.toastr.error("Request timeout", '',
              {
                timeOut: 2000
              });
          }
        }

        else if (ctType && ctType == "application/json,addService") {

          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else{
            this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 2000
              });
            }
          }
          else {
            this.toastr.error("Request timeout", '',
              {
                timeOut: 2000
              });
          }
        }

        else if (ctType && ctType == "application/json,getConsumbtion") {

          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else{
            this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 2000
              });
            }
          }
          else {
            this.toastr.error("Request timeout", '',
              {
                timeOut: 2000
              });
          }
        }

        else if (ctType && ctType == "application/json,puchase_service") {

          if (err.status === 503) {
            if (err.error.MessageAPI === 'please enter your correct request') {
              this.toastr.error('An error has occurred while sending your request please try later', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'your Account have been blocked') {
              this.toastr.error('Your account blocked, please wait 30 minutes and try again', '',
                {
                  timeOut: 5000
                });
            }
            else if (err.error.MessageAPI === 'Not Authorized') {
              this.toastr.error('Your session time has ended please login again for continue using your wallet', '',
                {
                  timeOut: 5000
                });
            }
            else{
            this.toastr.error(err.error.MessageAPI, '',
              {
                timeOut: 2000
              });
            }
          }
          else {
            this.toastr.error("Request timeout", '',
              {
                timeOut: 2000
              });
          }
        }

           /******************** start search page **************/
           if (ctType && ctType == "application/json,uploadFile") {
            if (err.status === 404) {
                this.toastr.error(err.error.MessageAPI, '',
                    {
                        timeOut: 2000
                    });
            } else if (err.status === 503) {
                this.toastr.error(err.error.MessageAPI, '',
                    {
                        timeOut: 2000
                    });
            } else {
                this.toastr.error("System is under enhancements", '',
                    {
                        timeOut: 5000
                    });
            }
        }

        /******************** end search page **************/

        /******************** start send tramsaction page **************/

        else if (ctType && ctType == "application/json,getFiles") {
            if (err.status === 503) {

                this.toastr.error(err.error.MessageAPI, '',
                    {
                        timeOut: 2000
                    });

            } else {
                this.toastr.error("System is under enhancements", '',
                    {
                        timeOut: 5000
                    });
            }
        }
        /******************** end send tramsaction page **************/

        /************** start transactions page  **********************/

        else if (ctType && ctType == "application/json,deleteFile") {
            if (err.status === 503) {
                this.toastr.error(err.error.MessageAPI, '',
                    {
                        timeOut: 2000
                    });
            } else {
                this.toastr.error("System is under enhancements", '',
                    {
                        timeOut: 5000
                    });
            }
        }

        /************** end transactions page  **********************/

        /************** start get balance page  **********************/

        else if (ctType && ctType == "application/json,downloadFile") {
            if (err.status === 503) {
                this.toastr.error(err.error.MessageAPI, '',
                    {
                        timeOut: 2000
                    });
            } else {
                this.toastr.error("System is under enhancements", '',
                    {
                        timeOut: 5000
                    });
            }
        }
        
        const error = err.error.message || err.statusText;
        return throwError(error);
      }
      ))
  }

}