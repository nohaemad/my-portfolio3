import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import 'rxjs/add/operator/catch';
import { environment } from './../../../../environments/environment'
import * as sha256 from 'sha256';
import { EnvService } from '../../../env.service';
import { CookieService } from 'angular2-cookie';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  // define url in environment.ts
  api_url: string = environment.apiUrl;
  // define hash password for login , reset password and register
  hash_password;
  // define jwt token 
  token: string;
  // define Http Options
  httpOptions = {};

  jwt_token = JSON.parse(localStorage.getItem('jwt_token'));

  

  // define constructor for http client , router and link of env
  constructor(private http: HttpClient, private router: Router, private env: EnvService,private _cookieService: CookieService) {
    // define url in env.js 
    this.api_url = this.env.apiUrl;
  }

  /****************** start register service **********/
  addUser(user): Observable<HttpResponse<Object>> {

    // define request of register api
    var sendObject = {
      //define username
      AccountName: user.name,
      //define email address
      AccountEmail: user.emailAdd,
      //define phone number
      AccountPhoneNumber: user.phone,
      //define password
      AccountPassword: user.password,
      //define address
      AccountAddress: user.address,
      //define authentication type
      AccountAuthenticationType: "passward",
      //define authentication value
      AccountAuthenticationValue: ''
    };

    // print request of register api
    console.log(JSON.stringify(sendObject));

    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,registerPage'
      })
    }

    //define the function of register api
    return this.http.post<HttpResponse<object>>(this.api_url + '/833194c9bb5d54419e8cf7',
      JSON.stringify(sendObject),
      this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandler));
  }


  /****************** end register  **********/


  /****************** start login  **********/

  userAuthentication(email, password): Observable<HttpResponse<Object>> {

    // define user token 
    this.token = JSON.parse(localStorage.getItem("userToken"));

    // define request of login api
    var sendObject = {
      // define email
      EmailOrPhone: email,
      // define password
      Password: password,
      // define authentication value
      AuthValue: '',
      // define tokne
      SessionID: this.token

    };

    // print request of login api
    console.log(JSON.stringify(sendObject));

    //define http option
    this.httpOptions= {

      observe: 'response',

      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,loginPage'
      })
    }

    //define the function of login api
    return this.http.post<HttpResponse<object>>(this.api_url + '/9d6322c1f4d9d3f38aed8bf', JSON.stringify(sendObject), this.httpOptions)

      .catch(this.errorHandler);

  }

  /****************** end login  **********/

  /****************** start save public key  **********/

  SavePublickey(save_public_key) {

    // define request of save public key api
    var sendObject = {
      // define public key
      PublicKey: save_public_key.public_key,
      // define password
      Passsword: save_public_key.Passsword,
      // define email
      Email: save_public_key.email,
    };

    // print request of save public key api
    console.log("save public key request: " + JSON.stringify(sendObject));

  
    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,save_public_key',
        // define jwt token
        'jwt-token': save_public_key.jwt_token
      })
    }

    //define the function of save public key api
    return this.http.post<HttpResponse<object>>(this.api_url + '/99dc78047a8542fcf9decyp', JSON.stringify(sendObject), this.httpOptions)

      .catch(this.errorHandler);
  }

  /****************** end save public key  **********/


  /***************** start  logout **********/

  logout() {
    // remove public key from local storage
    localStorage.removeItem('PublicKey');

    this._cookieService.remove('jwt-token');

    // go to login page
    this.router.navigate(['/auth/login']);

    // define email , password and is checked
    var userDetails = { emailAdd: '', password: '', isChecked: true };

    //get userDetails in localstorage
    userDetails = JSON.parse(localStorage.getItem('dataToRemember'));

    //print data before logout
    console.log('before logout' + JSON.stringify(userDetails));



    // clear localstorage
    localStorage.clear();

    // check if user is checked
    if (userDetails.isChecked) {
      // save userDetails in localstorage
      localStorage.setItem('dataToRemember', JSON.stringify(userDetails));
    }
  }

  /***************** end  logout **********/

  /****************** start forget password **************/

  forgetPassword(user): Observable<HttpResponse<object>> {

    // define request of forget password api
    var sendObject = {
      // define email
      Email: user.emailAdd,
      // define phone number
      Phonnum: ''
    };

    // print request of forget password api
    console.log(JSON.stringify(sendObject));

    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,forgetPasswodPage'
      })
    }

    //define the function of forget password api
    return this.http.post<HttpResponse<object>>(this.api_url + "/cabaf46ee69d7b8445a5d791", JSON.stringify(sendObject), this.httpOptions)
      .pipe(
        retry(1),
        catchError(
          this.errorHandler,
        ));
  }

  /****************** end forget password **************/

  /****************** start reset password **************/

  resetPassword(user): Observable<HttpResponse<object>> {

    // define hash password for hashing password
    this.hash_password = sha256(user.newPassword);

    // define request of reset password api
    var sendObject = {
      //define email
      Email: user.emailAdd,
      //define phone number
      Phonnum: '',
      //define new password
      Newpassword: this.hash_password,
      //define code
      Code: user.code
    };

    var path_api = JSON.parse(localStorage.getItem('forget password response')).PathAPI;

    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,resetPasswodPage'
      })
    }

    // print request of reset password api
    console.log(JSON.stringify(sendObject));

    //define the function of reset password api
    return this.http.put<HttpResponse<object>>(this.api_url + "/" + path_api, JSON.stringify(sendObject), this.httpOptions)
      .pipe(
        retry(1),
        catchError(
          this.errorHandler,
        ));
  }

  /****************** end reset password **************/


  /****************** start error  **********/

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || "server Error");
  }

  /****************** end error  **********/

}
