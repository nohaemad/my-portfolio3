import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { EnvService } from '../../../env.service';
import { Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TokensService {

  // define url in environment.ts
  api_url: string = environment.apiUrl;
  // define Http Options
  httpOptions = {};

  jwt_token = JSON.parse(localStorage.getItem('jwt_token'));

  
  // define constructor for http client and link of env
  constructor(private http: HttpClient, private env: EnvService) {
    // define url in env.js 
    this.api_url = this.env.apiUrl;
  }


  /****************** start get tokens **************/

  getTokens(token): Observable<HttpResponse<Object>> {

    // define request of get tokens api
    var sendObject = {
      // define user public key
      Password: token.password,
      //define user password
      PublicKey: token.public_key
    };

    // print request of get tokens api
    console.log(JSON.stringify(sendObject));

    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json',
     // define jwt token
     'jwt-token': token.jwt_token
      })
    }

    //define the function of get tokens api

    return this.http.post<HttpResponse<object>>(this.api_url + '/8iapf151296c8bdg0be5b8w',
      JSON.stringify(sendObject),
      this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandler));
  }

  /****************** end get tokens **************/

  /****************** start add token **************/

  addToken(token): Observable<HttpResponse<Object>> {

    // define request of add token api
    var sendObject = {
      // define token name
      TokenName: token.name,
      // define token symbol
      TokenSymbol: token.symbol,
      // define token icon
      TokenIcon: token.icon,
      // define token description
      Description: token.Description,
      // define initiator address
      InitiatorAddress: token.public_key,
      // define password
      Password: token.password,
      // define token total supply
      TokensTotalSupply: token.TokensTotalSupply,
      // define token value
      TokenValue: token.TokenValue,
      // define token reissuability
      Reissuability: token.Reissuability,
      // define token precision
      Precision: token.Precision,
      // define token usage type
      UsageType: token.UsageType,
      // define token type
      TokenType: token.TokenType,
      // define token value dynamic
      ValueDynamic: token.ValueDynamic,
      // define token contract id
      ContractID: token.ContractID,
      // define token user public key
      UserPublicKey: token.UserPublicKey,
      // define token dynamic price
      Dynamicprice: token.Dynamicprice,
      // define token time
      TokenTime: token.TokenTime
    };

    // print request of add token api
    console.log(JSON.stringify(sendObject));

    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,addToken',
        // define jwt token
        'jwt-token': token.jwt_token
      })
    }

    //define the function of add token api
    return this.http.post<HttpResponse<object>>(this.api_url + '/a34c260d1b9e1e0482b4e90',
      JSON.stringify(sendObject),
      this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandler));
  }

  /****************** end add token  **********/

  /****************** start edit token  **********/

  editToken(token): Observable<HttpResponse<Object>> {

    // define request of edit token api
    var sendObject = {
      // define token id
      TokenID: token.id,
      // define token name
      TokenName: token.name,
      // define token symbol
      TokenSymbol: token.symbol,
      // define token icon
      TokenIcon: token.icon,
      // define token description
      Description: token.Description,
      // define initiator address
      InitiatorAddress: token.public_key,
      // define password
      Password: token.password,
      // define token total supply
      TokensTotalSupply: token.TokensTotalSupply,
      // define token value
      TokenValue: token.TokenValue,
      // define token reissuability
      Reissuability: token.Reissuability,
      // define token precision
      Precision: token.Precision,
      // define token usage type
      UsageType: token.UsageType,
      // define token type
      TokenType: token.TokenType,
      // define token value dynamic
      ValueDynamic: token.ValueDynamic,
      // define token contract id
      ContractID: token.ContractID,
      // define token user public key
      UserPublicKey: token.UserPublicKey,
      // define token dynamic price
      Dynamicprice: token.Dynamicprice
    };

    // print request of edit token api
    console.log(JSON.stringify(sendObject));

    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,editToken',
        // define jwt token
        'jwt-token': token.jwt_token
      })
    }

    //define the function of edit token api
    return this.http.put<HttpResponse<object>>(this.api_url + '/911b2f6bf2fef2f0633d60c7d',
      JSON.stringify(sendObject),
      this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandler));
  }

  /****************** end edit token  **********/

  /****************** add refund token  **********/

  refundToken(tokenDetails): Observable<HttpResponse<Object>> {

    // define request of refund token api
    var sendObject = {
      // define time
      Time: tokenDetails.timeZone,
      //define sender public key
      Sender: tokenDetails.Sender,
      // define receiver public key
      Receiver: tokenDetails.receiver,
      // define amount
      Amount: parseFloat(tokenDetails.Amount),
      // define signature
      Signature: tokenDetails.hash,
      // define token id
      TokenID: tokenDetails.id,
      // define flat currency
      FlatCurrency: tokenDetails.flat_currency
    };

    // print request of refund token api
    console.log(JSON.stringify(sendObject));

    //define http option
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,refundToken',
        // define jwt token
        'jwt-token': tokenDetails.jwt_token
      })
    }

    //define the function of refund token api
    return this.http.post<HttpResponse<object>>(this.api_url + '/512b2e814f397c47de6f5web', JSON.stringify(sendObject), this.httpOptions)

      .catch(this.errorHandler);
  }

  /****************** end refund token  **********/

  /****************** start error handle  **********/

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || "server Error");
  }

    /****************** end error handle  **********/

}
