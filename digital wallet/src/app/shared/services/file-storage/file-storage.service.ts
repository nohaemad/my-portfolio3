import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { EnvService } from '../../../env.service';
import { UploadFileStorage } from '../../models/file-storage/upload-file.model';
import { Observable } from 'rxjs';
import { EploreFilesRequest } from '../../models/file-storage/explore_file.model';
import { DeleteFileRequest } from '../../models/file-storage/delete_file.model';
import { DownloadFileRequest } from '../../models/file-storage/download_file.model';
import {CookieService} from 'angular2-cookie/core';

@Injectable({
  providedIn: 'root'
})
export class FileStorageService {

  constructor(private http: HttpClient, private env: EnvService,private _cookieService: CookieService) {
    // define url in env.js 
    this.api_url = this.env.apiUrl;
  }

  api_url: string = environment.apiUrl;

  httpOptions = {};

  jwt_token = (!!this._cookieService.get('jwt_token')) ? JSON.parse(this._cookieService.get('jwt_token')) : null;

 

  uploadFileStorage(upload_file_storage: UploadFileStorage): Observable<HttpResponse<Object>> {

    const formData: FormData = new FormData();
    Object.keys(upload_file_storage).forEach(key => formData.append(key, upload_file_storage[key]));

    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Accept': 'application/json,uploadFile',
        // define jwt token
        'jwt-token': this.jwt_token

      })
    }
    
    console.log(formData);
    return this.http.post<HttpResponse<Object>>
    (this.api_url + '/fe6b561cf1187e106e3dfb6829b2e12c78783', formData,this.httpOptions);
  }

  exploreFiles(explore_file_request: EploreFilesRequest): Observable<HttpResponse<Object>> {

    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,getFiles',
        // define jwt token
        'jwt-token': this.jwt_token
      })
    }

    return this.http.post<HttpResponse<Object>>
    (this.api_url + '/a9f0be2e898c28ac59c79d642722b88966c', JSON.stringify(explore_file_request),this.httpOptions);
  }

  deleteFile(delete_file_request: DeleteFileRequest): Observable<HttpResponse<Object>> {
    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,deleteFile',
        // define jwt token
        'jwt-token': this.jwt_token
      })
    }

    console.log(JSON.stringify(delete_file_request))
    return this.http.post<HttpResponse<Object>>
    (this.api_url + '/bbfcf281d1c9c237c81e7b5526280', JSON.stringify(delete_file_request),this.httpOptions);
  }

  downloadFile(download_file_request: DownloadFileRequest): Observable<HttpResponse<Object>> {

    this.httpOptions = {
      // define http header
      headers: new HttpHeaders({
        // define content type for interceptor
        'Content-Type': 'application/json,downloadFile',
        // define jwt token
        'jwt-token': this.jwt_token
      })
    }
    return this.http.post<HttpResponse<Object>>
    (this.api_url + '/abb3e140af96a1dfd63803e716473c', JSON.stringify(download_file_request),this.httpOptions);
  }

}
