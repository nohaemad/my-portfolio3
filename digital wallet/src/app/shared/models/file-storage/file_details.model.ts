export class FileDetails {
    Fileid: string;
    FileName: string;
    FileType: string;
    FileSize: string;
    Blockindex: string;
    Filehash: string;
  }
  