import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { QRCodeComponent } from 'angularx-qrcode';
import { AuthenticationComponent } from './authentication/pages.component';
import { LoginComponent } from './authentication/login/login.component';
import { RegisterComponent } from './authentication/register/register.component';
import { ForgetPasswordComponent } from './authentication/forget-password/forget-password.component';
import { ResetPasswordComponent } from './authentication/reset-password/reset-password.component';
import { AuthGuardService } from './shared/guards/auth-guard.service';

const routes: Routes = [
  { path: "pages", loadChildren: "app/pages/pages.module#PagesModule" , canActivate : [AuthGuardService]},
  {
    path: "auth",
    component: AuthenticationComponent,
    children: [
      {
        path: "login",
        component: LoginComponent
      // canActivate : [LoginGuardService]
      },
      {
        path: "register",
        component: RegisterComponent      },
      {
        path: "forget-password",
        component: ForgetPasswordComponent
      },

      {
        path: "reset-password",
        component: ResetPasswordComponent
      },
    ],
  },

  { path: "", redirectTo: "auth/login", pathMatch: "full" },
  { path: "**", redirectTo: "pages" },
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule],
})
export class AppRoutingModule {}
