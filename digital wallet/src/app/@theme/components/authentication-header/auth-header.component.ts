import { Router } from "@angular/router";
import { Component, Input, OnInit } from "@angular/core";

import { NbMenuService, NbSidebarService } from "@nebular/theme";
import {Location} from '@angular/common';

@Component({
  selector: "ngx-auth-header",
  styleUrls: ["./auth-header.component.scss"],
  templateUrl: "./auth-header.component.html"
})
export class AuthHeaderComponent implements OnInit {
  @Input() position = "normal";

  user: any;

  userMenu = [{ title: "Profile" }, { title: "Log out" }];
  userData:any;
  constructor(
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private router: Router,
    private location : Location
  ) {
    // this.userData = JSON.parse(localStorage.getItem('userData'));

  }

  ngOnInit() {
   
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, "menu-sidebar");

    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.router.navigate(["/pages/search-result"]);
  }

  onBack(){
    this.location.back();

  }
}
