import { Router } from "@angular/router";
import { Component, Input, OnInit, AfterContentInit } from "@angular/core";

import { NbMenuService, NbSidebarService } from "@nebular/theme";
import { UserService } from '../../../shared/services/user/user.service';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';
import { AccountService } from '../../../shared/services/account/account.service';
import { SettingsService } from '../../../shared/services/settings/settings.service';
import { TokensService } from '../../../shared/services/tokens/tokens.service';

@Component({
  selector: "ngx-header",
  styleUrls: ["./header.component.scss"],
  templateUrl: "./header.component.html"
})
export class HeaderComponent implements OnInit, AfterContentInit {
  @Input() position = "normal";

  deactivate = {
    name: '',
    password: '',
    public_key: '',
    reason: '',
    jwt_token : ''
  }

  user: any;
  username: string;
  image_url: string;
  userData = {};

  token = {
    password: '',
    public_key: '',
    jwt_token : ''
    }

  isDeactivate: boolean

  userDetails = {
    PublicKey: JSON.parse(localStorage.getItem('userData')).AccountPublicKey,
    Password: JSON.parse(localStorage.getItem('userData')).AccountPassword
  }

  balanceDetails: any = {
    TotalBalance: 0,
    TotalSent: 0,
    TotalReceived: 0
  }
  userMenu = [{ title: "Profile" }, { title: "Log out" }];

  constructor(
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private toastr: ToastrService,
    private router: Router,
    private userSvr: UserService,
    private location: Location,
    private accountSvr: AccountService,
    private settingSvr: SettingsService,
    private tokenSvr: TokensService

  ) {
    this.userData = JSON.parse(localStorage.getItem('userData'));
    console.log(this.location.path());


  }

  ngOnInit() {

    this.token.public_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey
    this.token.password = JSON.parse(localStorage.getItem('userData')).AccountPassword;
    this.token.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));
        this.tokenSvr.getTokens(this.token).subscribe((tokens: any) => {
      localStorage.setItem('tokensDetails', JSON.stringify(tokens));
    })

    // this.userService
    //   .getUsers()
    //   .subscribe((users: any) => (this.user = users.nick));
    this.username = JSON.parse(localStorage.getItem('userData')).AccountName;
    this.image_url = "../../assets/images/default.png"

    this.isDeactivate = JSON.parse(localStorage.getItem('userData')).AccountStatus;

  }

  ngAfterContentInit() {

  }

  activateAccount() {
    this.deactivate.password = JSON.parse(localStorage.getItem('userData')).AccountPassword;
    this.deactivate.name = JSON.parse(localStorage.getItem('userData')).AccountName;
    this.deactivate.public_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey;
    this.deactivate.reason = '';
    this.deactivate.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));
    this.settingSvr.activateAccount(this.deactivate).subscribe((data: any) => {
      localStorage.setItem("activate response", JSON.stringify(data));
      if (!JSON.parse(localStorage.getItem('activate response')).Message) {
        this.toastr.success("your account is activated successfuly", '',
          {
            timeOut: 2000
          });

        this.router.navigate(['/auth//login']);
      } else {
        this.toastr.error(JSON.parse(localStorage.getItem('activate response')).Message, '',
          {
            timeOut: 2000
          });
      }
    });
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, "menu-sidebar");

    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    // this.router.navigate(["/pages/search-result"]);
  }
  logout() {
    this.userSvr.logout();
    this.toastr.success('logout is successful', 'Please login to go to your profile',
      {
        timeOut: 2000
      });

  }

  goToProfile() {
    this.router.navigate(["/pages/edit-profile"]);
  }

  goToDeactivate() {
    this.router.navigate(["/pages/deactivate-account"]);
  }
}
