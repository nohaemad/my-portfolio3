import { Component } from "@angular/core";

@Component({
  selector: "ngx-footer",
  styleUrls: ["./footer.component.scss"],
  template: `
    <div class="text-center" style="margin: auto;">
    <p style = "text-align: center !important;margin-bottom: 0;color: #fff;font-size:16px;">Copyright &copy; 2019 Inovatian. All Rights Reserved</p>
    </div>
  `
})
export class FooterComponent {}
