/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
import { UserService } from './shared/services/user/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-app',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {


  constructor(private userSvr: UserService, private router: Router) {
    //     this.userSvr.getAdminToken().subscribe((token:any) => {
    // localStorage.setItem("admin token" , JSON.stringify(token));
    //     });
  }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem("isLoggin")) === true) {
      if(JSON.parse(localStorage.getItem("isLoggin")) === "/pages/refund-token"){
      this.router.navigate(['./pages/refund-token']);
      }else if(JSON.parse(localStorage.getItem("isLoggin")) === "/pages/add-service"){
        this.router.navigate(['./pages/add-service']);
      }
      else if(JSON.parse(localStorage.getItem("isLoggin")) === "/pages/add-token"){
        this.router.navigate(['./pages/add-token']);
      }
      else if(JSON.parse(localStorage.getItem("isLoggin")) === "/pages/balance"){
        this.router.navigate(['./pages/balance']);
      }
      else if(JSON.parse(localStorage.getItem("isLoggin")) === "/pages/token-creation-transaction"){
        this.router.navigate(['./pages/token-creation-transaction']);
      }
      else if(JSON.parse(localStorage.getItem("isLoggin")) === "/pages/deactivate-account"){
        this.router.navigate(['./pages/deactivate-account']);
      }else if(JSON.parse(localStorage.getItem("isLoggin")) === "/pages/edit-profile"){
        this.router.navigate(['./pages/edit-profile']);
      }else if(JSON.parse(localStorage.getItem("isLoggin")) === "/pages/edit-token"){
        this.router.navigate(['./pages/edit-token']);
      }else if(JSON.parse(localStorage.getItem("isLoggin")) === "/pages/privacy"){
        this.router.navigate(['./pages/privacy']);
      }else if(JSON.parse(localStorage.getItem("isLoggin")) === "/pages/refund-transaction"){
        this.router.navigate(['./pages/refund-transaction']);
      }else if(JSON.parse(localStorage.getItem("isLoggin")) === "/pages/send"){
        this.router.navigate(['./pages/send']);
      }else if(JSON.parse(localStorage.getItem("isLoggin")) === "/pages/services"){
        this.router.navigate(['./pages/services']);
      }else if(JSON.parse(localStorage.getItem("isLoggin")) === "/pages/terms"){
        this.router.navigate(['./pages/terms']);
      }else if(JSON.parse(localStorage.getItem("isLoggin")) === "/pages/token-details"){
        this.router.navigate(['./pages/token-details']);
      }else if(JSON.parse(localStorage.getItem("isLoggin")) === "/pages/transactions"){
        this.router.navigate(['./pages/transactions']);
      }
    }else{


            this.router.navigate(['./login'])
    }
  }
}
