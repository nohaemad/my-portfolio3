/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { APP_BASE_HREF } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { CoreModule } from "./@core/core.module";
import {CookieService, CookieOptions} from 'angular2-cookie/core';
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { ThemeModule } from "./@theme/theme.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NbAuthModule } from '@nebular/auth';
import { PagesModule } from './authentication/pages.module';
import { ToastrModule } from 'ngx-toastr';
import { NotifierModule } from 'angular-notifier';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { EnvServiceProvider } from './env.service.provider';
import { MessagingService } from './shared/services/messaging/messaging.service';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { InoInterceptor } from './shared/services/ino.interceptor';
import { UserInterceptor } from './shared/interceptors/user.interceptor';
import { SettingsInterceptor } from './shared/interceptors/settings.interceptor';
import { AccountInterceptor } from './shared/interceptors/account.interceptor';
import { TokensInterceptor } from './shared/interceptors/tokens.interceptor';
import { ServicesInterceptor } from './shared/interceptors/services.interceptor';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,    FormsModule,
    ToastrModule.forRoot(
      {
        preventDuplicates: true,
        maxOpened: 1,
        progressBar: false,
        autoDismiss:true,
        closeButton:true,
        positionClass: 'toast-top-center',
      },
    ),
    NotifierModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NbAuthModule,
    PagesModule,
    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),
    PaginationModule.forRoot()
  ],
  bootstrap: [AppComponent],
  providers: [EnvServiceProvider,
    MessagingService,
    CookieService,
    { provide: APP_BASE_HREF, useValue: "/" },
  { provide: HTTP_INTERCEPTORS, useClass: InoInterceptor, multi: true },
  {provide: CookieOptions, useValue: false},
  // { provide: HTTP_INTERCEPTORS, useClass: SettingsInterceptor, multi: true },
  // { provide: HTTP_INTERCEPTORS, useClass: AccountInterceptor, multi: true },
  // { provide: HTTP_INTERCEPTORS, useClass: TokensInterceptor, multi: true },
  // { provide: HTTP_INTERCEPTORS, useClass: ServicesInterceptor, multi: true }
]
})
export class AppModule {}
