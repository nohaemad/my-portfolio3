// import { UserBalanceComponent } from "./user-balance/user-balance.component";
import { NgModule } from "@angular/core";
import { NbAuthModule } from '@nebular/auth';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 
import { ToastrModule } from 'ngx-toastr';
// import { PagesComponent } from "./pages.component";
// import { DashboardModule } from "./dashboard/dashboard.module";
import { PagesRoutingModule } from "./pages-routing.module";
import { ThemeModule } from "../@theme/theme.module";
// import { MiscellaneousModule } from "./miscellaneous/miscellaneous.module";
// import { QrComponent } from "./qr/qr.component";
// import { QRCodeModule } from "angularx-qrcode";
// import { SearchResultComponent } from "./search-result/search-result.component";
// import { RegisterComponent } from './register/register.component';
// import { CryptographyRegisterComponent } from './cryptography-register/cryptography-register.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthenticationComponent } from './pages.component';
import { InternationalPhoneModule } from 'ng4-intl-phone';
import { CustomFormsModule } from 'ng2-validation'
import { UserService } from '../shared/services/user/user.service';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
// import { InternationalPhoneNumberModule } from 'ngx-international-phone-number';

// import { DeactivateComponent } from './deactivate/deactivate.component';
// import { EditProfileComponent } from './edit-profile/edit-profile.component';

const PAGES_COMPONENTS = [AuthenticationComponent];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    // NbAuthModule,
    InternationalPhoneModule,
    CustomFormsModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(
      {
        timeOut: 2000, extendedTimeOut: 0,
        preventDuplicates: true,
        maxOpened: 1,
        progressBar: false,
        autoDismiss:true,
        closeButton:true,
        positionClass: 'toast-top-center',
      },
    ), // ToastrModule added
    // InternationalPhoneNumberModule

    // DashboardModule,
    // MiscellaneousModule,
    // QRCodeModule,

  ],
  declarations: [
    ...PAGES_COMPONENTS,
    // QrComponent,
    // SearchResultComponent,
    // UserBalanceComponent,
    RegisterComponent,
    LoginComponent,
    AuthenticationComponent,
    ForgetPasswordComponent,
    ResetPasswordComponent
    // DeactivateComponent,
    // EditProfileComponent 
  ],
  providers: [UserService]
})
export class PagesModule {}
