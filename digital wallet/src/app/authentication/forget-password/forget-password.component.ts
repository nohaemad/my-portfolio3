import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
// import Toastr Service
import { ToastrService } from 'ngx-toastr';
// import user service
import { UserService } from '../../shared/services/user/user.service';
// import jquery
import * as $ from 'jquery';


@Component({
  selector: 'ngx-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss', '../pages.component.scss']
})

export class ForgetPasswordComponent implements OnInit {

  // define userDetails input email 
  @Input() userDetails = { emailAdd: '' };

  // define send for if the forget password function not has error 
  send: boolean = false;

  // define click for disable button submit
  click: boolean = true;

  // start constructor for user service , router , toastr and router
  constructor(
    // define user service
    private userSvr: UserService,
    // define router
    private router: Router,
    // define toastr service
    private toastr: ToastrService) { }

  // start ngOnInit function
  ngOnInit() {
  }
  // end ngOnInit function

  // start function of forget password
  onSubmit(form: NgForm) {

    // start if condition
    // chcek if the user click on the login more than 1 time clear the toastr notification
    if (this.toastr.toasts.length > 0) {
      this.toastr.clear();
    }
    // end if condition

    // start check if the email is null
    if (form.value.emailAdd === '' || form.value.emailAdd === null) {
      // show notification if the email is null
      this.toastr.error('Email is Required - برجاء ادخال البريد الإلكتروني', '',
        {
          timeOut: 2000
        });
    }

    // end check if the email is null

    // start else condition 1- if  email not equal null

    else {

      // make button submit is enabled
      $('button[type="submit"]').prop('disabled', true);

      // get email of user and assign it to userobject.emailAdd
      this.userDetails.emailAdd = form.value.emailAdd;

      // start call forgetPassword from user service
      this.userSvr.forgetPassword(this.userDetails)
        .subscribe((data: any) => {
          // start if no errors

          // add email to local storage
          localStorage.setItem('email from forget', JSON.stringify(form.value.emailAdd));
          // go to reset password page
          this.router.navigate(['./auth/reset-password']);
          // start show notification please check your email
          this.toastr.success('please check your email', '',
            {
              timeOut: 2000
            });
          console.log("logged in");

          localStorage.setItem('forget password response', JSON.stringify(data));


          // make send is false for not send another email
          this.send = false;

          // end if no errors
        }, error => {
          // start if errors

          // make send is true for send another email because error

          this.send = true;

          // start call forgetPassword from user service because error

          this.userSvr.forgetPassword(this.userDetails)
            .subscribe((data: any) => {
              // start if no errors

              // add forget password response to local storage
              localStorage.setItem('forget response', JSON.stringify(data));

              // add email to local storage
              localStorage.setItem('email from forget', JSON.stringify(form.value.emailAdd));

              // go to reset password page
              this.router.navigate(['./auth/reset-password']);

              // start show notification please check your email
              this.toastr.success('please check your email', '',
                {
                  timeOut: 2000
                });
              console.log("logged in");
              // make send is false for not send another email
              this.send = false;
            });
        });

      // start check if send is false
      if (this.send === false) {
        // make click is true
        this.click = true;
        // make button submit is enabled
        $('button[type="submit"]').prop('disabled', false);
      }
      // end check if send is false
      // start check if send is true
      else {
        // make click is true
        this.click = false;
        // make button submit is disabled
        $('button[type="submit"]').prop('disabled', "disabled");
      }
      // end check if send is true
    }
    // end else condition 1- if  email not equal null
  }

  // end function of forget password

}
