import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
// import Toastr Service
import { ToastrService } from 'ngx-toastr';
// import user service
import { UserService } from '../../shared/services/user/user.service';
// import jquery
import * as $ from 'jquery';

@Component({
  selector: 'ngx-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss', '../pages.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  // define userDetails input email , password , confirm password and code
  @Input() userDetails = { emailAdd: '', newPassword: '', code: '', confirmNew_password: '' };

  // start constructor for user service , toastr and router
  constructor(
    // define user service
    private userSvr: UserService,
    // define router
    private router: Router,
    // define toastr service
    private toastr: ToastrService) { }
  // end constructor for user service , toastr and router

  // start ngOnInit function
  ngOnInit() {

    // start when click on eye icon of password jquery
    $(".toggle-password").click(function () {

      $(this).toggleClass("eva-eye eva-eye-off");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });
    // end when click on eye icon of password jquery
  }
  // end ngOnInit function

  // start function of reset password
  onSubmit(form: NgForm) {

    // start if condition
    // chcek if the user click on the login more than 1 time clear the toastr notification
    if (this.toastr.toasts.length > 0) {
      this.toastr.clear();
    }
    // end if condition


    // start check if the password is null
    if (form.value.newPassword === '' || form.value.newPassword === null) {
      // show notification if the password is null
      this.toastr.error('New password is required - برجاء ادخال كلمة المرور جديدة ', '',
        {
          timeOut: 2000
        });
    }
    // end check if the password is null

    // start check if the code is null
    if (form.value.code === '' || form.value.code === null) {
      // show notification if the password is null
      this.toastr.error('Code is required - برجاء ادخال الكود  ', '',
        {
          timeOut: 2000
        });
    }
    // end check if the code is null

    // start else condition 1- if the password or code is null
    else {

      // get email from forget from local storage and assign it to userDetails.emailAdd
      this.userDetails.emailAdd = JSON.parse(localStorage.getItem('email from forget'));
      // get password of user and assign it to userobject.newPassword
      this.userDetails.newPassword = form.value.newPassword;
      // get code of user and assign it to userobject.code
      this.userDetails.code = form.value.code;

      // start call resetPassword from user service
      this.userSvr.resetPassword(this.userDetails)
        .subscribe((data: any) => {

          // add reset password response to local storage
          localStorage.setItem('reset response', JSON.stringify(data));

          // start check if the reset response not has Message
          if (!JSON.parse(localStorage.getItem('reset response')).ErrorMessage) {

            // start show notification your password successfully changed
            this.toastr.success('your password successfully changed', '',
              {
                timeOut: 2000
              });
            // end show notification your password successfully changed

            localStorage.removeItem('forget password response');


            // go to login page
            this.router.navigate(['./auth/login']);

            // print logged in
            console.log("logged in");

          }
          // end check if the login response not has Message
          // start else condition the login response has Message
          else {
            this.toastr.error(JSON.parse(localStorage.getItem('reset response')).ErrorMessage, '',
              {
                timeOut: 2000
              });
          }
          // end else condition the login response has Message
        });
      // end call resetPassword from user service
    }
    // end else condition 1- if the password or code is null
  }
  // end function of reset password
}
