import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
// import Toastr Service
import { ToastrService } from 'ngx-toastr';
// import sha256
import * as sha256 from 'sha256';
// import user service
import { UserService } from '../../shared/services/user/user.service';
// import jquery
import * as $ from 'jquery';

@Component({
  selector: 'ngx-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss', '../pages.component.scss']
})
export class RegisterComponent implements OnInit {

  // define space
  space = ' ';

  // define plus
  plus = "+";

  // define special_char_amount for username
  special_char_amount = 0;

  // define special_char_amount2 for email
  special_char_amount2 = 0;

  // define array of special characters for username
  special_charchters = ['"', "!", "$", "%", "&", "`", "'", "(", ")", "*", "+", ",", "-", "/", ":", ";",
    "<", "=", ">", "?", "@", "[", "\\", "]", "^", "`", "{", "|", "}", "#", "~",
    '"', "_"];

  // define array of special characters for email
  special_charchters2 = ['"', "!", "$", "%", "&", "`", "'", "(", ")", "*", "+", ",", "/", ":", ";",
    "<", "=", ">", "?", "[", "\\", "]", "^", "`", "{", "|", "}", "#", "~",
    '"'];

  // define userDetails input username , email , phone , address , password and confirm password
  @Input() userDetails = {
    name: '',
    emailAdd: '',
    phone: '',
    address: '',
    password: '',
    confirmNew_password: ''
  }

  // define userobject for username , email , phone , address , password and confirm password
  userobject: any = {
    name: '',
    emailAdd: '',
    phone: '',
    address: '',
    password: ''
  };

  // define hash password
  hash_password;

  // define password of user
  passwordInput: string;

  // define username of user
  nameInput: string;

  // start constructor for user service , router , toastr and router
  constructor(
    private userSvr: UserService,
    private router: Router,
    private toastr: ToastrService) {
  }

  // start ngOnInit function
  ngOnInit() {

    // start when click on eye icon of password jquert
    $(".toggle-password").click(function () {

      $(this).toggleClass("eva-eye eva-eye-off");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });
    // end when click on eye icon of password jquert
  }
  // end ngOnInit function

  // start increment_special_amount function
  increment_special_amount() {
    // increase special_char_amount by 1
    this.special_char_amount = this.special_char_amount + 1;
  }
  // end increment_special_amount function

  // start get_special_amount function
  get_special_amount() {
    // get special_char_amount
    return this.special_char_amount;
  }
  // end get_special_amount function

  // start set_special_amonnt function
  set_special_amonnt() {
    // set special_char_amount by 0
    this.special_char_amount = 0;
  }
  // end set_special_amonnt function

  // start increment_special_amount2 function
  increment_special_amount2() {
    // increase special_char_amount2 by 1
    this.special_char_amount2 = this.special_char_amount2 + 1;
  }
  // end increment_special_amount2 function

  // start get_special_amount2 function
  get_special_amount2() {
    // get special_char_amount2
    return this.special_char_amount2;
  }
  // end get_special_amount2 function

  // start set_special_amonnt2 function
  set_special_amonnt2() {
    // set special_char_amount2 by 0
    this.special_char_amount2 = 0;
  }
  // end set_special_amonnt2 function

  // start function of register
  OnRegister(form: NgForm) {

    // start if condition
    // chcek if the user click on the login more than 1 time clear the toastr notification
    if (this.toastr.toasts.length > 0) {
      this.toastr.clear();
    }
    // end if condition

    //print data of user
    console.log(form.value);

    // start check if username or email or phone or password or confirm password or password not equal null
    if ((form.value.name === '' || form.value.name === null) ||
      (form.value.emailAdd === '' || form.value.emailAdd === null) ||
      (form.value.phone === '' || form.value.phone === null) ||
      (form.value.password === '' || form.value.password === null) ||
      (form.value.confirm === '' || form.value.confirm === null) ||
      (form.value.address === '' || form.value.address === null)) {

      // start check if the username is null
      if (form.value.name === '' || form.value.name === null) {
        // show notification if the username is null
        this.toastr.error('Username is required - برجاء ادخال اسم المستخدم  ', '',
          {
            timeOut: 2000
          });
      }
      // end check if the username is null

      // start check if the email is null and phone is null
      if (form.value.emailAdd === '' || form.value.emailAdd === null) {

        // start check if the phone is null
        if (form.value.phone === '' || form.value.phone === null) {
          // show notification if the phone is null
          this.toastr.error('Phone is required - برجاء ادخال رقم الهاتف', '',
            {
              timeOut: 2000
            });
        }
        // end check if the phone is null
      }
      // end check if the email is null and phone is null

      // start check if the password is null
      if (form.value.password === '' || form.value.password === null) {
        // show notification if the password is null
        this.toastr.error('Password is required - برجاء ادخال كلمة المرور', '',
          {
            timeOut: 2000
          });
      }
      // end check if the password is null

      // start check if the confirm password is null
      if (form.value.confirmNew_password === '' || form.value.confirmNew_password === null) {
        // show notification if the confirm password is null
        this.toastr.error('Confirm password is required - برجاء تأكيد كلمة المرور ', '',
          {
            timeOut: 2000
          });
      }
      // end check if the confirm password is null

      // start check if the address is null
      if (form.value.address === '' || form.value.address === null) {
        // show notification if the address is null
        this.toastr.error('Address is required - برجاء ادخال محل الاقامة ', '',
          {
            timeOut: 2000
          });
      }
      // end check if the address is null
    }
    // end check if username or email or phone or password or confirm password or password not equal null

    // start else condition 1- if username or email or phone or password or confirm password or password not equal null
    else {

      // start check if the form is valid
      if (form.valid) {

        // hashing password of user using sha256 and assign it to hash_password
        this.hash_password = sha256(form.value.password);

        // get username of user and assign it to userobject.name
        this.userobject.name = form.value.name;

        // get email of user and assign it to userobject.emailAdd
        this.userobject.emailAdd = form.value.emailAdd;

        // get phone of user and assign it to userobject.phone
        this.userobject.phone = form.value.phone;

        // get address of user and assign it to userobject.address
        this.userobject.address = form.value.address;

        // get hash_password and assign it to userobject.password
        this.userobject.password = this.hash_password;

        // get password of user and assign it to passwordInput
        this.passwordInput = form.value.password;

        // print password of user
        console.log(form.value.password);

        // print hash passowrd of user
        console.log(sha256(form.value.password));

        // get username of user and assign it to nameInput
        this.nameInput = form.value.name;

        // print password of password
        console.log(this.passwordInput);

        // print password of username
        console.log(this.nameInput);

        // print if the password contain username
        console.log("index of name : " + this.passwordInput.indexOf(this.nameInput));

        // start check if the password is contain username
        if (this.passwordInput.indexOf(this.nameInput) !== -1) {
          // show notification if the password is contain username
          this.toastr.error("Password shouldn't contain username", '',
            {
              timeOut: 2000
            });
        }
        // end check if the password is contain username

        // start else condition 1-  if the password is contain username 
        else {

          // start call addUser from user service
          this.userSvr.addUser(this.userobject)
            .subscribe((data: any) => {

              // add register response to local storage
              localStorage.setItem('userResponses', JSON.stringify(data));
              // start show notification register Successfuly
              this.toastr.success('Register is successful', 'Please check your email to confirm your account',
                {
                  timeOut: 2000
                });
              // end show notification register Successfuly

              // go to login page
              this.router.navigate(['./auth/login']);
              console.log(data);

            })
          // end call addUser from user service
        }
        // end else condition 1-  if the password is contain username 
      }
      // end check if the form is valid
    }
    // end else condition 1- if username or email or phone or password or confirm password or password not equal null
  }
  // end function of register
}
