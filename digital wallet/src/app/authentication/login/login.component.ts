import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
// import user service
import { UserService } from '../../shared/services/user/user.service';
// import Toastr Service
import { ToastrService } from 'ngx-toastr';
// import jquery
import * as $ from 'jquery';
// import sha256
import * as sha256 from 'sha256';
// import CryptoJS
import * as CryptoJS from 'crypto-js';
// import saveAs
import * as saveAs from 'file-saver';
// import Messaging Service
import { MessagingService } from '../../shared/services/messaging/messaging.service';
import { CookieService } from 'angular2-cookie';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss', '../pages.component.scss']
})

export class LoginComponent implements OnInit {

  // define counter
  public counter: number;
  // define private key
  private_key: string;
  // define userDetails input email , password and isChecked
  @Input() userDetails = { emailAdd: '', password: '', isChecked: false };
  // define userobject for email and password
  userobject: any = { emailAdd: '', password: '' };
  // define hash password
  hash_password;
  // define save public key
  save_public_key = {};



  // start constructor for user service , router , toastr and message service
  constructor(
    // define user service
    private userSvr: UserService,
    // define router
    private router: Router,
    // define toastr service
    private toastr: ToastrService,
    // define message service
    private messageSvr: MessagingService, private _cookieService: CookieService) { }
  // end constructor for user service , router , toastr and message service

  // start ngOnInit function
  ngOnInit() {

    // define user id
    const userId = 'user001';
    // call requestPermission function in message service
    this.messageSvr.requestPermission(userId);
    // define counter
    this.counter = 0;
    // print counter
    console.log('update counter', this.counter);
    // start check if dataToRemember not equal null
    if (JSON.parse(localStorage.getItem('dataToRemember')) !== null) {
      // get dataToRemember from local storage and assign it to userDetails
      this.userDetails = JSON.parse(localStorage.getItem('dataToRemember'));
      // print userDetails
      console.log(JSON.stringify(this.userDetails));
    }
    // end check if dataToRemember not equal null

    // start when click on eye icon of password jquery
    $(".toggle-password").click(function () {

      $(this).toggleClass("eva-eye eva-eye-off");
      let input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });
    // end when click on eye icon of password jquery
  }

  // end ngOnInit function

  // start function for display inputs when user try login more than 5 times

  disableInputs() {
    console.log('from disableInputs');
    var enableSubmit = function (ele) {
      $(ele).removeAttr("disabled");
    }
    $('.emailAdd').prop('disabled', true);
    $('.password').prop('disabled', true);
    alert('disable inputs for 3 minutes');
    setTimeout(function () { enableSubmit($('.emailAdd')) }, 3 * 60000);
    setTimeout(function () { enableSubmit($('.password')) }, 3 * 60000);
  }

  // end function for display inputs when user try login more than 5 times

  // start function of login
  onLogin(emailAdd, password) {

    // print data of user
    console.log(JSON.stringify(this.userDetails));

    // start if condition
    // chcek if the user click on the login more than 1 time clear the toastr notification
    if (this.toastr.toasts.length > 0) {
      this.toastr.clear();
    }
    // end if condition

    // start check if the email or password is null
    if ((emailAdd == "" || emailAdd == null) || (password == "" || password == null)) {
      // start check if the email is null
      if (emailAdd == "" || emailAdd == null) {
        // show notification if the email is null
        this.toastr.error('Email is Required - برجاء ادخال البريد الإلكتروني ');
      }
      // end check if the email is null
      // start check if the password is null
      if (password == "" || password == null) {
        // show notification if the password is null
        this.toastr.error('password is Required - برجاء ادخال كلمة المرور ');
      }
      // end check if the password is null
    }
    // end check if the email or password is null

    // start check if the email and password are null
    else if ((emailAdd == "" || emailAdd == null) && (password == "" || password == null)) {
      // show notification if the email and password are null
      this.toastr.error('you must enter email and password');
    }
    // end check if the email and password are null

    // start else condition 1- if the email or password is null 2- if the email and password is null
    else {
      // hashing the password with sha256
      this.hash_password = sha256(this.userDetails.password);

      // print goooooooooooooooooooo
      console.log("goooooooooooooooooooo");

      // start call userAuthentication from user service
      this.userSvr.userAuthentication(this.userDetails.emailAdd, this.hash_password)
        .subscribe((data: any) => {

          //print login response
          console.log('login response ', JSON.stringify(data.body));

          // add login response to local storage
          localStorage.setItem('userData', JSON.stringify(data.body));

          this._cookieService.put('jwt_token', JSON.stringify(data.headers.get('Jwt-Token')));


          localStorage.setItem('jwt_token', JSON.stringify(data.headers.get('Jwt-Token')));

// alert(data.headers.get('Jwt-Token'))

          // add data of user to local storage
          localStorage.setItem('dataToRemember', JSON.stringify(this.userDetails));

          // start check if the login response have AccountPrivateKey
          if (JSON.parse(localStorage.getItem('userData')).AccountPrivateKey) {

            // start check if the user confirm this message
            if (window.confirm('you responsible for this private key?')) {

              // define key for the encrypt private key
              var key = "digital wallet";

              // get AccountPrivateKey from local storage in item userData
              this.private_key = JSON.parse(localStorage.getItem('userData')).AccountPrivateKey;

              // encrypt the private key with Aes and assign it to enc_private_key
              var enc_private_key = CryptoJS.AES.encrypt(this.private_key.trim(), key.trim()).toString();

              // print enc_private_key
              console.log("enc privare key : " + enc_private_key);

              // add enc_private_key in file /tmp and assign it to file
              let file = new Blob([enc_private_key], { type: 'text/tmp;charset=utf-8' });

              // add file to private_key.tmp file and download it
              saveAs(file, 'private_key.tmp');

              // start check if the user is from inovatian accounts
              if ((emailAdd === "hatim@inovatian.com") ||
                (emailAdd === "hatim.fees@inovatian.com") ||
                (emailAdd === "hatim.refund@inovatian.com") ||
                (emailAdd === "askme557@gmail.com")) {

              }
              // end check if the user is from inovatian accounts

              // start else condition the user is not from inovatian accounts
              else {
                // start define the request of save public key
                this.save_public_key = {
                  // define public key from localstorage AccountPublicKey
                  public_key: JSON.parse(localStorage.getItem('userData')).AccountPublicKey,
                  // define email address
                  email: emailAdd,
                  // define hash passwrd
                  Passsword: this.hash_password,
                  // define jwt token 
                  jwt_token : JSON.parse(localStorage.getItem('jwt_token'))
                }
                // end define the request of save public key

                // start call SavePublickey from user service
                this.userSvr.SavePublickey(this.save_public_key).subscribe((data: any) => {
                  // add save public key response to local storage
                  localStorage.setItem("save public key response", JSON.stringify(data));
                })
                // end call SavePublickey from user service
              }
              // end else condition the user is not from inovatian accounts
            }
            // end check if the user confirm this message
          }
          // end check if the login response have AccountPrivateKey

          // start check if the login response not has Message
          if (!JSON.parse(localStorage.getItem('userData')).Message) {

            // add isLoggin to local storage
            localStorage.setItem("isLoggin", "true");

            // go to transaction page
            this.router.navigate(['./pages/transactions']);

            // start show notification Login Successfuly
            this.toastr.success('Login Successfuly', '',
              {
                timeOut: 2000
              });
            // end show notification Login Successfuly

            // print logged in
            console.log("logged in");

            // define the counter 0
            this.counter = 0;

          }
          // end check if the login response not has Message
          // start else condition the login response has Message
          else {

            // define counter and increase 1
            this.counter = this.counter + 1;

            // print counter
            console.log('this.counter ', this.counter);

            // show notification Login error
            this.toastr.error(JSON.parse(localStorage.getItem('userData')).Message);

            // start check if the counter bigger than 4
            if (this.counter > 4) {

              // print counter
              console.log('this.counter ', this.counter);

              // call disableInputs
              this.disableInputs();
            }
            // end check if the counter bigger than 4
          }
        })
      // end call userAuthentication from user service
    }
    // end else condition 1- if the email or password is null 2- if the email and password is null
  }
  // end function of login
}
