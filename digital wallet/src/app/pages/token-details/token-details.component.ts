import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
// import DomSanitizer
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'ngx-token-details',
  templateUrl: './token-details.component.html',
  styleUrls: ['./token-details.component.scss', '../pages.component.scss']
})
export class TokenDetailsComponent implements OnInit {

  // define imagePath for decrypt the image 
  imagePath: any;

  // define if the user is inovatian account
  isAdmin = true;

  // get AccountEmail from local storage and assign it to emailUser
  emailUser = JSON.parse(localStorage.getItem("userData")).AccountEmail;

  // define token_image
  token_image: boolean = false;

  // define default_image
  default_image = '';

  // define tokenDetails input
  tokenDetails = {
    id: '',
    name: '',
    symbol: '',
    icon: '',
    TokensTotalSupply: 0.000000,
    TokenValue: 0.000000,
    Reissuability: true,
    Precision: 0,
    UsageType: '',
    TokenType: '',
    ValueDynamic: true,
    ContractID: '',
    Dynamicprice: 0.000000,
    TokenTime: '',
    Description: '',
    UserPublicKey: [],
    jwt_token : ''
  };

  //get token details from userData item from local storage and assign it to tokenList
  tokenList = JSON.parse(localStorage.getItem('token details'));

  // define array for user public keys
  public_keys: any = [];

  // start constructor for DomSanitizer
  constructor(private _sanitizer: DomSanitizer,private location: Location) {
    
    localStorage.setItem("page location", JSON.stringify(this.location.path()));

    // start check if the user not choose the image
    if (this.tokenList.IconURL === '') {

      // assign token_image by true
      this.token_image = true;

    }
    // end check if the IconURL is null
    // start check else if the IconURL is null
    else {
      this.token_image = false;
      this.default_image = "../../assets/images/download.png"
    }
    // end check else if the IconURL is null

    // define token id and assign it to tokenDetails.id
    this.tokenDetails.id = this.tokenList.TokenID;
    // define token name and assign it to tokenDetails.name
    this.tokenDetails.name = this.tokenList.TokenName;
    // define token sybmbol and assign it to tokenDetails.symbol
    this.tokenDetails.symbol = this.tokenList.TokenSymbol;
    // define token icon and assign it to tokenDetails.icon
    this.tokenDetails.icon = this.tokenList.IconURL;
    // define token total supply and assign it to tokenDetails.TokensTotalSupply
    this.tokenDetails.TokensTotalSupply = this.tokenList.TokensTotalSupply;
    // define token value and assign it to tokenDetails.TokenValue
    this.tokenDetails.TokenValue = this.tokenList.TokenValue;
    // define token reissuability and assign it to tokenDetails.Reissuability
    this.tokenDetails.Reissuability = this.tokenList.Reissuability;
    // define token precision and assign it to tokenDetails.Precision
    this.tokenDetails.Precision = this.tokenList.Precision;
    // define token usage type and assign it to tokenDetails.UsageType
    this.tokenDetails.UsageType = this.tokenList.UsageType;
    // define token type and assign it to tokenDetails.TokenType
    this.tokenDetails.TokenType = this.tokenList.TokenType;
    // define token value dynamic and assign it to tokenDetails.ValueDynamic
    this.tokenDetails.ValueDynamic = this.tokenList.ValueDynamic;
    // define token contract id and assign it to tokenDetails.ContractID
    this.tokenDetails.ContractID = this.tokenList.ContractID;
    // define token dynamic price and assign it to tokenDetails.Dynamicprice
    this.tokenDetails.Dynamicprice = this.tokenList.Dynamicprice;
    // define token description and assign it to tokenDetails.Description
    this.tokenDetails.Description = this.tokenList.Description;
    // define token user public key and assign it to tokenDetails.UserPublicKey
    this.tokenDetails.UserPublicKey = this.tokenList.UserPublicKey;
    // define token user public key and assign it to this.public_keys
    this.public_keys = this.tokenList.UserPublicKey;

    this.tokenDetails.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));

    // decrypt the icon url to image and assign it to imagePath
    this.imagePath = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,'
      + this.tokenList.TokenIcon);
    console.log("public key array : " + this.tokenDetails.UserPublicKey);
  }

  // start ngOnInit function
  ngOnInit() {

    // start check if the user is from inovatian accounts
    if ((this.emailUser === "hatim@inovatian.com") ||
      (this.emailUser === "hatim.fees@inovatian.com") ||
      (this.emailUser === "hatim.refund@inovatian.com") ||
      (this.emailUser === "askme557@gmail.com")) {
      // assign isAdmin equal true
      this.isAdmin = true;
    }
    // end check if the user is from inovatian accounts
    // start check else if the user is from inovatian accounts       
    else {
      // assign isAdmin equal false
      this.isAdmin = false;
    }
    // end check else if the user is from inovatian accounts       
  }
  // end ngOnInit function
}
