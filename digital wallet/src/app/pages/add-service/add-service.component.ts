import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
// import services service
import { ServicesService } from '../../shared/services/services/services.service';
// import Toastr Service
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ngx-add-service',
  templateUrl: './add-service.component.html',
  styleUrls: ['./add-service.component.scss', '../pages.component.scss']
})
export class AddServiceComponent implements OnInit {

  // define userDetails input type , amount , Mbytes , Duration , day , bandwidth , password and public_key
  @Input() serviceDetails = {
    type: '',
    amount: 0,
    Mbytes: true,
    Duration: 0,
    day: true,
    bandwidth: 0,
    password: '',
    public_key: '',
    jwt_token : ''
  };

  // define type options
  typeOptions = [
    { id: 1, name: "please choose service type" },
    { id: 1, name: "Internet" },
    { id: 2, name: "VOIP" }
  ];

  // define day options
  dayOptions = [
    { id: 0, name: "please choose service duration" },
    { id: 1, name: "Day" },
    { id: 2, name: "Hour" },
    { id: 3, name: "Minutes" }
  ];

  // define bandwidth options
  bandwidthptions = [
    { id: 0, name: "please choose service bandwidth" },
    { id: 1, name: "Mbytes" },
    { id: 2, name: "Kilobytes" }
  ];

  // start constructor for services service , toastr and router
  constructor(
    // define services service
    private serviceSvr: ServicesService,
    // define router
    private router: Router,
    // define toastr service
    private toastr: ToastrService,
    private location: Location) { 
      localStorage.setItem("page location", JSON.stringify(this.location.path()));

    }

  // end constructor for services service , toastr and router

  // start ngOnInit function
  ngOnInit() {
  }
  // end ngOnInit function


  // start function of add service
  OnSubmit(form: NgForm) {

    // define dot
    var dot = "."

    // get amount of service and assign it to amount
    var amount = String(form.value.amount);

    // start check if the type is null
    if (form.value.type === '' || form.value.type === null) {
      // show notification if the type is null
      this.toastr.error('Service type is required', '',
        {
          timeOut: 5000
        });
    }
    // end check if the type is null

    // start check if the amount is null
    else if (form.value.amount === '' || form.value.amount === null) {
      // show notification if the amount is null
      this.toastr.error('Service type is required', '',
        {
          timeOut: 5000
        });
    }
    // end check if the amount is null

    // start check if the duration is null
    else if (form.value.Duration === '' || form.value.Duration === null) {
      // show notification if the amount is null
      this.toastr.error('Service Duration is required', '',
        {
          timeOut: 5000
        });
    }
    // end check if the duration is null

    // start check if the bandwidth is null
    else if (form.value.bandwidth === '' || form.value.bandwidth === null) {
      // show notification if the bandwidth is null
      this.toastr.error('Service bandwidth is required', '',
        {
          timeOut: 5000
        });
    }
    // end check if the bandwidth is null

    // start check if the amount larger than 1440 and last than 0 
    else if (form.value.amount > 1440 || form.value.amount <= 0) {
      // show notification if if the amount larger than 1440 and last than 0 
      this.toastr.error('amount must be between 1 and 1440', '',
        {
          timeOut: 5000
        });
    }
    // end check if the amount larger than 1440 and last than 0 

    // start check if the amount has dot
    else if (amount.indexOf(dot) !== -1) {
      // show notification if if the amount has dot 
      this.toastr.error('amount must be integer', '',
        {
          timeOut: 5000
        });
    }
    // end check if the amount has dot

    // start check if the bandwidth larger than 1024 and last than 0 
    else if (form.value.bandwidth > 1024 || form.value.bandwidth <= 0) {
      // show notification if if the bandwidth larger than 1024 and last than 0 
      this.toastr.error('bandwidth must be between 1 and 1024', '',
        {
          timeOut: 5000
        });
    }
    // end check if the bandwidth larger than 1024 and last than 0 

    // start check if the user choose day
    else if (form.value.day === true) {
      //start if the Duration larger than 31 and last than 0

      if (form.value.Duration > 31 || form.value.Duration <= 0) {
        // show notification if if the Duration larger than 31 and last than 0
        this.toastr.error('duration must be between 1 and 31', '',
          {
            timeOut: 5000
          });
      }
      //end if the Duration larger than 31 and last than 0
    }
    // end check if the user choose day

    // start check if the user choose minutes
    else if (form.value.day === false) {
      //start if the Duration larger than 1440 and last than 0
      if (form.value.Duration > 1440 || form.value.Duration <= 0) {
        // show notification if if the Duration larger than 1044 and last than 0
        this.toastr.error('duration must be between 1 and 1440', '',
          {
            timeOut: 5000
          });
      }
      //end if the Duration larger than 1440 and last than 0
    }
    // end check if the user choose minutes

    // start else 
    else {
      // get type of service and assign it to serviceDetails.type
      this.serviceDetails.type = form.value.type;
      // get amount of service and assign it to serviceDetails.amount
      this.serviceDetails.amount = form.value.amount;
      // get bandwidth of service and assign it to serviceDetails.bandwidth
      this.serviceDetails.bandwidth = form.value.bandwidth;
      // get AccountPublicKey from userData from local storage and assign it to serviceDetails.public_key
      this.serviceDetails.public_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey;
      // get AccountPassword from userData from local storage and assign it to serviceDetails.password
      this.serviceDetails.password = JSON.parse(localStorage.getItem('userData')).AccountPassword;
       // define jwt token 
this.serviceDetails.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));

      // print request of add service
      console.log("service details : " + this.serviceDetails);

      // start check if the user choose day 
      if (form.value.day === 'Day') {
        //define serviceDetails.day by true
        this.serviceDetails.day = true;
      }
      // end check if the user choose day 

      // start check if the user not choose day 
      else {
        //define serviceDetails.day by false
        this.serviceDetails.day = false;
      }

      // start check if the user choose Mbytes 
      if (form.value.Mbytes === 'Mbytes') {
        //define serviceDetails.Mbytes by true
        this.serviceDetails.Mbytes = true;
      }
      // end check if the user choose Mbytes 

      // start check if the user not choose Mbytes 
      else {
        //define serviceDetails.Mbytes by false
        this.serviceDetails.Mbytes = false;
      }
      // end check if the user not choose Mbytes 

      // start call addService from services service
      this.serviceSvr.addService(this.serviceDetails).subscribe((data: any) => {

        // add add service response to local storage
        localStorage.setItem("service response", JSON.stringify(data));

        // start check if the login response has MessageAPI
        if (JSON.parse(localStorage.getItem('service response')).MessageAPI) {
          // show notification add service error
          this.toastr.error(JSON.parse(localStorage.getItem('service response')).MessageAPI)

        }
        // end check if the login response has MessageAPI

        // start check if the login response not has MessageAPI
        else {
          // show notification Service created successfully
          this.toastr.success('Service created successfully', '',
            {
              timeOut: 5000
            });

          // go to send page
          this.router.navigate(['./pages/send']);
        }
        // end check if the login response not has MessageAPI
      });
      // end call addService from services service
    }
    // end else 
  }
  // end function of add service
}