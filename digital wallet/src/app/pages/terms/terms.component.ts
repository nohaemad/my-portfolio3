import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'ngx-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss', '../pages.component.scss']
})
export class TermsComponent implements OnInit {

  constructor(private location: Location) { 
    localStorage.setItem("page location", JSON.stringify(this.location.path()));

  }

  ngOnInit() {
  }

}
