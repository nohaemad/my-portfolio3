import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
// import tokens service
import { TokensService } from '../../shared/services/tokens/tokens.service';
// import Toastr Service
import { ToastrService } from 'ngx-toastr';
// import jquery
import * as $ from 'jquery';
// import strftime function from time.js file
declare const strftime: any;

@Component({
  selector: 'ngx-add-token',
  templateUrl: './add-token.component.html',
  styleUrls: ['./add-token.component.scss', '../pages.component.scss']
})
export class AddTokenComponent implements OnInit {

  // define ipLists for user public key array
  ipLists = [];

  // define special_char_amount3 for token name 
  special_char_amount3 = 0;

  // define tokenDetails input
  @Input() tokenDetails = {
    name: '',
    symbol: '',
    icon: '',
    TokensTotalSupply: 0.000000,
    TokenValue: 0.000000,
    Reissuability: true,
    Precision: 0,
    UsageType: '',
    TokenType: '',
    ValueDynamic: true,
    ContractID: '',
    Dynamicprice: 0.000000,
    TokenTime: '',
    Description: '',
    password: '',
    public_key: '',
    UserPublicKey: [],
    jwt_token: ''
  };

  // define sellersPermitFile for uploaded file
  sellersPermitFile: any;

  // define sellersPermitString for encrypt uploaded file
  sellersPermitString: string;

  // define pkarray array for user public key
  pkarray: Array<string> = new Array<string>();

  // define list_pks2 map for user public key
  list_pks2: Map<string, string> = new Map<string, string>();

  // define reissuability options
  ReissuabilityOptions = [
    { id: 0, name: true },
    { id: 1, name: false }
  ];

  // define value dynamic options
  valueDynamicOptions = [
    { id: 0, name: true },
    { id: 1, name: false }
  ];

  // define usage type options
  usageTypeOptions = [
    { id: 0, name: "security" },
    { id: 1, name: "utility" }
  ];

  // define token type options
  tokenTypeOptions = [
    { id: 0, name: "public" },
    { id: 1, name: "private" }
  ];

  // define special_charchters for special characters

  special_charchters = ['"', "!", "$", "%", "&", "`", "'", "(", ")", "*", "+", ",", "-", "/", ":", ";",
    "<", "=", ">", "?", "@", "[", "\\", "]", "^", "`", "{", "|", "}", "#", "~",
    '"', "_"];

  // start constructor for tokens service , toastr and router
  constructor(
    // define tokens service
    private tokenSvr: TokensService,
    // define router
    private router: Router,
    // define toastr service
    private toastr: ToastrService,
    private location: Location) {
    localStorage.setItem("page location", JSON.stringify(this.location.path()));
  }
  // end constructor for tokens service , toastr and router

  // start function for when the user click on add button append the jquery code to html
  addPK() {
    $('.ip-list').append(
      '<div class="ip-input">' +
      '<div class="row">' +
      '<div class="col-md-2">' +
      '<label>User Public Key</label>  <span class="astric">*</span>' +
      '</div>' +
      '<div class="col-md-8">' +

      '<div class="ip">' +
      '<input type="text" placeholder="Enter user public key" name="byte[]" placeholder="" class="form-control" style="border-radius: 20px !important;margin-bottom: 20px;">' +

      '</div>' +
      '</div>' +

      '<div class="col-md-2">' +
      '<div class="btn-remove-service" (click)="removePK()"  style="height: 45px;background: transparent;border: 1px solid #ddd;width: 65%;text-align: center;line-height: 45px;color: #000;border-radius: 10px;cursor: pointer;font-size: 18px;">' +
      '<a class=" ">x</a>' +
      '</div>' +
      '</div>' +
      '</div>' +

      '</div>'
    );
  }
  // end function for when the user click on add button append the jquery code to html

  // start ngOnInit function
  ngOnInit() {

    // start when the function is ready
    $(document).ready(function () {
      // start function when user click on remove button delete ip input
      $(document.body).on('click', '.btn-remove-service', function () {
        $(this).closest('.ip-input').remove();
      });
      // end function when user click on remove button delete ip input
    });
    // end when the function is ready

    // start when the click button has class save add the inputs value to titles2
    $(document.body).on('click', '.save', () => {
      // start add value inputs of user public and assign it to titles2
      const titles2 = $('input[name^=byte]').map(function (idx, elem) {

        return $(elem).val();
      }).get();

      // end add value inputs of user public and assign it to titles2

      // print titles2
      console.log('titles ' + titles2);

      // get titles2 and assign it to list_pks2
      this.list_pks2 = titles2;

      // print list_pks2
      console.log('list ids ' + this.list_pks2);
    });
    // end when the click button has class save add the inputs value to titles2
  }
  // end ngOnInit function

  // start increment_special_amount3 function
  increment_special_amount3() {
    // increase special_char_amount3 by 1
    this.special_char_amount3 = this.special_char_amount3 + 1;
  }
  // end increment_special_amount3 function

  // start get_special_amount3 function
  get_special_amount3() {
    // get special_char_amount3
    return this.special_char_amount3;
  }
  // end get_special_amount3 function

  // start set_special_amonnt3 function
  set_special_amonnt3() {
    // set special_char_amount3 by 0
    this.special_char_amount3 = 0;
  }
  // end set_special_amonnt3 function

  // start function for amount be float
  pad(num: number, size: number): string {
    let s = num + "";
    while (s.length < size) s = s + '0';
    return s;
  }
  // end function for amount be float

  // start function of add token            
  OnSubmit(addTokenForm: NgForm) {

    // make tokenDetails.UserPublicKey null
    this.tokenDetails.UserPublicKey = [];

    // start function for get all items of list_pks2 and push it tokenDetails.UserPublicKey
    this.list_pks2.forEach((key, value) => {
      if (this.list_pks2[value].length !== 0) {
        console.log('value => ' + this.list_pks2[value]);
        this.tokenDetails.UserPublicKey.push(this.list_pks2[value]);
      }
    });
    // end function for get all items of list_pks2 and push it tokenDetails.UserPublicKey

    // start chck if the name bigger than 20 characters
    if (addTokenForm.value.name > 20) {
      // show notification if the name bigger than 20 characters
      this.toastr.error('token name must be less than 13 characters - برجاء ادخال الاسم اقل من 20 حرف   ', '',
        {
          timeOut: 5000
        });
    }
    // end chck if the name bigger than 20 characters

    // start chck if the total supply least or equal 0
    if (addTokenForm.value.TokensTotalSupply <= 0) {
      // show notification if the total supply least or equal 0
      this.toastr.error('token total supply must be larger than 0 - برجاء ادخال رقم اكبر من 0   ', '',
        {
          timeOut: 5000
        });
    }
    // end chck if the total supply least or equal 0

    // start chck if the token value least or equal 0
    if (addTokenForm.value.TokenValue <= 0) {
      // show notification if the token value least or equal 0
      this.toastr.error('token value must be larger than 0 - برجاء ادخال رقم اكبر من 0  ', '',
        {
          timeOut: 5000
        });
    }
    // end chck if the token value least or equal 0

    // start chck if the token precision least 0 or equal bigger than 5
    if (addTokenForm.value.Precision <= 0 || addTokenForm.value.Precision > 5) {
      // show notification if the token precision least 0 or equal bigger than 5
      this.toastr.error('token Precision must be between 0 and 5 - برجاء ادخال الرقم بين 0 و 5   ', '',
        {
          timeOut: 5000
        });
    }
    // end chck if the token precision least 0 or equal bigger than 5

    // start check if the token name is null
    if (addTokenForm.value.name === '' || addTokenForm.value.name === null) {
      // show notification if the token name is null
      this.toastr.error('Token name is required - برجاء ادخال الاسم', '',
        {
          timeOut: 5000
        });
    }
    // end check if the token name is null

    // start check if the token symbol is null
    if (addTokenForm.value.symbol === '' || addTokenForm.value.symbol === null) {
      // show notification if the token symbol is null
      this.toastr.error('Token symbol is required - برجاء ادخال الرمز', '',
        {
          timeOut: 5000
        });
    }
    // end check if the token symbol is null

    // start check if the token total supply is null
    if (addTokenForm.value.TokensTotalSupply === '' || addTokenForm.value.TokensTotalSupply === null) {
      // show notification if the token total supply is null
      this.toastr.error('Token total supply is required ', '',
        {
          timeOut: 5000
        });
    }
    // end check if the token total supply is null

    // start check if the token value is null
    if (addTokenForm.value.TokenValue === '' || addTokenForm.value.TokenValue === null) {
      // show notification if the token value is null
      this.toastr.error('Token value is required - برجاء ادخال القيمة', '',
        {
          timeOut: 5000
        });
    }
    // end check if the token value is null

    // start check if the token reissuability is null
    if (addTokenForm.value.Reissuability === '' || addTokenForm.value.Reissuability === null) {
      // show notification if the token reissuability is null
      this.toastr.error('Token reissuability is required', '',
        {
          timeOut: 5000
        });
    }
    // end check if the token reissuability is null

    // start check if the token usage type is null
    if (addTokenForm.value.UsageType === '' || addTokenForm.value.UsageType === null) {
      // show notification if the token usage type is null
      this.toastr.error('Token usage type is required - برجاء ادخال نوع الاستخدام', '',
        {
          timeOut: 5000
        });
    }
    // end check if the token usage type is null

    // start check if the token type is null
    if (addTokenForm.value.TokenType === '' || addTokenForm.value.TokenType === null) {
      // show notification if the token type is null
      this.toastr.error('Token type is required - برجاء ادخال النوع', '',
        {
          timeOut: 5000
        });
    }
    // end check if the token type is null

    // start check if the token type is public and contract id is null
    if (addTokenForm.value.TokenType === "public") {
      //start check if the contract id is null
      if (addTokenForm.value.ContractID === '' || addTokenForm.value.ContractID === null) {
        // show notification if the contract id is null
        this.toastr.error('Token contract id is required', '',
          {
            timeOut: 5000
          });
      }
      //end check if the contract id is null
    }
    // end check if the token type is public and contract id is null

    // start check if the token type is private and user public key is null
    if (addTokenForm.value.TokenType === "private") {
      //start check if the user public key is null
      if (addTokenForm.value.byte === '' || addTokenForm.value.byte === null) {
        // show notification if the user public key is null
        this.toastr.error('Token user public key is required', '',
          {
            timeOut: 5000
          });
      }
      //end check if the user public key is null
    }
    // end check if the token type is private and user public key is null

    // start else
    else {

      // call strftime and assign it to tokenDetails.TokenTime 
      this.tokenDetails.TokenTime = strftime('%FT%H:%M:%S%z');

      // make slice for make time 02:00 and assign it to splitTime
      var splitTime = this.tokenDetails.TokenTime.slice(0, -2) + ":" + this.tokenDetails.TokenTime.slice(-2);

      // print splitTime
      console.log("split time : " + splitTime);

      // print tokenDetails.TokenTime
      console.log("new time : " + this.tokenDetails.TokenTime);

      // assign splitTime to tokenDetails.TokenTime
      this.tokenDetails.TokenTime = splitTime;

      // define supplyInput and assign it to TokensTotalSupply
      var supplyInput = String(addTokenForm.value.TokensTotalSupply);

      // define valueInput and assign it to TokenValue
      var valueInput = String(addTokenForm.value.TokenValue);

      // define dynamicInput and assign it to Dynamicprice
      var dynamicInput = String(addTokenForm.value.Dynamicprice);

      // define dot
      var dot = ".";

      // start check if the supplyInput not has dot
      if (supplyInput.indexOf(dot) === -1) {
        //print this.pad(0, 6) 
        console.log("pad function : " + this.pad(0, 6));

        // define floatNum and assign it to this.pad(0, 6)
        var floatNum = this.pad(0, 6);

        // make combination between supplyInput , floatNum and assign it to supplyInput
        supplyInput = supplyInput + "." + floatNum;

        // print supplyInput
        console.log("amount input :" + supplyInput);

      }
      // end check if the supplyInput not has dot

      // start else
      else {

        // make split and get before dot and after dot and assign it to supply_before
        var supply_before = supplyInput.split(".");

        // print supply_before
        console.log("amount before : " + supply_before);

        // define floatNum and assign it to this.pad(Number(supply_before[1]), 6)
        var floatNum = this.pad(Number(supply_before[1]), 6);

        // make combination between supply_before[0]  , floatNum and assign it to supplyInput
        supplyInput = supply_before[0] + "." + floatNum;

        // print supplyInput
        console.log("amount input :" + supplyInput);
      }
      // end else

      // start check if the valueInput not has dot
      if (valueInput.indexOf(dot) === -1) {
        //print this.pad(0, 6) 
        console.log("pad function : " + this.pad(0, 6));

        // define floatNum and assign it to this.pad(0, 6)
        var floatNum = this.pad(0, 6);

        // make combination between valueInput , floatNum and assign it to valueInput
        valueInput = valueInput + "." + floatNum;

        // print valueInput
        console.log("amount input :" + valueInput);
      }
      // end check if the valueInput not has dot

      // start else
      else {
        // make split and get before dot and after dot and assign it to value_before
        var value_before = valueInput.split(".");

        // print value_before
        console.log("amount before : " + value_before);

        // define floatNum and assign it to this.pad(Number(supply_before[1]), 6)
        var floatNum = this.pad(Number(supply_before[1]), 6);

        // make combination between value_before[0]  , floatNum and assign it to valueInput
        valueInput = value_before[0] + "." + floatNum;

        // print valueInput
        console.log("amount input :" + valueInput);
      }
      // end else

      // start check if the dynamicInput not has dot
      if (dynamicInput.indexOf(dot) === -1) {

        //print this.pad(0, 6) 
        console.log("pad function : " + this.pad(0, 6));

        // define floatNum and assign it to this.pad(0, 6)
        var floatNum = this.pad(0, 6);

        // make combination between dynamicInput , floatNum and assign it to dynamicInput
        dynamicInput = dynamicInput + "." + floatNum;

        // print dynamicInput
        console.log("amount input :" + dynamicInput);
      }
      // end check if the dynamicInput not has dot

      // start else
      else {
        // make split and get before dot and after dot and assign it to dynamic_before
        var dynamic_before = valueInput.split(".");

        // print dynamic_before
        console.log("amount before : " + dynamic_before);

        // define floatNum and assign it to this.pad(Number(dynamic_before[1]), 6)
        var floatNum = this.pad(Number(dynamic_before[1]), 6);

        // make combination between dynamic_before[0]  , floatNum and assign it to supplyInput
        dynamicInput = dynamic_before[0] + "." + floatNum;

        // print dynamicInput
        console.log("amount input :" + dynamicInput);
      }
      // end else

      // start check if the reissuability is true
      if (addTokenForm.value.Reissuability === true) {

        // make tokenDetails.Reissuability equal true
        this.tokenDetails.Reissuability = true
      }
      // end check if the reissuability is true

      // start check if the reissuability is false
      else {
        // make tokenDetails.Reissuability equal false
        this.tokenDetails.Reissuability = false
      }
      // end check if the reissuability is false

      // start check if the ValueDynamic is true
      if (addTokenForm.value.ValueDynamic === true) {
        // make tokenDetails.ValueDynamic equal true
        this.tokenDetails.ValueDynamic = true
      }
      // end check if the ValueDynamic is true

      // start check if the ValueDynamic is false
      else {
        // make tokenDetails.ValueDynamic equal false
        this.tokenDetails.ValueDynamic = false
      }
      // end check if the ValueDynamic is false

      //get sellersPermitString and assign it to tokenDetails.icon
      this.tokenDetails.icon = this.sellersPermitString;

      //get AccountPassword from userData item from local storage and assign it to tokenDetails.password
      this.tokenDetails.password = JSON.parse(localStorage.getItem('userData')).AccountPassword;

      //get AccountPublicKey from userData item from local storage and assign it to tokenDetails.public_key
      this.tokenDetails.public_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey;

      this.tokenDetails.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));

      // start call addToken from tokens service
      this.tokenSvr.addToken(this.tokenDetails).subscribe((data: any) => {
        // start show notification Token created successfully Successfuly
        this.toastr.success('Token created successfully', '',
          {
            timeOut: 5000
          });
        // end show notification Token created successfully

        // add token response to local storage
        localStorage.setItem("token response", JSON.stringify(data));

        // go to tokens page
        this.router.navigate(['./pages/tokens']);
      });
      // end call addToken from tokens service
    }
    // end else
  }
  // end function of add token            

  // start function for read the token icon
  picked(event) {
    let fileList: FileList = event.target.files;
    const file: File = fileList[0];
    this.sellersPermitFile = file;
    console.log("icon fie : " + this.sellersPermitFile)
    this.handleInputChange(file); //turn into base64
  }
  // end function for read the token icon

  // start function for check the icon url pattern
  handleInputChange(files) {
    var file = files;
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onloadend = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  // end function for check the icon url pattern

  // start function for encrypt the token icon  
  _handleReaderLoaded(e) {
    let reader = e.target;
    var base64result = reader.result.substr(reader.result.indexOf(',') + 1);
    this.sellersPermitString = base64result;
    console.log("base 64 icon : " + this.sellersPermitString);
  }
  // end function for encrypt the token icon
}

