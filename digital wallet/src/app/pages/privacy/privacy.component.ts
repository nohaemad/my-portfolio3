import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'ngx-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.scss', '../pages.component.scss']
})
export class PrivacyComponent implements OnInit {

  constructor(private location: Location) { 
    localStorage.setItem("page location", JSON.stringify(this.location.path()));
  }

  ngOnInit() {
  }

}
