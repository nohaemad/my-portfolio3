import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
// import account service
import { AccountService } from '../../shared/services/account/account.service';

@Component({
  selector: 'ngx-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss', '../pages.component.scss']
})
export class TransactionsComponent implements OnInit {

  // define transaction for password and public key
  transaction = {
    password: '',
    public_key: '',
    jwt_token : ''
  }

  // define userDetails for password and public key
  userDetails = {
    PublicKey: JSON.parse(localStorage.getItem('userData')).AccountPublicKey,
    Password: JSON.parse(localStorage.getItem('userData')).AccountPassword
  }

  // define transactions
  transactions: any = [];

  // start constructor for account service and toastr service
  constructor(private accountSvr: AccountService,private location: Location) {
    localStorage.setItem("page location", JSON.stringify(this.location.path()));

   }
  // end constructor for account service and toastr service

  // start ngOnInit function
  ngOnInit() {

    // start check if the public key
    if (this.userDetails.PublicKey != null) {

      // get AccountPassword from userData from local storage and assign it to transaction.password 
      this.transaction.password = JSON.parse(localStorage.getItem('userData')).AccountPassword;
      // get AccountPublicKey from userData from local storage and assign it to transaction.public_key 
      this.transaction.public_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey;

      this.transaction.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));

      // start call getTransactions from account service
      this.accountSvr.getTransactions(this.transaction).subscribe((transactions: any) => {

        // add get transactions response to local storage
        localStorage.setItem('transactionDetails', JSON.stringify(transactions));

        // get transactionDetails from local storage and assign it to transactions 
        this.transactions = JSON.parse(localStorage.getItem("transactionDetails")).normal;
      })
      // end call getTransactions from account service
    }
    // end check if the public key
  }
  // end ngOnInit function
}

