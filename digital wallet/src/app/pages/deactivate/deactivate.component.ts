import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
// import settings service
import { SettingsService } from '../../shared/services/settings/settings.service';
// import Toastr Service
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ngx-deactivate',
  templateUrl: './deactivate.component.html',
  styleUrls: ['./deactivate.component.scss', '../pages.component.scss']
})
export class DeactivateComponent implements OnInit {

  // define userDetails input for reason , password , public key and name
  @Input() userDetails = { reason: '', password: '', public_key: '', name: '',jwt_token :'' }


  // start constructor for setting service , toastr and router
  constructor(
    // define settings service
    private settingsSvr: SettingsService,
    // define router
    private router: Router,
    // define toastr service 
    private toastr: ToastrService,
    private location: Location) {
      localStorage.setItem("page location", JSON.stringify(this.location.path()));
     }
  // end constructor for setting service , toastr and router

  // start ngOnInit function
  ngOnInit() { }
  // end ngOnInit function

  // start function of deactivate account            
  deactivate_account(form: NgForm) {

    // start check if the reason is null
    if (form.value.reason === '' || form.value.reason === null) {
      // show notification if the reason is null
      this.toastr.error('Reason is required', '',
        {
          timeOut: 2000
        });
    }
    // end check if the reason is null

    // start else check if the reason is null
    else {

      // start check if user choose yes
      if (window.confirm('Are you sure, you want to deactivate your account?')) {

        //get AccountPassword from userData item from local storage and assign it to userDetails.password
        this.userDetails.password = JSON.parse(localStorage.getItem('userData')).AccountPassword;

        //get AccountName from userData item from local storage and assign it to userDetails.name
        this.userDetails.name = JSON.parse(localStorage.getItem('userData')).AccountName;

        //get AccountPublicKey from userData item from local storage and assign it to userDetails.public_key
        this.userDetails.public_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey;

        //get reason and assign it to userDetails.reason
        this.userDetails.reason = form.value.reason;

        this.userDetails.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));


        // start call deactivateAccount from settings service
        this.settingsSvr.deactivateAccount(this.userDetails).subscribe((data: any) => {

          // add deactivate response to local storage
          localStorage.setItem("deactivate response", JSON.stringify(data));

          // start show notification your account is deactivated successfuly         
          this.toastr.success("your account is deactivated successfuly", '',
            {
              timeOut: 2000
            });

          // go to login page
          this.router.navigate(['/auth//login']);

        })
        // end call deactivateAccount from settings service
      }
      // end check if user choose yes
    }
    // end else check if the reason is null
  }
  // end function of deactivate account            
}
