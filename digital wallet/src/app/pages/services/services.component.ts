import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

// import services service
import { ServicesService } from '../../shared/services/services/services.service';

@Component({
  selector: 'ngx-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss', '../pages.component.scss']
})
export class ServicesComponent implements OnInit {

  // define services
  services: any = [];

  // define check_string for response of check consumption 
  check_string = '';

  // define service for password , public key and id
  service = {
    // define password
    password: '',
    // define public key
    public_key: '',
    // define id
    id: '',
    jwt_token : ''
  }

  // define isService
  isService = false;

  // start constructor for services service
  constructor(
    private servicesSvr: ServicesService,private location: Location) { 
      localStorage.setItem("page location", JSON.stringify(this.location.path()));
    }
  // end constructor for services service

  // start ngOnInit function
  ngOnInit() {

    // get AccountPassword from userData from local storage and assign it to service.password 
    this.service.public_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey
    // get AccountPublicKey from userData from local storage and assign it to service.public_key 
    this.service.password = JSON.parse(localStorage.getItem('userData')).AccountPassword;

    this.service.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));

    // start call getServices from services service
    this.servicesSvr.getServices(this.service).subscribe((services: any) => {
      // add get transactions response to local storage
      localStorage.setItem('servicesDetails', JSON.stringify(services));

      // start check if servicesDetails has MessageAPI 
      if (JSON.parse(localStorage.getItem('servicesDetails')).MessageAPI) {
        // asign isService = false
        this.isService = false;
      }
      // end check if servicesDetails not has MessageAPI 

      // start check else if servicesDetails not has MessageAPI 
      else {
        // asign isService = true
        this.isService = true;

        // get services from local storage and assign it to this.services
        this.services = JSON.parse(localStorage.getItem("servicesDetails"));
      }
      // end check else if servicesDetails not has MessageAPI 
    });
    // end call getServices from services service
  }
  // end ngOnInit function

  // start getConsum function
  getConsum(id) {

    // get AccountPassword from userData from local storage and assign it to service.password 
    this.service.public_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey
    // get AccountPublicKey from userData from local storage and assign it to service.public_key 
    this.service.password = JSON.parse(localStorage.getItem('userData')).AccountPassword;

    this.service.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));

    // get id and assign it to service.id
    this.service.id = id;

    // alert service
    console.log("service request : " + JSON.stringify(this.service));

    // start call getConsumbtion from services service
    this.servicesSvr.getConsumbtion(this.service).subscribe((data: any) => {

      // add get consumbtion response to local storage
      localStorage.setItem('get consumbtion', JSON.stringify(data));

      // start check if servicesDetails not has MessageAPI 
      if (!JSON.parse(localStorage.getItem('get consumbtion')).MessageAPI) {
      }
      // end check if servicesDetails not has MessageAPI 
      // start check else if servicesDetails not has MessageAPI 
      else {
        // get get consumbtion from local storage and assign it to check_string
        if (JSON.parse(localStorage.getItem('get consumbtion')).MessageAPI === "this Voucher is not used yet") {
          this.check_string = "this service is not used yet"
        } else {
          this.check_string = JSON.parse(localStorage.getItem('get consumbtion')).MessageAPI;
        }
      }
      // end check else if servicesDetails not has MessageAPI 
    })
    // end call getConsumbtion from services service
  }
  // end getConsum function
}
