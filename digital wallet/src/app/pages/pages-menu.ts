import { NbMenuItem } from "@nebular/theme";

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: "Send",
    icon: "eva eva-arrow-upward",
    link: "send"
  },
  {

      title: "Transactions",
    icon: "eva eva-browser",
    children: [
      {
        title: "Normal Transactions",
        link: "transactions"
      },
      {
        title: "Refund Transactions",
        link: "refund-transaction"
      },
      {
        title: "Token Creation Transactions",
        link: "token-creation-transaction"
      }
    ]
  },
  {
    title: "Balance",
    icon: "fab fa-bitcoin",
    link: "balance"
  },
  {
    title: "Tokens",
    icon: "fas fa-coins",
    link: "tokens"
  },

  {
    title: "Services",
    icon: "eva eva-archive",
    link: "services"
  },

  {
    title: "File Storage",
    icon: "fas fa-file-upload",
    link: "file-storage"
  },

  {

    title: "Settings",
  icon: "eva eva-settings-2",
  children: [
    {
      title: "Privacy policy",
      link: "privacy"
    },
    {
      title: "Terms and conditions",
      link: "terms"
    },
   
  ]
},
  // {
  //   title: "QR Code",
  //   icon: "fas fa-qrcode",
  //   link: "qr"
  // },
  
];
