import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefundTokenComponent } from './refund-token.component';

describe('RefundTokenComponent', () => {
  let component: RefundTokenComponent;
  let fixture: ComponentFixture<RefundTokenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefundTokenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundTokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
