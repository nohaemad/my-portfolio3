import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';

// import Toastr Service
import { ToastrService } from 'ngx-toastr';
// import tokens service
import { TokensService } from '../../shared/services/tokens/tokens.service';
// import account Service
import { AccountService } from '../../shared/services/account/account.service';
// import jquery
import * as $ from 'jquery';
// import CryptoJS
import * as CryptoJS from 'crypto-js';
// import strftime function for time
declare const strftime: any;
// import setParam2 function for assign the parameters
declare const setParam2: any;
// import gethSig function for signature
declare const gethSig: any;



declare const encrypt_transaction2: any;

@Component({
  selector: 'ngx-refund-token',
  templateUrl: './refund-token.component.html',
  styleUrls: ['./refund-token.component.scss', '../pages.component.scss']
})
export class RefundTokenComponent implements OnInit {

  // define tokenDetails input
  tokenDetails = {
    id: '',
    Sender: '',
    receiver: '',
    flat_currency: true,
    Amount: 0.000000,
    hash: '',
    timeZone: '',
    priv_file: "",
    jwt_token: '',
    encrypt_data: ''
  };

  // define searchDetails input
  @Input() searchDetails = { searchText: '', name: '', password: '', jwt_token: '' };

  // define h_sig
  h_sig: '';

  // define fileContent
  fileContent: string = '';

  // define flatCurrencyOptions
  flatCurrencyOptions = [
    { id: 0, name: "please choose flat Currency" },
    { id: 1, name: true },
    { id: 2, name: false }
  ];

  // start define senderDetails for data of user
  senderDetails = {
    // define public key
    PublicKey: JSON.parse(localStorage.getItem('userData')).AccountPublicKey,
    // define private key
    PrivateKey: JSON.parse(localStorage.getItem('userData')).AccountPrivateKey,
  }

  //get token details from userData item from local storage and assign it to tokenList
  tokenList = JSON.parse(localStorage.getItem('token details'));

  // define receiverDetails for search
  receiverDetails: any = [];

  // start constructor for account service , tokens service , toastr and router

  constructor(
    // define router
    private router: Router,
    // define toastr service
    private toastr: ToastrService,
    // define tokens service
    private tokenSvr: TokensService,
    // define account service
    private accountSvr: AccountService,
    private location: Location
  ) {

    // define token id and assign it to tokenDetails.id
    this.tokenDetails.id = this.tokenList.TokenID;

    localStorage.setItem("page location", JSON.stringify(this.location.path()));

  }

  // start ngOnInit function
  ngOnInit() {
    // start check if searchDetails2 is exist
    if (JSON.parse(localStorage.getItem("searchDetails2"))) {
      // start define receiverDetails after search
      this.receiverDetails = {
        //get PublicKey from searchDetails2 item from local storage and assign it to PublicKey
        PublicKey: JSON.parse(localStorage.getItem('searchDetails2')).PublicKey,
        //get UserName from searchDetails2 item from local storage and assign it to UserName
        UserName: JSON.parse(localStorage.getItem('searchDetails2')).UserName
      }
      // end define receiverDetails after search
    }
    // end check if searchDetails2 is exist
    console.log("flat currency : " + this.tokenDetails.flat_currency)
  }
  // end ngOnInit function

  // start onChange for after user upload private key and read the content
  onChange(fileList: FileList): void {
    let file = fileList[0];
    let fileReader: FileReader = new FileReader();
    let self = this;
    fileReader.onloadend = function (x) {
      self.fileContent = String(fileReader.result);
      console.log("file content : " + self.fileContent);
    }
    fileReader.readAsText(file);
    this.fileContent = self.fileContent;
  }
  // end onChange for after user upload private key and read the content

  // start function for amount be float
  pad(num: number, size: number): string {
    let s = num + "";
    while (s.length < size) s = s + '0';
    return s;
  }
  // end function for amount be float

  // start check function for check if the user choose true or false
  check(flat) {
    console.log("flat currency : " + flat)
  }
  // end check function for check if the user choose true or false

  // start function of refund token            
  OnSubmit(form: NgForm) {

    // start check if the amount is null
    if (form.value.Amount == "" || form.value.Amount == null || (parseFloat(form.value.Amount) == 0)) {
      // show notification if the amount is null
      this.toastr.error('Amount is required , يرجى إدخال المبلغ', '',
        {
          timeOut: 2000
        });
    }
    // end check if the amount is null

    // start else
    else {

      // get senderDetails.PublicKey and assign it to tokenDetails.Sender
      this.tokenDetails.Sender = this.senderDetails.PublicKey;
      // get receiver public key and assign it to tokenDetails.receiver
      this.tokenDetails.receiver = form.value.receiver;

      // print tokenDetails.receiver
      console.log("receiver public key : " + this.tokenDetails.receiver)

      // get amount and assign it to amountInput 
      var amountInput = String(form.value.Amount);

      // define dot
      var dot = ".";

      // start check if the amountInput not has dot
      if (amountInput.indexOf(dot) === -1) {
        //print this.pad(0, 6) 
        console.log("pad function : " + this.pad(0, 6));
        // define floatNum and assign it to this.pad(0, 6)
        var floatNum = this.pad(0, 6);
        // make combination between amountInput , floatNum and assign it to amountInput
        amountInput = amountInput + "." + floatNum;
        // print amountInput
        console.log("amount input :" + amountInput);

      }
      // end check if the amountInput not has dot

      // start else
      else {
        // make split and get before dot and after dot and assign it to amount_before
        var amount_before = amountInput.split(".");

        // print amount_before
        console.log("amount before : " + amount_before);

        // define floatNum and assign it to this.pad(Number(amount_before[1]), 6)
        var floatNum = this.pad(Number(amount_before[1]), 6);

        // make combination between amount_before[0]  , floatNum and assign it to amountInput
        amountInput = amount_before[0] + "." + floatNum;
        console.log("amount input :" + amountInput);
      }
      // end else

      console.log("final amout : " + amountInput);


      // get amount and assign it to tokenDetails.Amount
      this.tokenDetails.Amount = Number.parseFloat(amountInput);

      // print amount
      console.log("final amount 2 : " + this.tokenDetails.Amount);

      // call strftime and assign it to tokenDetails.TokenTime 
      this.tokenDetails.timeZone = strftime('%FT%I:%M:%S%z').replace("0200", "0000");

      // make slice for make time 02:00 and assign it to splitTime
      var splitTime = this.tokenDetails.timeZone.slice(0, -2) + ":" + this.tokenDetails.timeZone.slice(-2);
      // print splitTime
      console.log("split time : " + splitTime);

      // print tokenDetails.TokenTime
      console.log("new time : " + this.tokenDetails.timeZone);

      // assign splitTime to tokenDetails.TokenTime
      this.tokenDetails.timeZone = splitTime;

      // start check if the flat currency is true
      if (form.value.flat_currency === true) {
        // make tokenDetails.receiver equal true
        this.tokenDetails.receiver = this.receiverDetails.PublicKey;
      }
      // end check if the flat currency is true

      // start check if the flat currency is false
      else {
        // assign tokenDetails.receiver to empty string
        this.tokenDetails.receiver = "";
        // make tokenDetails.flat_currency equal false
        this.tokenDetails.flat_currency = false;
      }
      // end check if the flat currency is false

      // define key for the encrypt private key
      var key = "digital wallet";

      // decrypt the private key file content with Aes and assign it to dec_private_key
      var dec_private_key = CryptoJS.AES.decrypt(this.fileContent.trim(), key.trim()).toString(CryptoJS.enc.Utf8);

      // var privKey = this.senderDetails.PrivateKey;
      console.log("private key : " + dec_private_key);

      // define amount and assign it to string_amount
      var string_amount = String(amountInput);

      // print string_amount
      console.log("string amount : " + string_amount);

      // start setParam2 for signature
      setParam2(
        // define sender public key
        this.tokenDetails.Sender,
        // define amount
        string_amount,
                //define time
        splitTime,
        // define token id
        this.tokenDetails.id,
        // define dec_private_key
        dec_private_key,
        
      );
      // end setParam2 for signature

      console.log("sender public key : " + this.tokenDetails.Sender);

      console.log("token id : " + this.tokenDetails.id);

      console.log("service amount : " + string_amount);

      console.log("private key : " + dec_private_key);

      console.log("time : " + splitTime);

      // call gethSig function and assign it to h_sig
      this.h_sig = gethSig();

      // define h_sig and assign it to tokenDetails.hash
      this.tokenDetails.hash = this.h_sig

      // print signature
      console.log("signature : " + this.h_sig);

      this.tokenDetails.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));




      // start call refundToken from tokens service
      this.tokenSvr.refundToken(this.tokenDetails).subscribe((data: any) => {
        // start show notification token refunded successfully
        this.toastr.success('token refunded successfully', '',
          {
            timeOut: 2000
          });
        // end show notification token refunded successfully

        // add refund token response to local storage
        localStorage.setItem("refund token response", JSON.stringify(data));

        // go to tokens page
        this.router.navigate(['./pages/tokens']);

        // remove searchDetails from local storage
        localStorage.removeItem("searchDetails");
      });
      // end call refundToken from tokens service
    }
    // end else
  }
  // end function of refund token            

  // start function of search            
  onSearch(form: NgForm) {

    // define form data
    console.log(form.value);

    // start check if the search text is null
    if (form.value.searchText === '' || form.value.searchText === null) {
      // show notification if the search text is null
      this.toastr.error('Receiver public key is required');
    }
    // end check if the search text is null

    // start else
    else {
      //get AccountPassword from userData item from local storage and assign it to searchDetails.password
      this.searchDetails.password = JSON.parse(localStorage.getItem('userData')).AccountPassword;
      //get name from userData item from local storage and assign it to tokenDetails.name
      this.searchDetails.name = JSON.parse(localStorage.getItem('userData')).AccountName;
      // get search text and assign it to searchDetails.searchText
      this.searchDetails.searchText = form.value.searchText;

      this.searchDetails.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));

      // start call search from account service
      this.accountSvr.search(this.searchDetails).subscribe((data: any) => {
        // add search response to local storage
        localStorage.setItem('searchDetails2', JSON.stringify(data));

        // start check if searchDetails2 not has ErrorMessage
        if (!JSON.parse(localStorage.getItem('searchDetails2')).ErrorMessage) {

          // remove class show from modal class
          $('.modal').removeClass('show');

          // hide modal overlay
          $('.modal-backdrop.show').hide();

          // set padding right for body
          $('body').css("padding-right", "0");

          //  show notification Search successfully
          this.toastr.success('Search is Successfully');

          // reload refund token page

          // this.tokenDetails.id = this.tokenDetails.id.replace("%2","");

          console.log("token id : " + this.tokenDetails.id);
          window.location.reload();

          // this.router.navigate(['./pages/refund-token/'+this.tokenDetails.id+'']);
        }
        // end call refundToken from tokens service

        // start else
        else {
          //  show notification Search by error message
          this.toastr.error(JSON.parse(localStorage.getItem('searchDetails2')).ErrorMessage)
        }
        // end else
      });
      // end call search from account service
    }
    // end else
  }
  // end function of refund token            
}

export class DigitalwalletTransaction {
  Sender: string;
  Receiver: string;
  TokenID: string;
  Amount: number;
  Time: string;
  Signature: string;
}
