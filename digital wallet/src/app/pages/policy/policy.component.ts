import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'ngx-policy',
  templateUrl: './policy.component.html',
  styleUrls: ['./policy.component.scss', '../pages.component.scss']
})
export class PolicyComponent implements OnInit {

  constructor(private location: Location) {
    localStorage.setItem("page location", JSON.stringify(this.location.path()));
   }

  ngOnInit() {
  }

}
