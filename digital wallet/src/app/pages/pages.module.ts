import { NgModule } from "@angular/core";

import { PagesComponent } from "./pages.component";
import { PagesRoutingModule } from "./pages-routing.module";
import { ThemeModule } from "../@theme/theme.module";
import { MiscellaneousModule } from "./miscellaneous/miscellaneous.module";
import { QRCodeModule } from "angularx-qrcode";
import { SendComponent } from "./send/send.component";
import { TransactionsComponent } from "./transactions/transactions.component";
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { InternationalPhoneModule } from 'ng4-intl-phone';
import { DeactivateComponent } from './deactivate/deactivate.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {NgxQRCodeModule} from 'ngx-qrcode2'
import { ToastrModule } from 'ngx-toastr';
import { PrivacyComponent } from './privacy/privacy.component';
import { TermsComponent } from './terms/terms.component';
import { PolicyComponent } from './policy/policy.component';
import { TokensComponent } from './tokens/tokens.component';
import { AddTokenComponent } from './add-token/add-token.component';
import { UploadService } from "../@core/mock/upload.service";
import { EditTokenComponent } from './edit-token/edit-token.component';
import { TokenDetailsComponent } from './token-details/token-details.component';
import { RefundTokenComponent } from './refund-token/refund-token.component';
import { BalanceComponent } from './balance/balance.component';
import { ServicesComponent } from './services/services.component';
import { AddServiceComponent } from './add-service/add-service.component';
import { RefundTransactionsComponent } from './refund-transactions/refund-transactions.component';
import { CreationTransactionsComponent } from './creation-transactions/creation-transactions.component';
import { UploadFileComponent } from './file-storage/upload-file/upload-file.component';
import { FileListComponent } from './file-storage/file-list/file-list.component';

const PAGES_COMPONENTS = [PagesComponent];

@NgModule({
  imports: [
    ToastrModule.forRoot(
      {
        timeOut: 2000, extendedTimeOut: 0,
        preventDuplicates: true,
        maxOpened: 1,
        progressBar: false,
        autoDismiss:true,
        closeButton:true,
        positionClass: 'toast-top-center',
      },
    ),
    PagesRoutingModule,
    ThemeModule,
    MiscellaneousModule,
    QRCodeModule,
    NgxPaginationModule,
    NgxQRCodeModule,
    InternationalPhoneModule,
    // AngularFireDatabaseModule,
    // AngularFireAuthModule,
    // AngularFireMessagingModule,
    // // AngularFireModule.initializeApp(environment.firebase),
  ],
  declarations: [
    ...PAGES_COMPONENTS,
    SendComponent,
    EditProfileComponent,
    TransactionsComponent,
    DeactivateComponent,
    PrivacyComponent,
    TermsComponent,
    PolicyComponent,
    TokensComponent,
    AddTokenComponent,
    EditTokenComponent,
    TokenDetailsComponent,
    RefundTokenComponent,
    BalanceComponent,
    ServicesComponent,
    AddServiceComponent,
    RefundTransactionsComponent,
    CreationTransactionsComponent,
    UploadFileComponent,
    FileListComponent
    ],
  providers: [UploadService]
})
export class PagesModule {}
