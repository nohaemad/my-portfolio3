import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'ngx-tokens',
  templateUrl: './tokens.component.html',
  styleUrls: ['./tokens.component.scss', '../pages.component.scss']
})
export class TokensComponent implements OnInit {

  // define tokens
  tokens: any = [];

  // define isAdmin
  isAdmin = true;

  // define token for password and public key
  token = {
    password: '',
    public_key: '',
    jwt_token : ''
  }

  // define isRefund
  isRefund = false;

  // define isToken
  isToken = false;

  // get AccountEmail from local storage and assign it to emailUser
  emailUser = JSON.parse(localStorage.getItem("userData")).AccountEmail;

  // start constructor
  constructor(private location: Location) {

    localStorage.setItem("page location", JSON.stringify(this.location.path()));

    // start check if tokensDetails exist in local storage
    if (JSON.parse(localStorage.getItem("tokensDetails"))) {
      // assign isToken equal true
      this.isToken = true;
      // get tokens from local storage and assign it to this.tokens
      this.tokens = JSON.parse(localStorage.getItem("tokensDetails"));
    }
    // end check if tokensDetails exist in local storage
    // start check else if tokensDetails exist in local storage
    else {
      // assign isToken equal false
      this.isToken = false;
    }
    // end check else if tokensDetails exist in local storage
  }
  // end constructor

  // start ngOnInit
  ngOnInit() {

    // start check if tokensDetails exist in local storage
    if (JSON.parse(localStorage.getItem('tokensDetails'))) {
      // assign isToken equal true
      this.isToken = true;
    }
    // end check if tokensDetails exist in local storage
    // start check else if tokensDetails exist in local storage
    else {
      // assign isToken equal false
      this.isToken = false;
    }
    // end check else if tokensDetails exist in local storage

    // start check if the user is from inovatian accounts
    if ((this.emailUser === "hatim@inovatian.com") ||
      (this.emailUser === "hatim.fees@inovatian.com") ||
      (this.emailUser === "hatim.refund@inovatian.com") ||
      (this.emailUser === "askme557@gmail.com")) {
      // assign isAdmin equal true
      this.isAdmin = true;
    }
    // end check if the user is from inovatian accounts
    // start check else if the user is from inovatian accounts       
    else {
      // assign isAdmin equal false
      this.isAdmin = false;
    }
    // end check else if the user is from inovatian accounts       
  }
  // end ngOnInit

  save(token) {
    localStorage.setItem("token details", JSON.stringify(token));
  }

}
