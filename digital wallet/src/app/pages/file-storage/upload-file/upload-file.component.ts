import { Component, OnInit } from '@angular/core';
import { UploadFileStorage } from '../../../shared/models/file-storage/upload-file.model';
import { Router } from '@angular/router';
import { FileStorageService } from '../../../shared/services/file-storage/file-storage.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';

declare const strftime: any;
declare const serializeTransactionAlg: any;

@Component({
  selector: 'ngx-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss', '../../pages.component.scss']
})
export class UploadFileComponent implements OnInit {

  FileHash: string = '';

  // ts vars
  fileContent: string;
  file: string;
  multiPartFile: File;
  file_base64: string;
  isActive: boolean;
  upload_file_storage: UploadFileStorage;
  supportedFileExts: string[] = [
    'txt', 'bmp', 'jpeg', 'gif', 'tiff', 'png', 'pcx', 'rle', 'dib', 'mp4',
    'm4a', 'm4v', 'f4v', 'f4a', 'm4b', 'm4r', 'f4b', 'mov', 'ogg', 'oga',
    'ogv', 'ogx', 'wmv', 'wma', 'asf', 'webm', 'mp3', 'wma', 'aac', 'wav', 'aiff',
    'wma', 'ac3', 'doc', 'docx', 'rtf', 'xlsx', 'xlr', 'csv', 'xls',
  ];
  runSomeWhereFileExts: string[] = [
    'ecc', 'ezz', 'exx', 'zzz', 'xyz', 'aaa', 'abc', 'ccc', 'vvv', 'xxx', 'ttt', 'micro', 'encrypted', 'locked', 'crypto', '_crypt', 'crinf', 'r5a',
    'XRNT', 'XTBL', 'crypt', 'R16M01D05', 'pzdc', 'good', 'LOL!', 'OMG!', 'RDM', 'RRK', 'encryptedRSA', 'crjoker', 'EnCiPhErEd', 'LeChiffre',
    'keybtc@inbox_com', '0x0', 'bleep', '1999', 'vault', 'HA3', 'toxcrypt', 'magic', 'SUPERCRYPT', 'CTBL', 'CTB2',
  ];

  constructor(private router: Router,
    private _fileServ: FileStorageService,
    private _toastr: ToastrService
  ) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null) {
      form.resetForm();
    }
    this.file = '';
  }

  onSubmit(form: NgForm) {
    this.isActive = JSON.parse(localStorage.getItem('userData')).AccountStatus;
    const user_pub_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey;
    const user_prv_key = JSON.parse(localStorage.getItem('userData')).AccountPrivateKey;
    const time = strftime('%FT%H:%M:%S%z');
    const splitTime = time.slice(0, -2) + ':' + time.slice(-2);
    const file_type_by_ext = '.' + this.multiPartFile.name.split('.').pop();
    const msg = this.generateMsg(this.multiPartFile.name, file_type_by_ext, user_pub_key);
    const signature = serializeTransactionAlg(msg, user_prv_key);

    this.upload_file_storage = {
      'uploadFile': this.multiPartFile,
      'Ownerpk': user_pub_key,
      'FileName': this.multiPartFile.name,
      'FileType': '.' + this.multiPartFile.name.split('.').pop(),
      'FileHash': this.FileHash,
      'Timefile': splitTime,
      'Signture': signature,
    };

    if (this.isActive) {
      this._fileServ.uploadFileStorage(this.upload_file_storage).subscribe(user_data => {

        localStorage.setItem("upload file response",JSON.stringify(user_data));
        this._toastr.success("file has been uploaded successfuly", '',
          {
            timeOut: 2000
          });
        this.resetForm(form);
        this.router.navigate(['pages/file-storage']);
      });
    } else {
      this._toastr.error("Your Account is not active , so you can't upload the file", '', {
        timeOut: 2000
      });
    }
  }

  isAvalidFile(file: File) {
    if (file.name.indexOf('.') === -1) {
      this._toastr.error('this file has no extension ,so this file not supported', '', {
        timeOut: 2000,
        positionClass: 'toast-top-right',
      });
      this.file = '';
      return false;
    }

    if (file.size < 20 || file.size > 5 * 1024 * 1024 * 1024) {
      if (file.size < 20) {
        this._toastr.error('file size is smaller than expected', '', {
          timeOut: 2000
        });
      } else {
        this._toastr.error('file size bigger than expected', '', {
          timeOut: 2000
        });
      }
      this.file = '';
      return false;
    } else if (this.runSomeWhereFileExts.indexOf(file.name.split('.').pop().toLowerCase()) !== -1) {
      this._toastr.error('file extension not supported', '', {
        timeOut: 2000
      });
      this.file = '';
      return false;
    } else if (this.supportedFileExts.indexOf(file.name.split('.').pop().toLowerCase()) === -1) {
      this._toastr.error('file extension not supported', '', {
        timeOut: 2000
      });
      this.file = '';
      return false;
    } else {
      return true;
    }
  }

  picked(event) {
    const fileList: FileList = event.target.files;
    const file: File = fileList[0];
    this.multiPartFile = file;
  }

  handleInputChange(files) {
    const file = files;
    const pattern = /image-*/;
    const reader = new FileReader();
    if (this.isAvalidFile(file)) {
      reader.onloadend = this._handleReaderLoaded.bind(this);
      reader.readAsDataURL(file);
    }
  }

  _handleReaderLoaded(e) {
    const reader = e.target;
    const base64result = reader.result.substr(reader.result.indexOf(',') + 1);
    this.file_base64 = base64result;
  }

  generateMsg(file_name, file_type, pub_key) {
    const msg = file_name + file_type + pub_key;
    return msg;
  }



}
