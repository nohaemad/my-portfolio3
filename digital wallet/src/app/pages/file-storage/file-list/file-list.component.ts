import { Component, OnInit } from '@angular/core';
import { EploreFilesRequest } from '../../../shared/models/file-storage/explore_file.model';
import { DeleteFileRequest } from '../../../shared/models/file-storage/delete_file.model';
import { DownloadFileRequest } from '../../../shared/models/file-storage/download_file.model';
import { FileDetails } from '../../../shared/models/file-storage/file_details.model';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FileStorageService } from '../../../shared/services/file-storage/file-storage.service';
declare const strftime: any;
declare const serializeTransactionAlg: any;

@Component({
  selector: 'ngx-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.scss', '../../pages.component.scss']
})
export class FileListComponent implements OnInit {

   // ts vars
   explore_file_request: EploreFilesRequest;
   delete_file_request: DeleteFileRequest;
   download_file_request: DownloadFileRequest;
   res: any;
   filesList: FileDetails[] = [];
  
  constructor( private _toastr: ToastrService,
    private _router: Router,
    private _fileServ: FileStorageService) { }

  ngOnInit() {
    this.getFilesList();
  }

  getFilesList() {
    const pub_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey;
    const password = JSON.parse(localStorage.getItem('userData')).AccountPassword;
    this.explore_file_request = {
      PublicKey: pub_key,
      Password: password,
    };
    this._fileServ.exploreFiles(this.explore_file_request).subscribe(files_list => {

      localStorage.setItem("files",JSON.stringify(files_list));
      this.filesList = JSON.parse(localStorage.getItem("files")).Files;
    });
  }

  downloadFile(file: FileDetails) {

    const password = JSON.parse(localStorage.getItem('userData')).AccountPassword;
    const pub_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey;
    const prv_key = JSON.parse(localStorage.getItem('userData')).AccountPrivateKey;
    const time = strftime('%FT%H:%M:%S%z');
    const splitTime = time.slice(0, -2) + ':' + time.slice(-2);
    const msg = this.generateMsg(pub_key, password,file.Fileid);
    const signature = serializeTransactionAlg(msg, prv_key);

    this.download_file_request = {
      'Password': password,
      'PublicKey': pub_key,
      'FileID': file.Fileid,
      'Time': splitTime,
      'Signture': signature,
    };
    this._fileServ.downloadFile(this.download_file_request).subscribe(res => {
      console.log(res);
      this.getFileFromUrl(JSON.parse(JSON.stringify(res)).MessageAPI, file.FileName);

      this._toastr.success('File has been downloaded successfully', '',
        {
          timeOut: 2000
        });
    });
  }

  deleteFile(file) {

    const password = JSON.parse(localStorage.getItem('userData')).AccountPassword;
    const pub_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey;
    const prv_key = JSON.parse(localStorage.getItem('userData')).AccountPrivateKey;
    const time = strftime('%FT%H:%M:%S%z');
    const splitTime = time.slice(0, -2) + ':' + time.slice(-2);
    const msg = this.generateMsg(pub_key, password,file.Fileid);
    const signature = serializeTransactionAlg(msg, prv_key);

    this.delete_file_request = {
      'Password': password,
      'PublicKey': pub_key,
      'FileID': file.Fileid,
      'Time': splitTime,
      'Signture': signature,
    };
    this._fileServ.deleteFile(this.delete_file_request).subscribe(res => {
      this._toastr.success('File deleted successfully', '',
        {
          timeOut: 2000
        });
      this._router.navigate(['pages/file-storage']);
    });
  }

  generateMsg(pub_key, password,file_id) {
    const msg =  pub_key + password+file_id;
    return msg;
  }

  getFileFromUrl(file_url: string, file_name: string) {
    fetch(file_url)
      .then(resp => resp.blob())
      .then(blob => {
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.style.display = 'none';
        a.href = url;
        // the filename you want
        a.download = file_name;
        document.body.appendChild(a);
        a.click();
        window.URL.revokeObjectURL(url);
      })
      .catch((err) => console.log(err));
  }

}
