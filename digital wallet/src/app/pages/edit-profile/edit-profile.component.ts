import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
// import settings service
import { SettingsService } from '../../shared/services/settings/settings.service';
// import Toastr Service
import { ToastrService } from 'ngx-toastr';
// import jquery
import * as $ from 'jquery';
// import sha256
import * as sha256 from 'sha256';

@Component({
  selector: 'ngx-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss', '../pages.component.scss']
})
export class EditProfileComponent implements OnInit {

  // define space
  space = ' ';

  // define plus
  plus = "+";

  // define user data
  userData;

  // define userDetails input username , email , phone , address , password , old password and public key
  userInfo = { name: '', emailAdd: '', phone: '', address: '', password: '', oldPassword: '', public_key: '',jwt_token : '' };

  // define hash password
  hash_password: string;

  // define special_char_amount for username
  special_char_amount = 0;

  // define special_char_amount2 for email
  special_char_amount2 = 0;

  // define array of special characters for username
  special_charchters = ['"', "!", "$", "%", "&", "`", "'", "(", ")", "*", "+", ",", "-", "/", ":", ";",
    "<", "=", ">", "?", "@", "[", "\\", "]", "^", "`", "{", "|", "}", "#", "~",
    '"', "_"];

  // define array of special characters for email
  special_charchters2 = ['"', "!", "$", "%", "&", "`", "'", "(", ")", "*", "+", ",", "/", ":", ";",
    "<", "=", ">", "?", "[", "\\", "]", "^", "`", "{", "|", "}", "#", "~",
    '"'];

  // start constructor for settings service , router , toastr and router
  constructor(
    // define settings service
    private settingsSvr: SettingsService,
    //define router
    private router: Router,
    // define toastr
    private toastr: ToastrService,
    private location: Location) {
      localStorage.setItem("page location", JSON.stringify(this.location.path()));
     }

  // start ngOnInit function
  ngOnInit() {

    // start define userData for data of user
    this.userData = {
      // define username
      name: JSON.parse(localStorage.getItem('userData')).AccountName,
      // define email
      emailAdd: JSON.parse(localStorage.getItem('userData')).AccountEmail,
      //define phone
      phone: JSON.parse(localStorage.getItem('userData')).AccountPhoneNumber,
      // define address
      address: JSON.parse(localStorage.getItem('userData')).AccountAddress,
      // define password
      password: '',
      // define old password
      oldPassword: ''
    }
    // end define userData for data of user

    // start when click on eye icon of password jquery
    $(".toggle-password").click(function () {
      $(this).toggleClass("eva-eye eva-eye-off");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });
    // end when click on eye icon of password jquery
  }
  // end ngOnInit function

  // start increment_special_amount4 function
  increment_special_amount4() {
    // increase special_char_amount by 1
    this.special_char_amount = this.special_char_amount + 1;
  }
  // end increment_special_amount4 function

  // start get_special_amount4 function
  get_special_amount4() {
    // get special_char_amount
    return this.special_char_amount;
  }
  // end get_special_amount4 function

  // start set_special_amonnt4 function
  set_special_amonnt4() {
    // set special_char_amount by 0
    this.special_char_amount = 0;
  }
  // end set_special_amonnt4 function

  // start increment_special_amount5 function
  increment_special_amount5() {
    // increase special_char_amount2 by 1
    this.special_char_amount2 = this.special_char_amount2 + 1;
  }
  // end increment_special_amount5 function

  // start get_special_amount5 function
  get_special_amount5() {
    // get special_char_amount2
    return this.special_char_amount2;
  }
  // end get_special_amount5 function

  // start set_special_amonnt5 function
  set_special_amonnt5() {
    // set special_char_amount2 by 0
    this.special_char_amount2 = 0;
  }
  // end set_special_amonnt5 function

  // start function of edit profile

  onSubmit(form: NgForm) {
    // start check if user choose yes
    if (window.confirm('Are you sure, you want to update?')) {

      // start check if username or email or phone or address not equal null
      if ((form.value.name === '' || form.value.name === null) ||
        (form.value.emailAdd === '' || form.value.emailAdd === null) ||
        (form.value.phone === '' || form.value.phone === null) ||
        (form.value.address === '' || form.value.address === null)) {

        // start check if the username is null
        if (form.value.name === '' || form.value.name === null) {
          // show notification if the username is null
          this.toastr.error('Name is required', '',
            {
              timeOut: 2000
            });
        }
        // end check if the username is null

        // start check if the email is null
        if (form.value.emailAdd === '' || form.value.emailAdd === null) {
          // show notification if the email is null
          this.toastr.error('Email is required', '',
            {
              timeOut: 2000
            });
        }
        // end check if the email is null

        // start check if the phone is null
        if (form.value.phone === '' || form.value.phone === null) {
          // show notification if the phone is null
          this.toastr.error('phone is required', '',
            {
              timeOut: 2000
            });
        }
        // end check if the phone is null

        // start check if the address is null
        if (form.value.address === '' || form.value.address === null) {
          // show notification if the address is null
          this.toastr.error('address is required', '',
            {
              timeOut: 2000
            });
        }
        // end check if the address is null

      }
      // end check if username or email or phone or address not equal null

      // start else
      else {
        // start check if the password is null
        if (form.value.password === null || form.value.password === '') {
          // get username of user and assign it to userInfo.name
          this.userInfo.name = form.value.name;
          // get email of user and assign it to userInfo.emailAdd
          this.userInfo.emailAdd = form.value.emailAdd;
          // get phone number of user and assign it to userInfo.phone
          this.userInfo.phone = form.value.phone;
          // get address of user and assign it to userInfo.address
          this.userInfo.address = form.value.address;
          //get AccountPassword from userData item from local storage and assign it to userInfo.password
          this.userInfo.password = JSON.parse(localStorage.getItem('userData')).AccountPassword;
          //get AccountName from userData item from local storage and assign it to userInfo.name
          this.userInfo.oldPassword = JSON.parse(localStorage.getItem('userData')).AccountPassword;
          //get AccountPublicKey from userData item from local storage and assign it to userInfo.public_key
          this.userInfo.public_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey;
          this.userInfo.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));
        }
        // end check if the password is null

        // start else
        else {

          // hashing password of user using sha256 and assign it to hash_password
          this.hash_password = sha256(form.value.password);
          // get username of user and assign it to userInfo.name
          this.userInfo.name = form.value.name;
          // get email of user and assign it to userInfo.emailAdd
          this.userInfo.emailAdd = form.value.emailAdd;
          // get phone number of user and assign it to userInfo.phone
          this.userInfo.phone = form.value.phone;
          // get address of user and assign it to userInfo.address
          this.userInfo.address = form.value.address;
          // get password of user and assign it to userInfo.address
          this.userInfo.password = this.hash_password;
          //get AccountPassword from userData item from local storage and assign it to userInfo.password
          this.userInfo.oldPassword = JSON.parse(localStorage.getItem('userData')).oldPassword;
          //get AccountPublicKey from userData item from local storage and assign it to userInfo.public_key
          this.userInfo.public_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey;
          this.userInfo.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));
        }
        // end else
        console.log('8&*&     ' + this.userInfo.password);

                  // start call editUserInfo from settings service
        this.settingsSvr.editUserInfo(this.userInfo).subscribe((data: any) => {
         
                   // add edit profile response to local storage
          localStorage.setItem("userData", JSON.stringify(data));

                   // start show notification edit is successful      
            this.toastr.success('edit is successful', '',
              {
                timeOut: 2000
              });

              // go to edit profile
            this.router.navigate(['/pages/edit-profile']);
         
        })
                          // end call editUserInfo from settings service
      }
      // end else
    }
    // end check if user choose yes
  }
  // end function of edit profile

  cancel() {
    if (window.confirm('Are you sure, you want to cancel your update?')) {
      this.toastr.error('you cancel edits');
      this.router.navigate(['/pages/transactions']);

    }
  }
}
