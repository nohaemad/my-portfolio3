import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
// import tokens service
import { TokensService } from '../../shared/services/tokens/tokens.service';
// import Toastr Service
import { ToastrService } from 'ngx-toastr';
// import DomSanitizer
import { DomSanitizer } from '@angular/platform-browser';
// import jquery
import * as $ from 'jquery';


@Component({
  selector: 'ngx-edit-token',
  templateUrl: './edit-token.component.html',
  styleUrls: ['./edit-token.component.scss', '../pages.component.scss']
})
export class EditTokenComponent implements OnInit {

  // define imagePath for decrypt the image 
  imagePath: any;

  // define token_image
  token_image: boolean = false;

  // define default_image
  default_image = '';

  // define tokenDetails input
  tokenDetails = {
    id: '',
    name: '',
    symbol: '',
    icon: '',
    TokensTotalSupply: 0.000000,
    TokenValue: 0.000000,
    Reissuability: true,
    Precision: 0,
    UsageType: '',
    TokenType: '',
    ValueDynamic: true,
    ContractID: '',
    Dynamicprice: 0.000000,
    TokenTime: '',
    Description: '',
    UserPublicKey: [],
    password: '',
    public_key: '',
    jwt_token : ''
  };

  // define list_pks2 map for user public key
  list_pks2: Map<string, string> = new Map<string, string>();

  // define sellersPermitFile for uploaded file
  sellersPermitFile: any;

  // define sellersPermitString for encrypt uploaded file
  sellersPermitString: string;

  // define reissuability options
  ReissuabilityOptions = [
    { id: 0, name: true },
    { id: 1, name: false }
  ];

  // define value dynamic options
  valueDynamicOptions = [
    { id: 0, name: true },
    { id: 1, name: false }
  ];

  // define usage type options
  usageTypeOptions = [
    { id: 0, name: "security" },
    { id: 1, name: "utility" }
  ];

  // define token type options
  tokenTypeOptions = [
    { id: 0, name: "public" },
    { id: 1, name: "private" }
  ];

  //get token details from userData item from local storage and assign it to tokenList
  tokenList = JSON.parse(localStorage.getItem('token details'));

  // start constructor for tokens service , toastr , router and DomSanitizer
  constructor(
    // define tokens service
    private tokenSvr: TokensService,
    // define toastr service
    private toastr: ToastrService,
    // define router
    private router: Router,
    // define DomSanitizer
    private _sanitizer: DomSanitizer,
    private location: Location) {
      localStorage.setItem("page location", JSON.stringify(this.location.path()));

    // start check if the user not choose the image
    if (this.tokenList.IconURL === '') {
      this.token_image = true;
    }
    // end check if the user not choose the image
    // start else
    else {
      this.token_image = false;
      this.default_image = "../../assets/images/download.png"
    }
    // end else

    // define token id and assign it to tokenDetails.id
    this.tokenDetails.id = this.tokenList.TokenID;
    // define token name and assign it to tokenDetails.name
    this.tokenDetails.name = this.tokenList.TokenName;
    // define token sybmbol and assign it to tokenDetails.symbol
    this.tokenDetails.symbol = this.tokenList.TokenSymbol;
    // define token icon and assign it to tokenDetails.icon
    this.tokenDetails.icon = this.tokenList.TokenIcon;
    // define token total supply and assign it to tokenDetails.TokensTotalSupply
    this.tokenDetails.TokensTotalSupply = this.tokenList.TokensTotalSupply;
    // define token value and assign it to tokenDetails.TokenValue
    this.tokenDetails.TokenValue = this.tokenList.TokenValue;
    // define token reissuability and assign it to tokenDetails.Reissuability
    this.tokenDetails.Reissuability = this.tokenList.Reissuability;
    // define token precision and assign it to tokenDetails.Precision
    this.tokenDetails.Precision = this.tokenList.Precision;
    // define token usage type and assign it to tokenDetails.UsageType
    this.tokenDetails.UsageType = this.tokenList.UsageType;
    // define token type and assign it to tokenDetails.TokenType
    this.tokenDetails.TokenType = this.tokenList.TokenType;
    // define token value dynamic and assign it to tokenDetails.ValueDynamic
    this.tokenDetails.ValueDynamic = this.tokenList.ValueDynamic;
    // define token contract id and assign it to tokenDetails.ContractID
    this.tokenDetails.ContractID = this.tokenList.ContractID;
    // define token dynamic price and assign it to tokenDetails.Dynamicprice
    this.tokenDetails.Dynamicprice = this.tokenList.Dynamicprice;
    // define token description and assign it to tokenDetails.Description
    this.tokenDetails.Description = this.tokenList.Description;
    // define token user public key and assign it to tokenDetails.UserPublicKey
    this.tokenDetails.UserPublicKey = this.tokenList.UserPublicKey;

    // decrypt the icon url to image and assign it to imagePath
    this.imagePath = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,'
      + this.tokenList.TokenIcon);
  }

  // start ngOnInit function
  ngOnInit() {

    // start when the function is ready
    $(document).ready(function () {
      // start the function when click on update service
      $(document.body).on('click', '.btn-add-service-update', function () {
        const index = $('.services-list').length + 1;
        $('.services-list_update').append(
          '<div class="service-input">' +
          '<div class="row">' +
          '<div class="col-md-3" style="flex: 0 0 21%;">' +
          '<label style="line-height:40px;">Service Code</label>' +
          '</div>' +
          '<div class="col-md-6">' +
          '<div class="form-group">' +
          '<input type="text" class="form-control" style="border-radius: 20px;" name="ser_codes_update[]" >' +
          '</div>' +
          '</div>' +
          '<div class="col-md-3">' +
          '<i class="fa fa-minus-circle btn-sm btn-remove-service" style="border-radius: 20px !important;color: #ff8633;"></i>' +
          '</div>' +
          '</div>' +
          '</div>',
        );
      });
      // end the function when click on update service
    });
    // end when the function is ready

    // start when the click button has class save add the inputs value to titles2
    $(document.body).on('click', '.save-update', () => {
      // start add value inputs of user public and assign it to titles2
      const titles = $('input[name^=ser_codes_update]').map(function (idx, elem) {
        return $(elem).val();
      }).get();
      // end add value inputs of user public and assign it to titles2

      console.log('titles ' + titles);
      this.list_pks2.clear();
      this.list_pks2 = titles;
    });
    // end when the click button has class save add the inputs value to titles2

    // start function when user click on remove button delete ip input
    $(document.body).on('click', '.btn-remove-service', function () {
      $(this).closest('.ip-input').remove();
    });
    // end function when user click on remove button delete ip input
  }
  // end ngOnInit function

  // start function of edit token            
  OnSubmit(editTokenForm: NgForm) {

    // make tokenDetails.UserPublicKey null
    this.tokenDetails.UserPublicKey = [];

    // start function for get all items of list_pks2 and push it tokenDetails.UserPublicKey
    this.list_pks2.forEach((key, value) => {
      if (this.list_pks2[value].length !== 0) {
        this.tokenDetails.UserPublicKey.push(this.list_pks2[value]);
      }
    });
    // end function for get all items of list_pks2 and push it tokenDetails.UserPublicKey

    //get AccountPassword from userData item from local storage and assign it to tokenDetails.password
    this.tokenDetails.password = JSON.parse(localStorage.getItem('userData')).AccountPassword;

    //get AccountPublicKey from userData item from local storage and assign it to tokenDetails.public_key
    this.tokenDetails.public_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey;

    this.tokenDetails.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));

    // start call editToken from tokens service
    this.tokenSvr.editToken(this.tokenDetails).subscribe((data: any) => {

      // start show notification Token edited successfully
      this.toastr.success('Token edited successfully', '',
        {
          timeOut: 2000
        });

      // edit token response to local storage
      localStorage.setItem("edit token response", JSON.stringify(data));

      // go to tokens page
      this.router.navigate(['./pages/tokens']);
    });
    // end call editToken from tokens service
  }
  // end function of edit token            

  // start function convertMapToArray
  convertMapToArray(data: Map<string, string>) {
    var alldata: any[];
    data.forEach((key, value) => {
      console.log('key ' + key + 'value' + value);
      alldata.push(value);
    })
    return alldata;
  }
  // start function convertMapToArray

  // start function removePK
  removePK() {
    $(document.body).on('click', '.btn-remove-service', function () {
      $(this).closest('.ip-input').remove();
    });
  }
  // end function removePK

  // start function addPK
  addPK() {
    $('.ip-list').html('');
    $('.ip-list').append(
      '<div class="ip-input">' +
      '<div class="row">' +
      '<div class="col-md-2">' +
      '<label>User Public Key</label>' +
      '</div>' +
      '<div class="col-md-8">' +

      '<div class="ip">' +
      '<input type="text" placeholder="Enter user public key" name="ser_codes_update[]" placeholder="" class="form-control" style="border-radius: 20px !important;margin-bottom: 20px;">' +

      '</div>' +
      '</div>' +

      '<div class="col-md-2">' +
      '<div class="btn-remove-service" (click)="removePK()"  style="height: 45px;background: transparent;border: 1px solid #ddd;width: 65%;text-align: center;line-height: 45px;color: #000;border-radius: 10px;cursor: pointer;font-size: 18px;">' +
      '<a class=" ">x</a>' +
      '</div>' +
      '</div>' +
      '</div>' +

      '</div>'
    );
  }
  // end function addPK

  // start function for read the token icon                 
  picked(event) {
    let fileList: FileList = event.target.files;
    const file: File = fileList[0];
    this.sellersPermitFile = file;
    console.log("icon fie : " + this.sellersPermitFile)
    this.handleInputChange(file); //turn into base64
  }
  // end function for read the token icon

  // start function for check the icon url pattern
  handleInputChange(files) {
    var file = files;
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onloadend = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  // end function for check the icon url pattern

  // start function for encrypt the token icon  
  _handleReaderLoaded(e) {
    let reader = e.target;
    var base64result = reader.result.substr(reader.result.indexOf(',') + 1);
    this.sellersPermitString = base64result;
    console.log("base 64 icon : " + this.sellersPermitString);
  }
  // end function for encrypt the token icon  
}
