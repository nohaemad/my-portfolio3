import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
// import account service
import { AccountService } from '../../shared/services/account/account.service';

@Component({
  selector: 'ngx-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.scss', '../pages.component.scss']
})
export class BalanceComponent implements OnInit {

  // define balance details
  balanceDetails: any = [];

  // define balance for password and public key
  balance = {
    // define password
    password: '',
    // define public key
    public_key: '',
    jwt_token: ''
  }

  // define token id
  tokenId = '';

  // define user details
  userDetails = {
    PublicKey: JSON.parse(localStorage.getItem('userData')).AccountPublicKey,
    Password: JSON.parse(localStorage.getItem('userData')).AccountPassword
  }

  // start constructor for account service
  constructor(
    private accountSvr: AccountService,
    private location: Location) { 
      localStorage.setItem("page location", JSON.stringify(this.location.path()));
    }
  // end constructor for account service

  // start ngOnInit function
  ngOnInit() {

    // get AccountPassword from userData from local storage and assign it to balance.password 
    this.balance.password = JSON.parse(localStorage.getItem('userData')).AccountPassword;

    // get AccountPublicKey from userData from local storage and assign it to balance.public_key 
    this.balance.public_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey;

    this.balance.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));

    // start call getBalance from account service
    this.accountSvr.getBalance(this.balance).subscribe((data: any) => {

      // add get balance response to local storage
      localStorage.setItem('balanceDetails', JSON.stringify(data));

      // print get balance response
      console.log("balance details : " + JSON.stringify(data));

      // print token name
      console.log("token name : " + JSON.parse(localStorage.getItem('balanceDetails'))[0].Tokenname);

      // get balanceDetails from local storage and assign it to balanceDetails 
      this.balanceDetails = JSON.parse(localStorage.getItem('balanceDetails'));
    })
    // end call getBalance from account service
  }
  // end ngOnInit function

}
