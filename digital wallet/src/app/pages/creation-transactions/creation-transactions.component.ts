import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
// import account service
import { AccountService } from '../../shared/services/account/account.service';

@Component({
  selector: 'ngx-creation-transactions',
  templateUrl: './creation-transactions.component.html',
  styleUrls: ['./creation-transactions.component.scss', '../pages.component.scss']
})
export class CreationTransactionsComponent implements OnInit {

  // define transaction for password and public key
  transaction = {
    // define password
    password: '',
    // define public key
    public_key: '',
    jwt_token : ''
  }

  // define user details
  userDetails = {
    PublicKey: JSON.parse(localStorage.getItem('userData')).AccountPublicKey,
    Password: JSON.parse(localStorage.getItem('userData')).AccountPassword
  }

  // define transactions
  transactions: any = [];

  // start constructor for account service
  constructor(
    private accountSvr: AccountService,
    private location: Location) { 
    localStorage.setItem("page location", JSON.stringify(this.location.path()));
  }
  // end constructor for account service

  // start ngOnInit function
  ngOnInit() {
    // start check if the public key
    if (this.userDetails.PublicKey != null) {

      // get AccountPassword from userData from local storage and assign it to transaction.password 
      this.transaction.password = JSON.parse(localStorage.getItem('userData')).AccountPassword;

      // get AccountPublicKey from userData from local storage and assign it to transaction.public_key 
      this.transaction.public_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey;

      this.transaction.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));

      // start call getTransactions from account service
      this.accountSvr.getTransactions(this.transaction).subscribe((transactions: any) => {
        // add get transactions response to local storage
        localStorage.setItem('transactionDetails3', JSON.stringify(transactions));

        // get transactionDetails3 from local storage and assign it to transactions 
        this.transactions = JSON.parse(localStorage.getItem("transactionDetails3")).tokenCreation;
      })
      // end call getTransactions from account service
    }
    // end check if the public key
  }
  // end ngOnInit function
}
