import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationTransactionsComponent } from './creation-transactions.component';

describe('CreationTransactionsComponent', () => {
  let component: CreationTransactionsComponent;
  let fixture: ComponentFixture<CreationTransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationTransactionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
