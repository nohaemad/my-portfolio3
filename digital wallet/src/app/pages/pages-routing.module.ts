import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import { PagesComponent } from "./pages.component";
import { NotFoundComponent } from "./miscellaneous/not-found/not-found.component";

import { SendComponent } from "./send/send.component";
import { TransactionsComponent } from "./transactions/transactions.component";
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { DeactivateComponent } from './deactivate/deactivate.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TermsComponent } from './terms/terms.component';
import { PolicyComponent } from './policy/policy.component';
import { TokensComponent } from './tokens/tokens.component';
import { AddTokenComponent } from './add-token/add-token.component';
import { EditTokenComponent } from './edit-token/edit-token.component';
import { TokenDetailsComponent } from './token-details/token-details.component';
import { RefundTokenComponent } from './refund-token/refund-token.component';
import { BalanceComponent } from './balance/balance.component';
import { ServicesComponent } from './services/services.component';
import { AddServiceComponent } from './add-service/add-service.component';
import { RefundTransactionsComponent } from './refund-transactions/refund-transactions.component';
import { CreationTransactionsComponent } from './creation-transactions/creation-transactions.component';
import { UploadFileComponent } from './file-storage/upload-file/upload-file.component';
import { FileListComponent } from './file-storage/file-list/file-list.component';

const routes: Routes = [
  {
    path: "",
    component: PagesComponent,
    children: [
      { path: "send", component: SendComponent, pathMatch: "full" },

      {
        path: "transactions",
        component: TransactionsComponent,
        pathMatch: "full"
      },

      {
        path: "refund-transaction",
        component: RefundTransactionsComponent,
        pathMatch: "full"
      },

      {
        path: "token-creation-transaction",
        component: CreationTransactionsComponent,
        pathMatch: "full"
      },
      {
        path: "edit-profile",
        component: EditProfileComponent,
        pathMatch: "full"
      },
      {
        path: "deactivate-account",
        component: DeactivateComponent,
        pathMatch: "full"
      },

      {
        path: "balance",
        component: BalanceComponent,
        pathMatch: "full"
      },

      {
        path: "tokens",
        component: TokensComponent,
        pathMatch: "full"
      },

      {
        path: "add-token",
        component: AddTokenComponent,
        pathMatch: "full"
      },

      {
        path: "edit-token",
        component: EditTokenComponent,
        pathMatch: "full"
      },

      {
        path: "token-details",
        component: TokenDetailsComponent,
        pathMatch: "full"
      },

      {
        path: "refund-token",
        component: RefundTokenComponent,
        pathMatch: "full"
      },
     
      {
        path: "services",
        component: ServicesComponent,
        pathMatch: "full"
      },

      {
        path: "add-service",
        component: AddServiceComponent,
        pathMatch: "full"
      },

      {
        path: "privacy",
        component: PrivacyComponent,
        pathMatch: "full"
      },

      {
        path: "terms",
        component: TermsComponent,
        pathMatch: "full"
      },

      {
        path: "policy",
        component: PolicyComponent,
        pathMatch: "full"
      },

      {
        path: "upload-file",
        component: UploadFileComponent,
        pathMatch: "full"
      },
      {
        path: "file-storage",
        component: FileListComponent,
        pathMatch: "full"
      },

      {
        path: "miscellaneous",
        loadChildren: "./miscellaneous/miscellaneous.module#MiscellaneousModule"
      },
     
      {
        path: "**",
        component: NotFoundComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {}
