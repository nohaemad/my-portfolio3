import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';

// import Toastr Service
import { ToastrService } from 'ngx-toastr';
// import account Service
import { AccountService } from '../../shared/services/account/account.service';
// import services Service
import { ServicesService } from '../../shared/services/services/services.service';
// import jquery
import * as $ from 'jquery';
// import CryptoJS
import * as CryptoJS from 'crypto-js';
import * as sha256 from 'sha256';
import * as crypto_string from 'crypto';
// import * as crypto_string from 'crypto';


// import strftime function for assign the parameters
declare const setParam: any;
// import gethSig function for signature
declare const gethSig: any;
// import strftime function for time
declare const strftime: any;

declare const encrypt_transaction2: any;

@Component({
  selector: "ngx-send",
  templateUrl: "./send.component.html",
  styleUrls: ["./send.component.scss", '../pages.component.scss']
})


export class SendComponent implements OnInit {



  // define receiverDetails for data of user
  userData: any;

  // define public key of user
  publicKey: string;

  // define sendDetails input for send transaction
  @Input() sendDetails = {
    reciever: '',
    Sender: '',
    Amount: 0.000000,
    Hash: '',
    priv_file: '',
    timeZone: '',
    token: '',
    encrypt_data: '',
    jwt_token: ''
  };

  // define sendDetails input for send transaction
  encryptDetails = {
    Receiver: '',
    Sender: '',
    Amount: 0.000000,
    Signature: '',
    Time: '',
    TokenID: '',
  };

  // define puchase_details input for purchase service
  @Input() puchase_details = {
    reciever: '',
    Sender: '',
    Amount: '',
    Hash: '',
    priv_file: '',
    timeZone: '',
    token: '',
    service_id: '',
    password: '',
    public_key: '',
    jwt_token: ''
  };

  // define searchDetails input for search in send transaction
  @Input() searchDetails = {
    searchText: '',
    name: '',
    password: '',
    jwt_token: ''
  };

  // define search2Details input for search in purchase service
  @Input() search2Details = {
    searchText2: 'service2',
    name: '',
    password: '',
    jwt_token: ''
  };

  // define isService for if the user add service
  isService = false;

  // define validator public key function 

  getValidator_publickey = {
    name : '',
    password : '',
    public_key : '',
    jwt_token: ''
  }

  // define serviceOptions
  serviceOptions = [
    { id: 1, name: "service2" }
  ]
  // define ino_token for ino token
  ino_token = "";

  // define service_id from service response
  service_id = "";

  // define amount_service from service response
  amount_service = '';

  // define receiverDetails2 for search in send transaction
  receiverDetails: any = {
    PublicKey: ''
  };

  // define receiverDetails2 for search in purchase service
  receiverDetails2: any = [];

  // define fileContent for private key file content
  fileContent: string = '';

  // start define senderDetails for data of user
  senderDetails = {
    // define public key
    PublicKey: JSON.parse(localStorage.getItem('userData')).AccountPublicKey,
    // define private key
    PrivateKey: JSON.parse(localStorage.getItem('userData')).AccountPrivateKey,
    // define password
    password: JSON.parse(localStorage.getItem("userData")).AccountPassword
  }
  // end define senderDetails for data of user

  // define for token_ids for get all tokens id
  token_ids: any = [];

  // define isSearch
  isSearch = false;

  algorithm = 'aes-256-cfb';
  // start constructor for account service , services service , toastr and router
  constructor(
    // define router
    private router: Router,
    // define toastr service
    private toastr: ToastrService,
    // define account service
    private accountSvr: AccountService,
    // define services service
    private serviceSvr: ServicesService,
    private location: Location
  ) { 
    localStorage.setItem("page location", JSON.stringify(this.location.path()));
  }
  // end constructor for account service , services service , toastr and router

  // start ngOnInit function
  ngOnInit() {

    // define ino_token and assign it 0000000000000
    this.ino_token = "0000000000000";

    // starrt check if searchDetails in local storage
    if (JSON.parse(localStorage.getItem("searchDetails"))) {

      // assign isSearch equal true
      this.isSearch = true;

      // start define receiverDetails after search
      this.receiverDetails = {
        //get PublicKey from searchDetails item from local storage and assign it to PublicKey
        PublicKey: JSON.parse(localStorage.getItem('searchDetails')).PublicKey,
        //get UserName from searchDetails item from local storage and assign it to UserName
        UserName: JSON.parse(localStorage.getItem('searchDetails')).UserName
      }
      // end define receiverDetails after search
    }
    // end check if searchDetails in local storage

    // start check else if searchDetails in local storage
    else {
      // assign isSearch equal false
      this.isSearch = false;
    }
    // end check else if searchDetails in local storage

    // start check if searchDetails2 is exist
    if (JSON.parse(localStorage.getItem("searchDetails2"))) {
      // assign isSearch equal true
      this.isSearch = true;

      // start define receiverDetails2 after search in purchase service
      this.receiverDetails2 = {
        //get PublicKey from searchDetails2 item from local storage and assign it to PublicKey
        PublicKey: JSON.parse(localStorage.getItem('searchDetails2')).PublicKey,
        //get UserName from searchDetails2 item from local storage and assign it to UserName
        UserName: JSON.parse(localStorage.getItem('searchDetails2')).UserName
      }
      // end define receiverDetails2 after search in purchase service
    }
    // end check if searchDetails2 is exist

    // start check else if searchDetails2 is exist
    else {
      // assign isSearch equal false
      this.isSearch = false;
    }
    // end check else if searchDetails2 is exist
    // start check if service response is exist in local storage
    if (JSON.parse(localStorage.getItem('service response'))) {

      // get service_id from service response
      this.service_id = JSON.parse(localStorage.getItem('service response')).ID;

      // print service_id
      console.log("service id : " + this.service_id);

      // get amount_service from service response in local storage
      // this.amount_service = JSON.parse(localStorage.getItem('service response')).Amount;

            this.amount_service = JSON.parse(localStorage.getItem('service response')).Amount;

      // print amount_service
      console.log("service amount : " + this.amount_service);
      // assign isService equal true
      this.isService = true;
    }
    // end check if service response is exist in local storage
    // start check else if service response is exist in local storage
    else {
      // assign isService equal false
      this.isService = false;
    }
    // end check else if service response is exist in local storage

    // get all tokens id from tokensDetails in local storage 
    this.token_ids = JSON.parse(localStorage.getItem('tokensDetails'));
  }
  // end ngOnInit function

  encryptText(keyStr, text) {

    const hash = sha256(keyStr);

    // const hash = crypto_string.createHash('sha256');
    // hash.update(keyStr);
    const keyBytes = CryptoJS.enc.Utf8.parse(hash);
    // const keyBytes = hash.digest();

    const iv = crypto_string.randomBytes(16);
    const cipher = crypto_string.createCipheriv(this.algorithm, keyBytes, iv);
    console.log('IV:', iv);
    let enc = [iv, cipher.update(text, 'utf8')];
    enc.push(cipher.final());
    return Buffer.concat(enc).toString('base64');
  }

  ////////////////////////////////////////////////////////////////////////

  // start function for amount be float
  pad(num: number, size: number): string {
    let s = num + "";
    while (s.length < size) s = s + '0';
    return s;
  }
  // end function for amount be float

  ////////////////////////////////////////////////////////////////////////

  // start onChange for after user upload private key and read the content
  onChange(fileList: FileList): void {
    let file = fileList[0];
    let fileReader: FileReader = new FileReader();
    let self = this;
    fileReader.onloadend = function (x) {
      self.fileContent = String(fileReader.result);
      console.log("file content : " + self.fileContent);
    }
    fileReader.readAsText(file);
    this.fileContent = self.fileContent;
  }
  // end onChange for after user upload private key and read the content

  ////////////////////////////////////////////////////////////////////////

  // start function of send transaction             
  OnSubmit(form: NgForm) {

    // get userData from local storage and assign it to this.userData
    this.userData = JSON.parse(localStorage.getItem('userData'));

    // print this.userData
    console.log(this.userData)

    // start check if the amount is null
    if (form.value.Amount == "" || form.value.Amount == null || (parseFloat(form.value.Amount) == 0)) {
      // show notification if the amount is null
      this.toastr.error('Amount is required , يرجى إدخال المبلغ', '',
        {
          timeOut: 2000
        });
    }
    // end check if the amount is null

    // start check if the amount leatest than 0
    if (form.value.Amount < 0) {
      // show notification if the amount leatest than 0
      this.toastr.error('Amount must be larger than 0 , يرجى إدخال المبلغ اكبر من 0', '',
        {
          timeOut: 2000
        });
    }
    // end check if the amount leatest than 0

    // start check if the token id is null                
    if (form.value.token_id == '' || form.value.token_id == null) {
      // show notification if the token id is null
      this.toastr.error('Please choose send token , يرجى تحديد الاسم  ', '',
        {
          timeOut: 2000
        });
    }
    // end check if the token id is null                

    // start else 1- if the amount is null 2- if the amount leatest than 0 3- if the token id is null  
    else {
      // get public key of user and assign it to this.publicKey
      this.publicKey = this.userData.PublicKey;

      // print data of form
      console.log(form.value);

      // get amount and assign it to amountInput
      var amountInput = String(form.value.Amount);

      //  define dot
      var dot = ".";

      // start check if the amountInput not has dot
      if (amountInput.indexOf(dot) === -1) {
        //print this.pad(0, 6) 
        console.log("pad function : " + this.pad(0, 6));
        // define floatNum and assign it to this.pad(0, 6)
        var floatNum = this.pad(0, 6);
        // make combination between amountInput , floatNum and assign it to amountInput
        amountInput = amountInput + "." + floatNum;
        // print amountInput
        console.log("amount input :" + amountInput);
      }
      // end check if the amountInput not has dot

      // start else if the amountInput not has dot
      else {

        // make split and get before dot and after dot and assign it to amount_before
        var amount_before = amountInput.split(".");
        // print amount_before
        console.log("amount before : " + amount_before);
        // define floatNum and assign it to this.pad(Number(amount_before[1]), 6)
        var floatNum = this.pad(Number(amount_before[1]), 6);

        // make combination between amount_before[0]  , floatNum and assign it to amountInput
        amountInput = amount_before[0] + "." + floatNum;
        console.log("amount input :" + amountInput);
      }
      // end else if the amountInput not has dot

      // print amountInput
      console.log("final amout : " + amountInput) //50.500000

      // get amount and assign it to tokenDetails.Amount
      this.sendDetails.Amount = Number.parseFloat(amountInput); //50.5

      // print sendDetails.Amount
      console.log("final amount 2 : " + this.sendDetails.Amount);

      // call strftime and assign it to sendDetails.TokenTime 
      this.sendDetails.timeZone = strftime('%FT%I:%M:%S%z').replace("0200", "0000");
      // make slice for make time 02:00 and assign it to splitTime
      var splitTime = this.sendDetails.timeZone.slice(0, -2) + ":" + this.sendDetails.timeZone.slice(-2);
      // print splitTime
      console.log("split time : " + splitTime);
      // print sendDetails.TokenTime
      console.log("new time : " + this.sendDetails.timeZone);
      // assign splitTime to sendDetails.TokenTime
      this.sendDetails.timeZone = splitTime;

      // get token id details from local storage and assign it to sendDetails.token
      this.sendDetails.token = JSON.parse(localStorage.getItem("token id details"));

      // start check if isSearch is true
      if (this.isSearch === true) {
        // get receiverDetails.PublicKey and assign it to sendDetails.reciever
        this.sendDetails.reciever = this.receiverDetails.PublicKey;
      }
      // end check if isSearch is true

      // start check else if isSearch is false
      else {
        // get receiverDetails.PublicKey and assign it to sendDetails.reciever
        this.sendDetails.reciever = this.receiverDetails.PublicKey;
      }

      // get senderDetails.PublicKey and assign it to sendDetails.Sender
      this.sendDetails.Sender = this.senderDetails.PublicKey;

      // print sendDetails.reciever
      console.log("receiver public key : " + this.sendDetails.reciever);

      // print form.value.reciever
      console.log("receiver public key 2^2 : " + form.value.reciever);

      // define key for the encrypt private key
      var key = "digital wallet";

      // decrypt the private key file content with Aes and assign it to dec_private_key
      var dec_private_key = CryptoJS.AES.decrypt(this.fileContent.trim(), key.trim()).toString(CryptoJS.enc.Utf8);

      // print dec_private_key
      console.log("private key : " + dec_private_key);
      // print sendDetails.Amount      var dot = ".";

      // start check if the amountInput not has dot
      if (amountInput.indexOf(dot) === -1) {
        //print this.pad(0, 6) 
        console.log("pad function : " + this.pad(0, 6));
        // define floatNum and assign it to this.pad(0, 6)
        var floatNum = this.pad(0, 6);
        // make combination between amountInput , floatNum and assign it to amountInput
        amountInput = amountInput + "." + floatNum;
        // print amountInput
        console.log("amount input :" + amountInput);
      }
      // end check if the amountInput not has dot

      // start else if the amountInput not has dot
      else {

        // make split and get before dot and after dot and assign it to amount_before
        var amount_before = amountInput.split(".");
        // print amount_before
        console.log("amount before : " + amount_before);
        // define floatNum and assign it to this.pad(Number(amount_before[1]), 6)
        var floatNum = this.pad(Number(amount_before[1]), 6);

        // make combination between amount_before[0]  , floatNum and assign it to amountInput
        amountInput = amount_before[0] + "." + floatNum;
        console.log("amount input :" + amountInput);
      }
      // end else if the amountInput not has dot
      console.log("number amount : " + this.sendDetails.Amount);
      // define amount and assign it to string_amount
      var string_amount = String(amountInput);
      // print string_amount
      console.log("string amount : " + string_amount);

      // start setParam for signature
      setParam(
        // define sender public key
        this.senderDetails.PublicKey,
        // define receiver public key
        this.sendDetails.reciever,

        // define amount
        string_amount,
        // define dec_private_key
        dec_private_key,
        //define time
        splitTime);
      // end setParam for signature

      // call gethSig function and assign it to h_sig
      var h_sig = gethSig();

      // define h_sig and assign it to sendDetails.hash
      this.sendDetails.Hash = h_sig

      // print signature
      console.log("signature : " + h_sig);

      // start call get validator public key function 

      this.getValidator_publickey.name = JSON.parse(localStorage.getItem('userData')).AccountName;
      this.getValidator_publickey.password = JSON.parse(localStorage.getItem('userData')).AccountPassword;
      this.getValidator_publickey.public_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey;
this.getValidator_publickey.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));



this.accountSvr.getValidator_publickey(this.getValidator_publickey).subscribe((data:any) => {
localStorage.setItem("validator public key",JSON.stringify(data));
console.log("validator public key : " + JSON.stringify(data));
});
      // end call get validator public key function 


      // call encrypt_transaction2 function 

      var data = new DigitalwalletTransaction()
      data.Sender = this.senderDetails.PublicKey;
      data.Receiver = this.receiverDetails.PublicKey;
      data.TokenID = form.value.token_id;
      data.Time = splitTime;
      data.Amount = Number.parseFloat(amountInput);
      data.Signature = h_sig;
      const key_encryption = "d5fce9709e9cae77db9ecf540758e7c3d6787acd";

      var RSAkey_encryption = encrypt_transaction2();

      console.log("encryption details : " + JSON.stringify(data));

      const encrypted = CryptoJS.AES.encrypt(JSON.stringify(data), key_encryption);

      this.sendDetails.encrypt_data = RSAkey_encryption.concat(encrypted);

      console.log(encrypted.toString());

      this.sendDetails.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));
      // start call refundToken from account service for send transaction
      this.accountSvr.sendBalance(this.sendDetails)
        .subscribe((data: any) => {



          // add send transaction response to local storage
          localStorage.setItem('Responses', JSON.stringify(data));

          // start show notification transaction sent successfully
          this.toastr.success('Your transaction with ' + this.sendDetails.Amount + ' coin has been sent successfully to ' + this.receiverDetails.UserName + '', '',
            {
              timeOut: 2000
            });
          // end show notification transaction sent successfully

          // go to transactions page
          this.router.navigate(['./pages/transactions']);


          this.userData.Balance = parseInt(this.userData.Balance) - parseInt(form.value.Amount)

          // remove userData from local storage
          localStorage.removeItem("userData");

          // set userData in local storage
          localStorage.setItem("userData", JSON.stringify(this.userData));

          // remove searchDetails from local storage
          localStorage.removeItem("searchDetails");

        })
      // end call refundToken from account service for send transaction
    }
    // end else 1- if the amount is null 2- if the amount leatest than 0 3- if the token id is null  
  }
  // end function of send transaction             


  ////////////////////////////////////////////////////////////////////////

  // start function of purchase service             
  purchase_service(form: NgForm) {

    // call strftime and assign it to puchase_details.TokenTime 
    this.puchase_details.timeZone = strftime('%FT%I:%M:%S%z').replace("0200", "0000");

    // make slice for make time 02:00 and assign it to splitTime
    var splitTime = this.puchase_details.timeZone.slice(0, -2) + ":" + this.puchase_details.timeZone.slice(-2);
    // print splitTime
    console.log("split time2 : " + splitTime);
    // print sendDetails.TokenTime
    console.log("new time : " + this.sendDetails.timeZone);
    // print receiver public key 
    console.log("receiver public key : " + this.puchase_details.reciever);
    // print form.value.reciever
    console.log("receiver public key 2^2 : " + form.value.reciever);

    // define key for the encrypt private key
    var key = "digital wallet";
    // decrypt the private key file content with Aes and assign it to dec_private_key
    var dec_private_key = CryptoJS.AES.decrypt(this.fileContent.trim(), key.trim()).toString(CryptoJS.enc.Utf8);

    // print dec_private_key
    console.log("private key2 : " + dec_private_key);

    // this.amount_service = this.amount_service.replace("000000","25")
    console.log("service amount : " + this.amount_service);


    this.amount_service = this.amount_service

    // get sender public key and assign it to puchase_details.Sender
    this.puchase_details.Sender = this.senderDetails.PublicKey;
    // get receiver public key and assign it to puchase_details.reciever
    this.puchase_details.reciever = this.receiverDetails2.PublicKey;
    // get time and assign it to puchase_details.timeZone
    this.puchase_details.timeZone = splitTime;

    // get amount and assign it to puchase_details.Amount
    this.puchase_details.Amount = this.amount_service;
    // this.puchase_details.Amount = this.amount_service;

    console.log("purchase service amount : " + this.amount_service);
    // get service id and assign it to puchase_details.service_id
    this.puchase_details.service_id = this.service_id;
    // get token and assign it to puchase_details.token
    this.puchase_details.token = form.value.token;
    //get public key from userData item from local storage and assign it to puchase_details.public_key
    this.puchase_details.public_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey
    //get AccountPassword from userData item from local storage and assign it to puchase_details.password
    this.puchase_details.password = JSON.parse(localStorage.getItem('userData')).AccountPassword;

    this.puchase_details.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));


    
    // var float_amount = parseFloat(this.amount_service);


    var string_amount = String(this.amount_service);

    // print string_amount

    console.log("string amount : " + string_amount);

    // start setParam for signature
    setParam(
      // define sender public key
      this.puchase_details.Sender,
      // define receiver public key
      this.puchase_details.reciever,
      // define amount
      string_amount,
      // define dec_private_key
      dec_private_key,
      splitTime);
    // end setParam for signature

console.log("sender public key : " + this.puchase_details.Sender);

console.log("receiver public key : " + this.puchase_details.reciever);

console.log("service amount : " + string_amount);

console.log("private key : " + dec_private_key);

console.log("time : " + splitTime);



    // call gethSig function and assign it to h_sig
    var h_sig = gethSig();

    // define h_sig and assign it to puchase_details.hash
    this.puchase_details.Hash = h_sig;

    // print signature
    console.log("signature : " + h_sig);

    // start call puchase_service from services service for send transaction
    this.serviceSvr.puchase_service(this.puchase_details).subscribe((data: any) => {

      // add purchase service response to local storage
      localStorage.setItem('purchase response', JSON.stringify(data));

      // remove service response from local storage
      localStorage.removeItem("service response");

      // start show notification service is added successfuly
      this.toastr.success('service is added successfuly', '',
        {
          timeOut: 2000
        });
      // end show notification service is added successfuly

      // go to services page
      this.router.navigate(['./pages/services']);

      // remove searchDetails2 from local storage
      localStorage.removeItem("searchDetails2");
    })
    // end call puchase_service from services service for send transaction
  }
  // end function of purchase service             

  // start function of remove service             
  removeService() {
    // remove service response from local storage
    localStorage.removeItem("service response");
    // remove searchDetails from local storage
    localStorage.removeItem("searchDetails2");

    // go to add service page
    this.router.navigate(['./pages/add-service']);

    // start show notification service is canceled successfuly
    this.toastr.success('service is canceled successfuly', '',
      {
        timeOut: 2000
      });

    // end show notification service is canceled successfuly
  }
  // end function of purchase service  

  ///////////////////////////////////////////////////////////////////////

  // start function of save token id  
  save(token) {
    // add  token id to local storage
    localStorage.setItem("token id details", JSON.stringify(token));
  }
  // end function of save token details  

  //////////////////////////////////////////////////////////////////////

  // start function of search in send transaction   
  onSearch(form: NgForm) {

    // define form data
    console.log(form.value);

    // start check if the search text is null
    if (form.value.searchText === '' || form.value.searchText === null) {
      // show notification if the search text is null
      this.toastr.error('Receiver public key is required');
    }
    // end check if the search text is null

    // start check else if the search text is null
    else {
      // alert search text
      localStorage.setItem("search text : ", JSON.stringify(form.value.searchText));
      //get AccountPassword from userData item from local storage and assign it to searchDetails.password
      this.searchDetails.password = JSON.parse(localStorage.getItem('userData')).AccountPassword;
      //get name from userData item from local storage and assign it to tokenDetails.name
      this.searchDetails.name = JSON.parse(localStorage.getItem('userData')).AccountName;
      // get search text and assign it to searchDetails.searchText
      this.searchDetails.searchText = form.value.searchText;

      this.searchDetails.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));

      // start call search from account service
      this.accountSvr.search(this.searchDetails).subscribe((data: any) => {

        // add search response in send transaction to local storage 
        localStorage.setItem('searchDetails', JSON.stringify(data));

        // start check if searchDetails not has ErrorMessage
        if (!JSON.parse(localStorage.getItem('searchDetails')).ErrorMessage) {

          // remove class show from modal class
          $('.modal').removeClass('show');

          // hide modal overlay
          $('.modal-backdrop.show').hide();

          // set padding right for body
          $('body').css("padding-right", "0");

          //  show notification Search successfully
          this.toastr.success('Search is Successfully');

          // reload send transaction page
          location.reload();
        }
        // end check if searchDetails not has ErrorMessage

        // start check else if searchDetails not has ErrorMessage 
        else {
          //  show notification Search by error message
          this.toastr.error(JSON.parse(localStorage.getItem('searchDetails')).ErrorMessage)
        }
        // end check else if searchDetails not has ErrorMessage 
      });
      // end call search from account service
    }
    // end check else if the search text is null
  }
  // end function of search in send transaction  

  ///////////////////////////////////////////////////////////////////////

  // start function of search in purchase service   
  onSearch2(form: NgForm) {

    // define form data
    console.log(form.value);

    // start check if the search text is null
    if (form.value.searchText2 === '' || form.value.searchText2 === null) {
      // show notification if the search text is null
      this.toastr.error('Receiver public key is required');
    }
    // end check if the search text is null

    // start check else if the search text is null
    else {
      //get AccountPassword from userData item from local storage and assign it to search2Details.password
      this.search2Details.password = JSON.parse(localStorage.getItem('userData')).AccountPassword;
      //get name from userData item from local storage and assign it to search2Details.name
      this.search2Details.name = JSON.parse(localStorage.getItem('userData')).AccountName;
      // get search text and assign it to search2Details.searchText2
      this.search2Details.searchText2 = form.value.searchText2;

      this.search2Details.jwt_token = JSON.parse(localStorage.getItem('jwt_token'));

      // start call search2 from account service
      this.accountSvr.search2(this.search2Details).subscribe((data: any) => {

        // add search response in send transaction to local storage 
        localStorage.setItem('searchDetails2', JSON.stringify(data));

        // start check if searchDetails2 not has ErrorMessage
        if (!JSON.parse(localStorage.getItem('searchDetails2')).ErrorMessage) {

          // remove class show from modal class
          $('.modal').removeClass('show');
          // hide modal overlay
          $('.modal-backdrop.show').hide();
          // set padding right for body
          $('body').css("padding-right", "0");

          //  show notification Search successfully
          this.toastr.success('Search is Successfully');

          // reload purchase service page
          location.reload();
        }
        // end check if searchDetails2 not has ErrorMessage

        // start check else if searchDetails not has ErrorMessage 
        else {
          this.toastr.error(JSON.parse(localStorage.getItem('searchDetails')).ErrorMessage)
        }
        // end check else if searchDetails not has ErrorMessage 
      });
      // end call search2 from account service
    }
    // end check else if the search text is null
  }
  // end function of search in purchase service   
}

export class DigitalwalletTransaction {
  Sender: string;
  Receiver: string;
  TokenID: string;
  Amount: number;
  Time: string;
  Signature: string;
  ServiceId: string;
}
