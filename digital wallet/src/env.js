(function (window) {
    window.__env = window.__env || {};
  
    // API url
    window.__env.apiUrl = 'http://3.13.219.172:8095';   
    //'http://197.51.66.21:8095';
    window.__env.envFileLoaded = true;
    // Whether or not to enable debug mode
    // Setting this to false will disable console output
    window.__env.enableDebug = false;
  }(this));