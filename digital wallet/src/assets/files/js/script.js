


function doSign2(private_key, public_key) {
    var rsa = new RSAKey();
    rsa.readPrivateKeyFromPEMString(private_key);
    var hashAlg = 'sha1';
    var hSig = rsa.sign('msg', hashAlg);
    var signature = linebrk(hSig, 16);
    doVerify2(signature, public_key);
}

function doVerify2(signature, public_key) {
 
    var sMsg = 'msg';
    var hSig = signature;

    // alert("welcome to verify")

    console.log("public key verify : " + public_key);

    try {
        var pubKey = KEYUTIL.getKey(public_key);

        var isValid = pubKey.verify(sMsg, hSig);
        // alert(isValid);
        if (isValid) {
            console.log("**************************** valid ***********************************");
        } else {
            console.log("**************************** not valid ***********************************");
        }
    } catch (e) {
        console.log(e);
    }


}

function copyMsgAndSig() {
    _displayStatus("reset");
    document.form1.msgverified.value = document.form1.msgsigned.value;
    document.form1.sigverified.value = document.form1.siggenerated.value;
}

function _displayStatus(sStatus) {
    var div1 = document.getElementById("verifyresult");
    if (sStatus == "valid") {
        div1.style.backgroundColor = "skyblue";
        div1.innerHTML = "This signature is VALID.";
    } else if (sStatus == "invalid") {
        div1.style.backgroundColor = "deeppink";
        div1.innerHTML = "This signature is NOT VALID.";
    } else if (sStatus == "pub_err") {
        div1.style.backgroundColor = "deeppink";
        div1.innerHTML = "Public key can't verify private key signature , so signature is NOT VALID. or not from trusted sender";
    } else {
        div1.style.backgroundColor = "yellow";
        div1.innerHTML = " Please fill values below and push [Verify this sigunature] button.";
    }
}


