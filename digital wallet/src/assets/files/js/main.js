'use strict';

const crypto_string = require('crypto');
const fs = require('fs');
const NodeRSA = require('node-rsa');
const algorithm = 'aes-256-cfb';
//*************************************************************************************************** */


function SHA1 (msg) {

  function rotate_left(n,s) {

      var t4 = ( n<<s ) | (n>>>(32-s));
      return t4;
  };




  function lsb_hex(val) {


      var str="";


      var i;


      var vh;


      var vl;




      for( i=0; i<=6; i+=2 ) {


          vh = (val>>>(i*4+4))&0x0f;


          vl = (val>>>(i*4))&0x0f;


          str += vh.toString(16) + vl.toString(16);


      }


      return str;


  };




  function cvt_hex(val) {


      var str="";


      var i;


      var v;




      for( i=7; i>=0; i-- ) {


          v = (val>>>(i*4))&0x0f;


          str += v.toString(16);


      }


      return str;


  };






  function Utf8Encode(string) {


      string = string.replace(/\r\n/g,"\n");


      var utftext = "";




      for (var n = 0; n < string.length; n++) {




          var c = string.charCodeAt(n);




          if (c < 128) {


              utftext += String.fromCharCode(c);


          }


          else if((c > 127) && (c < 2048)) {


              utftext += String.fromCharCode((c >> 6) | 192);


              utftext += String.fromCharCode((c & 63) | 128);


          }


          else {


              utftext += String.fromCharCode((c >> 12) | 224);


              utftext += String.fromCharCode(((c >> 6) & 63) | 128);


              utftext += String.fromCharCode((c & 63) | 128);


          }




      }




      return utftext;


  };




  var blockstart;


  var i, j;


  var W = new Array(80);


  var H0 = 0x67452301;


  var H1 = 0xEFCDAB89;


  var H2 = 0x98BADCFE;


  var H3 = 0x10325476;


  var H4 = 0xC3D2E1F0;


  var A, B, C, D, E;


  var temp;




  msg = Utf8Encode(msg);




  var msg_len = msg.length;




  var word_array = new Array();


  for( i=0; i<msg_len-3; i+=4 ) {


      j = msg.charCodeAt(i)<<24 | msg.charCodeAt(i+1)<<16 |


      msg.charCodeAt(i+2)<<8 | msg.charCodeAt(i+3);


      word_array.push( j );


  }




  switch( msg_len % 4 ) {


      case 0:


          i = 0x080000000;


      break;


      case 1:


          i = msg.charCodeAt(msg_len-1)<<24 | 0x0800000;


      break;




      case 2:


          i = msg.charCodeAt(msg_len-2)<<24 | msg.charCodeAt(msg_len-1)<<16 | 0x08000;


      break;




      case 3:


          i = msg.charCodeAt(msg_len-3)<<24 | msg.charCodeAt(msg_len-2)<<16 | msg.charCodeAt(msg_len-1)<<8    | 0x80;


      break;


  }




  word_array.push( i );




  while( (word_array.length % 16) != 14 ) word_array.push( 0 );




  word_array.push( msg_len>>>29 );


  word_array.push( (msg_len<<3)&0x0ffffffff );






  for ( blockstart=0; blockstart<word_array.length; blockstart+=16 ) {




      for( i=0; i<16; i++ ) W[i] = word_array[blockstart+i];


      for( i=16; i<=79; i++ ) W[i] = rotate_left(W[i-3] ^ W[i-8] ^ W[i-14] ^ W[i-16], 1);




      A = H0;


      B = H1;


      C = H2;


      D = H3;


      E = H4;




      for( i= 0; i<=19; i++ ) {


          temp = (rotate_left(A,5) + ((B&C) | (~B&D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;


          E = D;


          D = C;


          C = rotate_left(B,30);


          B = A;


          A = temp;


      }




      for( i=20; i<=39; i++ ) {


          temp = (rotate_left(A,5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;


          E = D;


          D = C;


          C = rotate_left(B,30);


          B = A;


          A = temp;


      }




      for( i=40; i<=59; i++ ) {


          temp = (rotate_left(A,5) + ((B&C) | (B&D) | (C&D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;


          E = D;


          D = C;


          C = rotate_left(B,30);


          B = A;


          A = temp;


      }




      for( i=60; i<=79; i++ ) {


          temp = (rotate_left(A,5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;


          E = D;


          D = C;


          C = rotate_left(B,30);


          B = A;


          A = temp;


      }




      H0 = (H0 + A) & 0x0ffffffff;


      H1 = (H1 + B) & 0x0ffffffff;


      H2 = (H2 + C) & 0x0ffffffff;


      H3 = (H3 + D) & 0x0ffffffff;


      H4 = (H4 + E) & 0x0ffffffff;




  }




  var temp = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);




  return temp.toLowerCase();



}

//****************************************************************************************************** */

function encryptText(keyStr, text) {
  const hash = crypto_string.createHash('sha256');
  hash.update(keyStr);
  const keyBytes = hash.digest();

  const iv = crypto_string.randomBytes(16);
  const cipher = crypto_string.createCipheriv(algorithm, keyBytes, iv);
  console.log('IV:', iv);
  let enc = [iv, cipher.update(text, 'utf8')];
  enc.push(cipher.final());
  return Buffer.concat(enc).toString('base64');
}

function decryptText(keyStr, text) {
  const hash = crypto_string.createHash('sha256');
  hash.update(keyStr);
  const keyBytes = hash.digest();

  const contents = Buffer.from(text, 'base64');
  const iv = contents.slice(0, 16);
  const textBytes = contents.slice(16);
  const decipher = crypto_string.createDecipheriv(algorithm, keyBytes, iv);
  let res = decipher.update(textBytes, '', 'utf8');
  res += decipher.final('utf8');
  return res;
} 

function encrypt_transaction(transaction,user_private_key){
//user private key hash
const privatekeyhash = user_private_key+ Date.now().toString()// "26ac2cf9f2f0ab62351455efb2e9a080fee17a5dbe6d09d97277096302569e5d";

//********************** */encrypt the hash using RSA***********************s*******
//----------- initialize pair of RSA keys---------------
let publickey = fs.readFileSync('validator0/public.pem').toString();
let privatekey = fs.readFileSync('validator0/private.pem').toString();
const key = new NodeRSA();
key.importKey(publickey,'pkcs1-public-pem');
const publickeyWEK = key.exportKey('pkcs1-public-pem');
key.publickey =publickeyWEK
key.importKey(privatekey,'pkcs1-private-pem');
const privatekeyWEK = key.exportKey('pkcs1-private-pem');
key.privatekey =privatekeyWEK
//------------- encrypt using RSA-----------------------
const text = privatekeyhash;
const encryptedkey = key.encrypt(privatekeyhash, 'base64');
console.log('RSA of privatekey encryption : ', encryptedkey);
console.log('------------- I AM JUST LINE --------------------');
//********************/encryption of json data using AEsS***********************
const encrypteddata ='{"Sender":"Sender","Receiver":"Receiver","TokenID":"asdfghjklmnbvcxasdfghjkjtrew","Amount":20,"Time":"2019-08-18T15:24:47.66524029+02:00","Signature":"Signature"}' //"{'Email': 'alaa.aa.radwan1994@gmail.com','Address': 'cairoo Egypt','Phonnum': '0211113111111111','Name': 'alaarwadwan','AuthenticationType': 'passss','AuthenticationValue':'','Password': '15e2b0d3c33891ebb0f1ef609ec419420c20e320ce94c65fbc8c3312448eb225'}"
const encryptedoutput = encryptText(privatekeyhash,encrypteddata)
console.log('AUS of data encryption: ', encryptedoutput);
const encrypted = "yhMo0lYMjXZu7VjB+WMp5lk5pfB1Aj4CYGD+OM1HftBjbsEvk4zxTjbhhNkg77RrYQcWCVozL0kWmvw3qoe6gs1lzyDPf4ujQVJyMxdUCskTpod7U9yd0NRVPp18S3VnetBi+jvLvBGQiM7W9yecY7wK9+mhZq2cCKD3H9q6JnTHyYLvWAhAUhV1MGu1nXl2tNmxuRPnjlR3KnavXYGASzrOjOCOj83u1e1gVBmTvUdH5+vQVcFjMSF+EFHPDU+ABwQO+gyQftWGf9oTsZVdONzH1MhDixLtQ8h1fe5NXAp7yiUw+v4JMcXL+N0qEKsL+GSgGapWlbvIqlTvbmI6pYShrQ87j1eq4geS6MIO"
// console.log('Encrypted: ', encrypted);
const decrypted = decryptText("d5fce9709e9cae77db9ecf540758e7c3d6787acd", "F4OC4naQ74v4147hrgKaNptCDwu6CDncsKA1WSo403hBspFkoMnR3xJlod4PAV8G2ylP2rHBWULsuQdQxhAFsg==");
console.log('Decrypted: ', decrypted);
console.log('------------- I AM ANOTHER LINE --------------------')
var hashhhhh = SHA1(publickey)
console.log("hashed key "+hashhhhh)
console.log(hashhhhh.length)
console.log(" data is :")
console.log(encryptedkey + encryptedoutput)
}