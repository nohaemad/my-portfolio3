// const NodeRSA = require('node-rsa');
// const key = new NodeRSA({b: 512});
const fs = require('fs');
// const text = 'Hello RSA!';
// const publickey = fs.readFileSync('vnode0/public.pem').toString();
// console.log(publickey)
// const key2 = new NodeRSA(publickey);
// console.log("key2"+key2)
// const encrypted2 = key2.encrypt(text, 'base64');
// console.log("encrypted2"+encrypted2)
// console.log("\n")
// console.log("length of encrypted2 : "+encrypted2.length)

const crypto_string = require('crypto');

function RSAEncrypt(rsakey, text){
    let encrypted = crypto_string.publicEncrypt({key:rsakey,
        padding:crypto_string.constants.RSA_PKCS1_PADDING},new Buffer(text)).toString('base64');
    return encrypted;
}

rsakey= fs.readFileSync('vnode5/public.pem').toString(); //'-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCm5IN1uRvak0Kod3YviD/b67dj\nzP/ubU+8RgbCnm1HUVYBAVGnvg5epMfGunXKFXSb1ehOvQ2K+fEJLa+pKy2uLZLd\n/6gbUGJn+q8wGiFKfu0U0H3E+2yH6eFX+IPXx5OJNwUE6yqKR6hOBz5qR/AtVRfM\n6aAcDLIR7wE06SnHVQIDAQAB\n-----END PUBLIC KEY-----\n'


let text = "fhimhposrjthsirjpthvmjthsrutshrithsrutphuthr"
let encrypted = RSAEncrypt(rsakey,text)

console.log("encrypted         "+encrypted)
console.log("length         "+encrypted.length)
